---
title: "SABER"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 13
---
[SABER](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/) is a key-encapsulation mechanism (KEM) based on the hardness of the module learning with rounding problem (MLWR) [Specs.](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/files/saberspecround3.pdf).

- Required parameter for definiteness:
  - $l$: Positive integer

- Non-required parameter for definiteness:
  - $n$: Positive integer (always $256$)
  - $q$: Positive integer (always $2^{13}$)
  - $p$: Positive integer (always $2^{10}$)
  - $T$: Positive integer
  - $\mu$: positiver Integer
