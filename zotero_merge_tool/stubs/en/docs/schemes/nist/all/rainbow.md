---
title: "Rainbow"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 12
---
[Rainbow](https://www.pqcrainbow.org/) is a public-key cryptosystem based on the hardness of solving a set of random multivariate quadratic systems [Specs.](https://csrc.nist.gov/projects/post-quantum-cryptography/round-2-submissions).

- Required parameter for definiteness:
  - $\nu_1$: Positive integer

- Required parameter for definiteness:
  - $\mathbb{F}$: Galois-Field (either $GF(16)$ or $GF(256)$)
  - $o_1$: Positive integer
  - $o_2$: Positive integer
