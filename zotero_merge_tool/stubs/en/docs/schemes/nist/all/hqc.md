---
title: "HQC"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 6
---
[HQC](http://pqc-hqc.org/) is a Hamming quasi-cyclic code-based public-key encryption scheme [Specs.](https://www.pqc-hqc.org/download.php?file=hqc-specification_2021-06-06.pdf).
