---
title: "Falcon"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 3
---
[Falcon](https://falcon-sign.info/) is a lattice-based signature scheme based on the short integer solution problem (SIS) over NTRU lattices [Specs.](https://falcon-sign.info/falcon.pdf).

- Required parameter for definiteness:
  - $n$: Positive integer

- Required parameter for definiteness:
  - $q$: Positive integer (always $12289$)
  - $\sigma$: Positive float
  - $\sigma_{min}$: Positive float
  - $\sigma_{max}$: Positive float
  - $\lfloor \beta^{2} \rfloor$: Positive integer
