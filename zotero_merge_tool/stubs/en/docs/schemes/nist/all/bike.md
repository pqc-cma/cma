---
title: "BIKE"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 1
---
[BIKE](https://bikesuite.org/) is a bit flipping key encapsulation based on QC-MDPC (Quasi-Cyclic Moderate Density Parity-Check) [Specs.](https://bikesuite.org/files/v4.1/BIKE_Spec.2020.10.22.1.pdf).
