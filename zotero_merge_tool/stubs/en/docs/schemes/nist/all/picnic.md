---
title: "Picnic"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 10
---
[Picnic](https://microsoft.github.io/Picnic/) digital signature algorithm based on the zero-knowledge proof system and symmetric key primitives [Specs.](https://github.com/microsoft/Picnic/blob/master/spec/spec-v3.0.pdf).
