---
title: "CRYSTALS-Dilithium"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 2
---
[CRYSTALS-Dilithium](https://pq-crystals.org/dilithium/) is a digital signature scheme based on the hardness of the shortest vector problem (SVP) over module lattices [Specs.](https://pq-crystals.org/dilithium/data/dilithium-specification-round3-20210208.pdf).

- Required parameter for definiteness:
  - $\tau$: Positive integer

- Non-required parameter for definiteness:
  - $q$: Positive integer (always $8380417$)
  - $d$: Positive integer (always $13$)
  - $\gamma_1$: Positive integer
  - $\gamma_2$: Positive float
  - $(k,l)$: Positive integer
  - $\eta$: Positive integer
