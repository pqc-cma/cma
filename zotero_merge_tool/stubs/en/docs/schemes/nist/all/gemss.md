---
title: "GeMSS"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 5
---
[GeMSS](https://www-polsys.lip6.fr/Links/NIST/GeMSS.html) is a multivariate signature scheme that produces small signatures [Specs.](https://www-polsys.lip6.fr/Links/NIST/GeMSS_specification.pdf).
