---
title: "Signal Protocol"
linktitle: "Signal Protocol"
date: 2021-11-02T20:28:24+08:00
draft: false
type: docs
weight: 7
---
The Signal protocol is a secure instant messaging protocol that underlies the security of numerous applications such as WhatsApp, Skype, Facebook Messenger among many others.
