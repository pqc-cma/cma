---
title: "Related Work"
linktitle: "Related Work"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 4
---
A collection of survey papers dealing with general challenges and recommendations regarding the migration to post-quantum cryptography and crypto-agility.

*A full reference list can  be found in the [references](../refs) section. All references are listed in alphabetical order.*
