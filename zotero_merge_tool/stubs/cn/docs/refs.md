---
title: "参考文献"
linktitle: "参考文献"
date: 2021-05-06T00:13:12+02:00
draft: false
type: docs
weight: 12
---
###### [A.16]
[A. Langley. ImperialViolet - CECPQ1 results, 2016.](https://www.imperialviolet.org/2016/11/28/cecpq1.html)

###### [AASA+19]
[G. Alagic, J. Alperin-Sheriff, D. Apon, D. Cooper, Q. Dang, Y. Liu, C. Miller, D.Moody, R. Peralta, et al.2019.Status report on the first round of the NIST post-quantum cryptography standardization process. US Department of Commerce,National Institute of Standards and Technology](https://nvlpubs.nist.gov/nistpubs/ir/2019/NIST.IR.8240.pdf)

###### [ABB+20]
[E. Alkim, P. S. L. M. Barreto, N. Bindel, J. Krämer, P. Longa, and J. E. Ricardini. The lattice-based digital signature scheme qtesla. In M. Conti, J. Zhou, E. Casalicchio, and A. Spognardi, editors, Applied Cryptography and Network Security, pages 441–460. Springer International Publishing, 2020](https://eprint.iacr.org/2019/085.pdf)

###### [ABB+20]
[N. Aragon, P. Barreto, S. Bettaieb, L. Bidoux, O. Blazy, J. C. Deneuville, P. Gaborit, S. Gueron, T. Guneysu, C. A. Melchor, et al.2020. BIKE: bit flipping key encapsulation (22 Oct 2020)](https://bikesuite.org/files/v4.1/BIKE_Spec.2020.10.22.1.pdf)

###### [ABBC10]
[T. Acar, M. Belenkiy, M. Bellare, and D. Cash. Cryptographic agility and its relation to circular encryption. 2010.](https://eprint.iacr.org/2010/117)

###### [ABD+21]
[R. Avanzi, J. Bos, L. Ducas, E. Kiltz, T. Lepoint, V. Lyubashevsky, J. M. Schanck,P. Schwabe, G. Seiler, and D. Stehlé. 2021. CRYSTALS-Kyber algorithm specifi-cations and supporting documentation (version 3.01).NIST PQC Round 3(31Jan 2021)](https://pq-crystals.org/kyber/data/kyber-specification-round3-20210131.pdf)

###### [ABF+17]
[Y. Acar, M. Backes, S. Fahl, S. Garfinkel, D. Kim, M. L. Mazurek, and C. Stransky. Comparing the Usability of Cryptographic APIs. In 2017 IEEE Symposium on Security and Privacy (SP), pages 154–171, San Jose, CA, USA, May 2017. IEEE, doi:10.1109/SP.2017.52](http://ieeexplore.ieee.org/document/7958576/)

###### [ADPS16]
[E. Alkim, L. Ducas, T. Pöppelmann, and P. Schwabe. Post-quantum key exchange—a new hope. In 25Th {USENIX } security symposium ( {USENIX } security 16), pages 327–343, 2016](https://eprint.iacr.org/2015/1092.pdf)

###### [AEK+21]
[Azarderakhsh, R., El Khatib, R., Koziel, B., & Langenberg, B. (2021). Hardware Deployment of Hybrid PQC. IACR Cryptol. ePrint Arch., 2021, 541.](https://eprint.iacr.org/2021/541.pdf)

###### [AJO+20]
[Alagic G, Jeffery S, Ozols M, Poremba A. On Quantum Chosen-Ciphertext Attacks and Learning with Errors. Cryptography. 2020; 4(1):10. https://doi.org/10.3390/cryptography4010010](https://www.mdpi.com/2410-387X/4/1/10)

###### [AnKi18]
[An, H., & Kim, K. (2018, January). QChain: Quantum-resistant and decentralized PKI using blockchain. In Proc. SCIS (pp. 1-8).](https://caislab.kaist.ac.kr/publication/paper_files/2018/SCIS'18_HC_BC.pdf)

###### [AnSe20]
[An, SangWoo, and Seog Chung Seo. "Efficient Parallel Implementations of LWE-Based Post-Quantum Cryptosystems on Graphics Processing Units." Mathematics 8.10 (2020): 1781.](https://www.mdpi.com/2227-7390/8/10/1781)

###### [AVVY19]
[F. Armknecht, I. Verbauwhede, M. Volkamer, and M. Yung, editors. Biggest Failures in Security, volume 9 of Dagstuhl Reports. Dagstuhl Publishing, Nov. 2019.](https://drops.dagstuhl.de/opus/volltexte/2020/11981/)

###### [AZCH19]
[L. Hornquist Astrand, L. Zhu, M. Cullen, and G. Hudson. Public key cryptography for initial authentication in kerberos (PKINIT) algorithm agility. 2019. RFC 8636.](https://tools.ietf.org/html/rfc8636.html)

###### [BBC+20]
[D. Bernstein, B. Brumley, M. Chen, C. Chuengsatiansup, T. Lange, A. Marotzke, N. Tuveri, C. van Vredendaal, and B. Yang. Ntru prime: round 3 20201007. 2020](https://ntruprime.cr.yp.to/nist/ntruprime-20201007.pdf)

###### [BBC+21]
[G. Banegas, D. J. Bernstein, F. Campos, T. Chou, T. Lange, M. Meyer, B. Smith and J. Sotáková. CTIDH: faster constant-time CSIDH. 2021. Cryptology ePrint Archive, Report 2021/633](https://eprint.iacr.org/2021/633)

###### [BBG+19]
[Bindel, N., Braun, J., Gladiator, L., Stöckert, T., & Wirth, J. (2019). X. 509-compliant hybrid certificates for the post-quantum transition. Journal of Open Source Software, 4(40), 1606](https://joss.theoj.org/papers/10.21105/joss.01606)

###### [BHK+19]
[D. J. Bernstein, A. Hülsing, S. Kölbl, R. Niederhagen, J. Rijneveld, and P. Schwabe. The sphincs+ signature framework. In Proceedings of the 2019 ACM SIGSAC Conference on Computer and Communications Security, CCS ’19, page 2129–2146, New York, NY, USA, 2019. Association for Computing Machinery. doi:10.1145/3319535.3363229](https://eprint.iacr.org/2019/1086.pdf)

###### [BKS19]
[L. Botros, M. J. Kannwischer, and P. Schwabe. Memory-Efficient High-Speed Implementation of Kyber on Cortex-M4. In J. Buchmann and T. Nitaj, A.and Rachidi, editors, Progress in Cryptology – AFRICACRYPT 2019, volume 11627, pages 209–228. Springer International Publishing, Cham, 2019. Series Title: Lecture Notes in Computer Science. doi:10.1007/978-3-030-23696-0_11](http://link.springer.com/10.1007/978-3-030-23696-0_11)

###### [BPS21]
[Barker, W., Polk, W., & Souppaya, M. (2021). Getting Ready for Post-Quantum Cryptography: Exploring Challenges Associated with Adopting and Using Post-Quantum Cryptographic Algorithms (pp. 10-10). National Institute of Standards and Technology](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.04282021.pdf)

###### [Bra16]
[M. Braithwaite. Experimenting with post-quantum cryptography, 2016.](https://security.googleblog.com/2016/07/experimenting-with-post-quantum.html)

###### [BRP20]
[F. Borges, P. R. Reis and D. Pereira, "A Comparison of Security and its Performance for Key Agreements in Post-Quantum Cryptography," in IEEE Access, vol. 8, pp. 142413-142422, 2020, doi: 10.1109/ACCESS.2020.3013250.](https://ieeexplore.ieee.org/document/9153901)

###### [BSI20]
[BSI. 2020. Migration zu Post-Quanten-Kryptografie.](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Post-Quanten-Kryptografie.html)

###### [BSKNS20]
[K. Bürstinghaus-Steinbach, C. Krauß, R. Niederhagen, and M. Schneider. 2020.Post-Quantum TLS on Embedded Systems: Integrating and Evaluating Kyberand SPHINCS+ with Mbed TLS. InProceedings of the 15th ACM Asia Conferenceon Computer and Communications Security (ASIA CCS ’20). Association forComputing Machinery, 841–852](https://dl.acm.org/doi/abs/10.1145/3320269.3384725)

###### [BSNK19]
[K. Basu, D. Soni, M. Nabeel, and R. Karri. 2019. NIST Post-Quantum Cryptography - A Hardware Evaluation Study](https://eprint.iacr.org/2019/047)

###### [CaCr21]
[Campagna, M., & Crockett, E. (2019). Hybrid Post-Quantum Key Encapsulation Methods (PQ KEM) for Transport Layer Security 1.2 (TLS). Internet Engineering Task Force, Internet-Draft draft-campagna-tls-bike-sike-hybrid, 6](https://datatracker.ietf.org/doc/html/draft-campagna-tls-bike-sike-hybrid-06)

###### [Cam19]
[Campbell Sr, R. (2019). Evaluation of post-quantum distributed ledger cryptography. The Journal of The British Blockchain Association, 2(1), 7679.](https://jbba.scholasticahq.com/article/7679.pdf)

###### [CCA+21]
[S. Chowdhury, A. Covic, R. Y. Acharya, S. Dupee, and D. Ganji, F.and Forte. Physical security in the post-quantum era: A survey on side-channel analysis, random number generators, and physi- cally unclonable functions. Journal of Cryptographic Engineering, February 2021.](https://arxiv.org/abs/2005.04344)

###### [CCD+15]
[M.  Campagna,  L.  Chen,  O.  Dagdelen,  J.  Ding,  J  Fernick,  N.  Gisin,  D.  Hayford,  T.  Jennewein,  N.  Lütkenhaus,  and  M.  Mosca.  2015.Quantum  SafeCryptography  and  Security:  An  introduction,  benefits,  enablers  and  chal-lenges.European Telecommunications Standards InstituteETSI White Paper,8  (June  2015),  1–64.](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf)

###### [CCH+20]
[M. Campagna, C. Costello, B. Hess, A. Jalali, B. Koziel, B. LaMacchia, P. Longa, M. Naehrig, J. Renes, D. Urbanik, et al. Supersingular isogeny key encapsulation. 2020](https://sike.org/files/SIDH-spec.pdf)

###### [CCU+20]
[T. Chou, C. Cid, S. UiB, J. Gilcher, T. Lange, V. Maram, R. Misoczki, R. Niederhagen, K. G Paterson, Edoardo P., et al. Classic mceliece: conservative code-based cryptography 10 october 2020. 2020](https://classic.mceliece.org/nist/mceliece-20201010.pdf)

###### [CDG+17]
[Chase, M., Derler, D., Goldfeder, S., Orlandi, C., Ramacher, S., Rechberger, C., ... & Zaverucha, G. (2017, October). Post-quantum zero-knowledge and signatures from symmetric-key primitives. In Proceedings of the 2017 acm sigsac conference on computer and communications security (pp. 1825-1842)](https://eprint.iacr.org/2017/279.pdf)

###### [CDH+19]
[C. Chen, O. Danba, J. Hoffstein, A. Hülsing, J. Rijneveld, J. M Schanck, P. Schwabe, W. Whyte, and Z. Zhang. Ntru algorithm specifications and supporting documentation. Round-3 submission to the NIST PQC project, March 2019](https://ntru.org/f/ntru-20190330.pdf)

###### [CDF+21]
[Cremers, C., Düzlü, S., Fiedler, R., Fischlin, M., & Janson, C. (2021). BUFFing signature schemes beyond unforgeability and the case of post-quantum signatures. In Proceedings of the 42nd IEEE Symposium on Security and Privacy (S&P'21). IEEE Press.](https://publications.cispa.saarland/3417/)

###### [ChCh21]
[Chen, Ming-Shing, and Tung Chou. "Classic McEliece on the ARM Cortex-M4." IACR Cryptol. ePrint Arch. 2021 (2021): 492.](https://eprint.iacr.org/2021/492.pdf)

###### [ChSe21]
[Cho, J. Y., & Sergeev, A. (2021). Post-quantum MACsec in Ethernet Networks. Journal of Cyber Security and Mobility, 161-176.](https://journals.riverpublishers.com/index.php/JCSANDM/article/view/5973)

###### [CFP+19]
[Casanova, J. C. Faugere, G. M. R. J. Patarin, L. Perret, and J. Ryckeghem.2019. GeMSS: a great multivariate short signature.Submission to NIST PQCcompetition Round-2(2019)](https://www-polsys.lip6.fr/Links/NIST/GeMSS_specification.pdf)

###### [CJL+16]
[L. Chen, S. Jordan, Y. Liu, D. Moody, R. Peralta, R. Perlner, and D. Smith-Tone.2016.Report on post-quantum cryptography. Vol. 12. US Department of Com-merce, National Institute of Standards and Technology](https://nvlpubs.nist.gov/nistpubs/ir/2016/nist.ir.8105.pdf)

###### [CMC19]
[Clancy, T. C., McGwier, R. W., & Chen, L. (2019, May). Post-quantum cryptography and 5G security: tutorial. In Proceedings of the 12th Conference on Security and Privacy in Wireless and Mobile Networks (pp. 285-285).](https://dl.acm.org/doi/abs/10.1145/3317549.3324882?casa_token=_HA-JzKBo2wAAAAA:ulj89qh94VJPx57x5Nm4QekITn3LORo9o76BX27-MV7wfrCkdx3LF1mUn54Byqa20EkQSH9dBDk93Q)

###### [CPS19]
[E. Crockett, C. Paquin, and D. Stebila. Prototyping post-quantum and hybrid key exchange and authentication in TLS and SSH. 2019.](https://csrc.nist.gov/CSRC/media/Events/Second-PQC-Standardization-Conference/documents/accepted-papers/stebila-prototyping-post-quantum.pdf)

###### [CU16]
[L. Chen and R. Urian. Algorithm agility – discussion on TPM 2.0 ECC functionalities. In L. Chen, D. McGrew, and C. Mitchell, editors, Security Standardisation Research, volume 10074, pages 141–159. Springer Inter-national Publishing, 2016](http://link.springer.com/10.1007/978-3-319-49100-4_6.)

###### [DDS+20]
[J. Ding, J. Deaton, K. Schmidt, Vishakha, and Z. Zhang. Cryptanalysis of the Lifted Unbalanced Oil Vinegar Signature Scheme. In D. Micciancio and T. Ristenpart, editors, Advances in Cryptology – CRYPTO 2020, pages 279–298, Cham, 2020. Springer International Publishing.](https://eprint.iacr.org/2019/1490.pdf)

###### [DDW20]
[Z. Ding, J.and Zhang, J. Deaton, and L. Wang. A complete crypt- analysis of the post-quantum multivariate signature scheme himq- 3. In International Conference on Information and Communica- tions Security, pages 422–440. Springer, 2020.](https://link.springer.com/chapter/10.1007%2F978-3-030-61078-4_24)

###### [DFA+20]
[V. Ba Dang, F. Farahmand, M. Andrzejczak, K. Mohajerani, D. T. Nguyen, andK. Gaj. 2020. Implementation and benchmarking of round 2 candidates in the nist post-quantum cryptography standardization process using hardware andsoftware/hardware co-design approaches.Cryptology ePrint Archive: Report2020/795(2020)](https://cryptography.gmu.edu/athena/PQC/GMU_PQC_2020_SW_HW.pdf)

###### [DHP20]
[B. Dowling, T. Brandt Hansen, and K. G. Paterson. Many a Mickle Makes a Muckle: A Framework for Provably Quantum-Secure Hybrid Key Exchange. In PQCrypto 2020, 2020.](https://eprint.iacr.org/2020/099.pdf)

###### [DKL+21]
[L. Ducas, E. Kiltz, T. Lepoint, V. Lyubashevsky, P. Schwabe, G. Seiler, and D.Stehlé. 2021. CRYSTALS-Dilithium Algorithm Specifications and Supporting Documentation.Round-3 submission to the NIST PQC project(8 Feb 2021)](https://pq-crystals.org/dilithium/data/dilithium-specification-round3-20210208.pdf)

###### [DKR+19]
[J. D’Anvers, A. Karmakar, S. Roy, F. Vercauteren, J. Mera, A. Bass, and M. Beirendonck. Saber: Mod-lwr based kem: Round 3 submission. In NIST Post-Quantum Cryptography Standardization: Round 3, 2019](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/files/saberspecround3.pdf)

###### [DLW19]
[X. Dong, Z. Li, and X. Wang. Quantum cryptanalysis on some generalized Feistel schemes. Science China Information Sciences, 62(2):22501, February 2019.](https://eprint.iacr.org/2017/1249.pdf)

###### [DS05]
[J. Ding and D. Schmidt. Rainbow, a new multivariable polynomial signature scheme. In International Conference on Applied Cryptography and Network Security, pages 164–175. Springer, 2005](https://link.springer.com/chapter/10.1007/11496137_12)

###### [FC20]
[Tiago M. Fernández-C. 2020. From Pre-Quantum to Post-Quantum IoT Security:A Survey on Quantum-Resistant Cryptosystems for the Internet of Things.IEEEInternet of Things Journal7, 7 (2020), 6457–6480](https://ieeexplore.ieee.org/document/8932459)

###### [FHK+20]
[P. A. Fouque, J. Hoffstein, P. Kirchner, V. Lyubashevsky, T. Pornin, T. Prest, T.Ricosset, G. Seiler, W. Whyte, and Z. Zhang. 2020. Falcon: Fast-fourier lattice-based compact signatures over NTRU specifications v1. 2.NIST Post-Quantum Cryptography Standardization Round3 (2020)](https://falcon-sign.info/falcon.pdf)

###### [FMK19]
[Fluhrer, S., McGrew, D., Kampanakis, P., & Smyslov, V. (2019). Postquantum preshared keys for IKEv2. Internet Engineering Task Force, Internet-Draft draft-ietf-ipsecme-qr-ikev2-11.](https://datatracker.ietf.org/doc/html/draft-ietf-ipsecme-qr-ikev2-11)

###### [GHK+21]
[Gonzalez, Ruben, Andreas Hülsing, Matthias J. Kannwischer, Juliane Krämer, Tanja Lange, Marc Stöttinger, Elisabeth Waitz, Thom Wiggers, and Bo-Yin Yang. "Verifying Post-Quantum Signatures in 8 kB of RAM." (2021).](https://kannwischer.eu/papers/2021_streamingpqc.pdf)

###### [GKT13]
[R. Gagliano, S. Kent, and S. Turner. Algorithm Agility Procedure for the Resource Public Key Infrastructure (RPKI). Request for Comments. 2013. RFC 6916.](https://tools.ietf.org/html/rfc6916)

###### [GMP21]
[Grubbs, Paul, Varun Maram, and Kenneth G. Paterson. Anonymous, Robust Post-Quantum Public Key Encryption. Cryptology ePrint Archive, Report 2021/708, 2021. h ps://eprint. iacr. org/2021/708. 1, 5, 7, 13, 14, 2021.](https://eprint.iacr.org/2021/708.pdf)

###### [GoKa15]
[Ghosh, S., & Kate, A. (2015, June). Post-quantum forward-secure onion routing. In International Conference on Applied Cryptography and Network Security (pp. 263-286). Springer, Cham](https://ieeexplore.ieee.org/abstract/document/9363165)

###### [Hei19]
[Exchange, Quantum-Resistant Key. "Towards a Verifiably Secure Quantum-Resistant Key Exchange in IKEv2."](https://www.nm.ifi.lmu.de/pub/Diplomarbeiten/heid19/PDF-Version/heid19.pdf)

###### [HKW21]
[H . Hagemeier, S. Kousidis, and T. Wunderer. Standardisierung von post-quanten-kryptografie und empfehlungen des bsi. In German Federal Office for Information Security (BSI), editor, Tagungsband zum 17. Deutschen IT-Sicherheitskongress, page 382–294. SecuMedia Verlag, Ingelheim, Germany, Feb 2021. Note: No direct link available!]

###### [HLL+21]
[T. Hemmert, M. Lochter, D. Loebenberger, M. Margraf, S. Reinhardt, and G.Sigl. 2021. Quantencomputerresistente Kryptografie: Aktuelle Aktivitäten und Fragestellungen. InTagungsband zum 17. Deutschen IT-Sicherheitskongress, German Federal Office for Information Security (BSI) (Ed.). SecuMedia Verlag,Ingelheim, Germany, 367–380](https://www.secumedia-shop.net/Deutschland-Digital-Sicher-30-Jahre-BSI)

###### [HM19]
[S. Udyani H. Mudiyanselage. Next-generation web public-key infrastructure technologies, 2019. doi:10.5204/thesis.eprints.128643.](https://eprints.qut.edu.au/128643)

###### [Hou15]
[R. Housley. Guidelines for Cryptographic Algorithm Agility and Selecting Mandatory-to-Implement Algorithms. RFC 7696, 2015.](https://tools.ietf.org/html/rfc7696)

###### [HPA21]
[Howe, J., Prest, T., & Apon, D. (2021). SoK: How (not) to Design and Implement Post-Quantum Cryptography. IACR Cryptol. ePrint Arch., 2021, 462.](https://eprint.iacr.org/2021/462.pdf)

###### [HPDM20]
[A. Holcomb, G. C. C. F. Pereira, B. Das, and M. Mosca. PQFabric: A Permissioned Blockchain Secure from Both Classical and Quantum Attacks. arXiv:2010.06571](https://arxiv.org/abs/2010.06571)

###### [HNS+20]
[Hülsing, A., Ning, K. C., Schwabe, P., Weber, F., & Zimmermann, R. (2020). Post-quantum WireGuard. IACR Cryptol. ePrint Arch., 2020, 379.](https://eprint.iacr.org/2020/379.pdf)

###### [HZHW20]
[R. Huesmann, A. Zeier, A. Heinemann, and A. Wiesmaier. Zur Benutzbarkeit und Verwendung von API-Dokumentationen. In Christian Hansen, Andreas Nürnberger, and Bernhard Preim, editors, Mensch und Computer 2020 - Workshopband, Bonn, 2020. Gesellschaft für Informatik e.V. doi:10.18420/muc2020-ws119-002.](https://dl.gi.de/handle/20.500.12116/33515)

###### [JS19]
[S. Jaques and J. M. Schanck. Quantum Cryptanalysis in the RAM Model: Claw-Finding Attacks on SIKE. In A. Boldyreva and D. Micciancio, editors, Advances in Cryptology – CRYPTO 2019, volume 11692, pages 32–61. Springer International Publish- ing, 2019.](https://eprint.iacr.org/2019/103.pdf)

###### [KAMJ17]
[B. Koziel, R. Azarderakhsh, M. Mozaffari Kermani, and D. Jao. 2017.   Post-Quantum Cryptography on FPGA Based on Isogenies on Elliptic Curves.IEEETransactions on Circuits and Systems I: Regular Papers64, 1 (Jan. 2017), 86–99](https://ieeexplore.ieee.org/abstract/document/7725935)

###### [Kam+20]
[Panos Kampanakis and Douglas Steblia and Markus Friedl and Torben Hansen and Dimitrios Sikeridis. 2020. Post-quantum public key algorithms for the Secure Shell (SSH) protocol, 0 Internet-Draft. Internet Engineering Task Force](https://datatracker.ietf.org/doc/html/draft-kampanakis-curdle-pq-ssh-00)

###### [KaSi19]
[P. Kampanakis and D. Sikeridis. 2019.Two PQ Signature Use-cases: Non-issues, challenges and potential solutions. Technical Report 1276](https://eprint.iacr.org/2019/1276)

###### [KGC+20]
[V. B. Y. Kumar, N. Gupta, A. Chattopadhyay, M. Kasper, C. Krauß, and R. Nieder-hagen. 2020.  Post-Quantum Secure Boot. In2020 Design, Automation Test inEurope Conference Exhibition (DATE). 1582–1585 doi:10.23919/DATE48585.2020.9116252](https://ieeexplore.ieee.org/document/9116252)

###### [KiKw18]
[Kiefer, Franziskus, and Krzysztof Kwiatkowski. "Hybrid ECDHE-SIDH key exchange for TLS." draft-kiefer-tls-ecdhe-sidh-00 (work in progress) (2018).](https://datatracker.ietf.org/doc/html/draft-kiefer-tls-ecdhe-sidh-00)

###### [KKP20]
[S. Koteshwara, M. Kumar, and P. Pattnaik. 2020. Performance Optimization of Lattice Post-Quantum Cryptographic Algorithms on Many-Core Processors.In2020 IEEE International Symposium on Performance Analysis of Systems andSoftware (ISPASS). 223–225](https://ieeexplore.ieee.org/abstract/document/9238630)

###### [KKS+20]
[A. Kuznetsov, A. Kiian, O. Smirnov, A. Cherep, M. Kanabekova, and I. Chepurko. Testing of Code-Based Pseudorandom Num- ber Generators for Post-Quantum Application. In 2020 IEEE 11th International Conference on Dependable Systems, Services and Technologies (DESSERT), pages 172–177, 2020.](https://www.researchgate.net/publication/342456148_Testing_of_Code-Based_Pseudorandom_Number_Generators_for_Post-Quantum_Application)

###### [KOV+18]
[A. Khalid, T. Oder, F. Valencia, M. O’ Neill, T. Güneysu, and F. Regazzoni. Physical protection of lattice-based cryptography: Challenges and solutions. In Proceedings of the 2018 on Great Lakes Symposium on VLSI, pages 365–370. ACM, 2018.](https://pure.qub.ac.uk/files/156772945/paper.pdf)

###### [KPDG18]
[P. Kampanakis, P. Panburana, E. Daw, and D. Van Geest. 2018.  The Viability of Post-quantum X.509 Certificates.IACR Cryptol. ePrint Arch.2018 (2018)](http://eprint.iacr.org/2018/063)

###### [KSL+19]
[K. Kwiatkowski, N. Sullivan, A. Langley, D. Levin, and A. Mislove. 2019. Measuring TLS key exchange with post-quantum KEM. InWorkshop Record of the SecondPQC Standardization Conference](https://csrc.nist.gov/CSRC/media/Events/Second-PQC-Standardization-Conference/documents/accepted-papers/kwiatkowski-measuring-tls.pdf)

###### [KV19]
[K. Kwiatkowski and L. Valenta. The TLS Post-Quantum Experiment, October 2019.](https://blog.cloudflare.com/the-tls-post-quantum-experiment/)

###### [KwVa19]
[K. Kwiatkowski and L. Valenta. 2019. The TLS Post-Quantum Experiment](https://blog.cloudflare.com/the-tls-post-quantum-experiment/)

###### [Lang19]
[A. Langley. 2019. Real-world measurements of structured-lattices and supersin-gular isogenies in TLS](https://www.imperialviolet.org/2019/10/30/pqsivssl.html)

###### [LLP+13]
[K. Lee, Y. Lee, J. Park, K. Yim and I. You, "Security Issues on the CNG Cryptography Library (Cryptography API: Next Generation)," 2013 Seventh International Conference on Innovative Mobile and Internet Services in Ubiquitous Computing, 2013, pp. 709-713, doi: 10.1109/IMIS.2013.128](https://ieeexplore.ieee.org/document/6603762)

###### [LLJ20]
[Lohachab, A., Lohachab, A., & Jangra, A. (2020). A comprehensive survey of prominent cryptographic aspects for securing communication in post-quantum IoT networks. Internet of Things, 9, 100174.](https://www.sciencedirect.com/science/article/abs/pii/S2542660520300159#!)

###### [MAA+20]
[D. Moody, G. Alagic, D. C Apon, D. A. Cooper, Q. H. Dang, J. M. Kelsey, Y.Liu, C. A. Miller, R. C. Peralta, R. A. Perlner, A. Y. Robinson, D. C. Smith-Tone,and J. Alperin-Sheriff. 2020.  Status report on the second round of the NISTpost-quantum cryptography standardization process.](https://doi.org/10.6028/NIST.IR.8309)

###### [MAB+21]
[C. Aguilar Melchor, N. Aragon, S. Bettaieb, L. Bidoux, O. Blazy, J. C. Deneuville, P. Gaborit, E. Persichetti, G. Zémor, and I. C. Bourges. Hamming quasi-cyclic (hqc). NIST PQC Round, 3, 2021](http://pqc-hqc.org/doc/hqc-specification_2021-06-06.pdf)

###### [Mas02]
[S. Maseberg. Fail-Safe-Konzept für Public-Key-Infrastrukturen. PhDthesis, 2002.](http://tuprints.ulb.tu-darmstadt.de/246/)

###### [MdJvH+20]
[M. Müller, J. de Jong, M. van Heesch, B. Overeinder, and R. van Rijswijk-Deij. Retrofitting post-quantum cryptography in internet protocols: a case study of DNSSEC. 50(4):49–57, 2020. doi:10.1145/3431832.3431838.](https://dl.acm.org/doi/10.1145/3431832.3431838)

<!-- ###### [Meun21]
[Robin Meunier. Post-Quantum Kryptographie - Classic McEliece. 2021 Master Thesis. Darrmstadt University of Applied Sciences. Faculty of Computer Science.](/docs/migration/McEliece.pdf) -->

###### [MPD+18]
[L. Malina, L. Popelova, P. Dzurenda, J. Hajny, and Z. Martinasek. 2018. On Feasibility of Post-Quantum Cryptography on Small Devices (15th IFAC Conference on Programmable Devices and Embedded Systems PDeS 2018), Vol. 51. 462–467](https://www.sciencedirect.com/science/article/pii/S2405896318308474)

###### [MRD+20]
[L. Malina, S. Ricci, P. Dzurenda, D. Smekal, J. Hajny, and T. Gerlich. 2020. Towards Practical Deployment of Post-quantum Cryptography on Constrained Platforms and Hardware-Accelerated Platforms. In Innovative Security Solutions for Information Technology and Communications. Springer International Publishing, 109–124](https://link.springer.com/chapter/10.1007/978-3-030-41025-4_8)

###### [MW18]
[K. Mindermann and S. Wagner. Usability and Security Effects of Code Examples on Crypto APIs. In 2018 16th Annual Conference on Privacy, Security and Trust (PST), pages 1–2. IEEE, August 2018.](https://ieeexplore.ieee.org/document/8514203)

###### [MW20]
[K. Mindermann and S. Wagner. 2020. Fluid Intelligence Doesn’t Matter! Effectsof Code Examples on the Usability of Crypto APIs. In2020 IEEE/ACM 42ndInternational Conference on Software Engineering: Companion Proceedings (ICSE-Companion). 306–307.](https://arxiv.org/abs/2004.03973)

###### [NAB+21]
[M. Naehrig, E. Alkim, J. W Bos, L. Ducas, K. Easterbrook, B. LaMacchia, P. Longa,I. Mironov, V. Nikolaenko, C. Peikert, et al.2020. Frodokem learning with errorskey encapsulation.NIST PQC Round3 (2021)](https://frodokem.org/files/FrodoKEM-specification-20210604.pdf)

###### [NIWA17]
[Niederhagen, R., & Waidner, M. (2017). Practical Post-Quantum Cryptography. Fraunhofer White Paper, vol. ISSN, 2192-8169](https://www.sit.fraunhofer.de/fileadmin/dokumente/studien_und_technical_reports/Practical.PostQuantum.Cryptography_WP_FraunhoferSIT.pdf?_=1503992279)

###### [NWAO19]
[M. D. Noel, O. V. Waziri, M. S. Abdulhamid, and A. J. Ojeniyi. Stateful hash-based digital signature schemes for bitcoin cryptocurrency. In 2019 15th International Conference on Electronics, Computer and Computation (ICECCO), pages 1–6, 2019.](https://ieeexplore.ieee.org/document/9043192/)

###### [NWE19]
[A. Neish, T. Walter, and P. Enge. Quantum-resistant authentication algorithms for satellite-based augmentation systems. Navigation, 66(1):199–209, 2019.](https://web.stanford.edu/group/scpnt/gpslab/pubs/papers/Neish_2018_IONITM_QuantumResistantAuthenticationUpdated.pdf)

###### [OHW+18]
[M. O’Neill, S. Heidbrink, J. Whitehead, T. Perdue, L. Dickinson, T. Collett, N. Bonner, K. Seamons, and D. Zappala. The Secure Socket API: TLS as an Operating System Service. In 27th USENIX Security Symposium (USENIX Security 18), pages 799–816. USENIX Association, 2018.](https://www.usenix.org/conference/usenixsecurity18/presentation/oneill)

###### [OP20]
[M. Ounsworth and M. Pala. Composite Keys and Signatures For Use In Internet PKI. Internet-Draft-ounsworth-pq-composite-sigs-03, Internet Engineering Task Force, July 2020. Backup Publisher: Internet Engineering Task Force Num Pages: 18](https://tools.ietf.org/id/draft-ounsworth-pq-composite-sigs-01.html)

###### [OPP19]
[D. Ott, C. Peikert, and participants. 2019.   Identifying Research Challengesin Post Quantum Cryptography Migration and Cryptographic Agility.  (Sept.2019).](https://cra.org/crn/2019/10/research-challenges-in-post-quantum-cryptography-migration-and-cryptographic-agility/)

###### [PASC20]
[S. Paul and P. Scheible. 2020. Towards Post-Quantum Security for Cyber-Physical Systems: Integrating PQC into Industrial M2M Communication. InComputerSecurity – ESORICS 2020. Vol. 12309. Springer International Publishing, 295–316](https://link.springer.com/chapter/10.1007/978-3-030-59013-0_15)

###### [PN19]
[S. Paul and M. Niethammer. On the importance of cryptographic agility for industrial automation. at - Automatisierungstechnik, 67(5):402–416, May 2019, doi:10.1515/auto-2019-0019](http://www.degruyter.com/view/j/auto.2019.67.issue-5/auto-2019-0019/auto-2019-0019.xml)

###### [PRKK19]
[S. Pugh, M. S. Raunak, D. R. Kuhn, and R. Kacker. Systematic Testing of Post-Quantum Cryptographic Implementations Using Metamorphic Testing. In 2019 IEEE/ACM 4th International Workshop on Metamorphic Testing (MET), pages 2–8, Montreal, QC, Canada, May 2019. IEEE, doi:10.1109/MET.2019.00009.](https://ieeexplore.ieee.org/document/8785645/)

###### [PS20]
[S. Paul and P. Scheible. Towards post-quantum security for cyber-physical systems: Integrating PQC into industrial m2m communica tion. In L. Chen, N. Li, K. Liang, and S. Schneider, editors, Computer Security – ESORICS 2020, volume 12309, pages 295–316. Springer In ternational Publishing, 2020. Series Title: Lecture Notes in Computer Science. doi:10.1007/978-3-030-59013-0\_15.](http://link.springer.com/10.1007/978-3-030-59013-0_15)

###### [PST13]
[R. Perlner and D. Smith-Tone. A classification of differential invariants for multivariate post-quantum cryptosystems. In P. Gaborit, editor, Post- Quantum Cryptography, volume 7932, pages 165–173. Springer Berlin Heidelberg, 2013. Series Title: Lecture Notes in Computer Science. doi:10.1007/ 978-3-642-38616-9_11](http://link.springer.com/10.1007/978-3-642-38616-9_11)

###### [PST19]
[C. Paquin, D. Stebila, and G. Tamvada. 2019.Benchmarking Post-QuantumCryptography in TLS. Technical Report 1447] (http://eprint.iacr.org/2019/1447)

###### [ScSt17]
[Schanck, John M., and Douglas Stebila. "A Transport Layer Security (TLS) extension for establishing an additional shared secret." IETF Draft (2017).](https://datatracker.ietf.org/doc/html/draft-schanck-tls-additional-keyshare-00)

###### [SFG20]
[Steblia, D., Fluhrer, S. and Gueron, S., 2020. Hybrid key exchange in TLS 1.3. Internet-Draft draft-ietf-tls-hybrid-design-02. Internet Engineering Task Force.](https://datatracker.ietf.org/doc/html/draft-ietf-tls-hybrid-design)

###### [SKD20]
[D. Sikeridis, P. Kampanakis, and M. Devetsikiotis. 2020. Post-Quantum Authentication in TLS 1.3: A Performance Study. Technical Report 071](http://eprint.iacr.org/2020/071)

###### [SM16]
[D. Stebila and M. Mosca. Post-quantum Key Exchange for the Internet and the Open Quantum Safe Project. In R. Avanzi and H. Heys, editors, Selected Areas in Cryptography – SAC 2016, Lecture Notes in Computer Science, pages 14–37, Cham, 2016. Springer International Publishing. doi:10.1007/978-3-319-69453-5\_2.](https://eprint.iacr.org/2016/1017.pdf)

###### [SSPB19]
[S. Samardjiska, P. Santini, E. Persichetti, and G. Banegas. A reaction attack against cryptosystems based on LRPC codes. In P. Schwabe and N. Thériault, editors, Progress in Cryptology – LATINCRYPT 2019, pages 197–216. Springer International Pub- lishing, 2019.](https://eprint.iacr.org/2019/845.pdf)

###### [SSW20]
[Peter Schwabe, Douglas Stebila, and Thom Wiggers. 2020. Post-Quantum TLS Without Handshake Signatures. In Proceedings of the 2020 ACM SIGSAC Conference on Computer and Communications Security (CCS '20). Association for Computing Machinery, New York, NY, USA, 1461–1480. DOI:https://doi.org/10.1145/3372297.3423350](https://eprint.iacr.org/2020/534)

###### [StMo16]
[D. Stebila and M. Mosca. 2016.  Post-quantum Key Exchange for the Internet and the Open Quantum Safe Project. In Selected Areas in Cryptography – SAC2016, R. Avanzi and H. Heys (Eds.). Springer International Publishing, 14–37](https://doi.org/10.1007/978-3-319-69453-5_2)

###### [SWZ16]
[Schanck, John M., William Whyte, and Zhenfei Zhang. "Quantum-safe hybrid (QSH) ciphersuite for Transport Layer Security (TLS) version 1.2." IETF, Internet-Draft draft-whyte-qsh-tls (2016)](https://datatracker.ietf.org/doc/html/draft-whyte-qsh-tls12-02)

###### [TKY21]
[Chou, Tung, Matthias J. Kannwischer, and Bo-Yin Yang. "Rainbow on Cortex-M4." IACR Cryptol. ePrint Arch. 2021 (2021): 532.](https://kannwischer.eu/papers/2021_rainbowm4.pdf)

###### [TLW19]
[J. Tian, J. Lin, and Z. Wang. 2019. Ultra-Fast Modular Multiplication Implementa-tion for Isogeny-Based Post-Quantum Cryptography. In2019 IEEE InternationalWorkshop on Signal Processing Systems (SiPS). 97–102 doi:10.1109/SiPS47522.2019.9020384](https://ieeexplore.ieee.org/document/9020384)

###### [TRH+20]
[Tujner, Z., Rooijakkers, T., van Heesch, M., & Önen, M. (2020). QSOR: Quantum-Safe Onion Routing. arXiv preprint arXiv:2001.03418](https://arxiv.org/abs/2001.03418)

###### [Unr20]
[Unruh, D. (2020, December). Post-quantum verification of Fujisaki-Okamoto. In International Conference on the Theory and Application of Cryptology and Information Security (pp. 321-352). Springer, Cham.](https://link.springer.com/chapter/10.1007/978-3-030-64837-4_11)

###### [UWK15]
[M. Ullmann, C. Wieschebrink, and D. Kügler. Public key infrastructure and crypto agility concept for intelligent transportation systems. In Sulc, Noll (Eds.): VEHICULAR 2015: The Fourth International Conference on Advances in Vehicular Systems, Technologies and Applications. October 11-16, 2015, St. Julians, Malta, pages 14 – 19, 2015.](http://www.thinkmind.org/index.php?view=article&articleid=vehicular_2015_1_30_30028.)

###### [vHAA+19]
[van Heesch, M., van Adrichem, N. L., Attema, T., & Veugen, T. (2019). Towards Quantum-Safe VPNs and Internet. IACR Cryptol. ePrint Arch., 2019, 1277.](https://eprint.iacr.org/2019/1277.pdf)

###### [VBDK+20]
[M. Van Beirendonck, J. P. D’Anvers, A. Karmakar, J. Balasch, and I. Verbauwhede. A side-channel resistant implementation of saber. IACR Cryptol. ePrint Arch, 733, 2020.](https://eprint.iacr.org/2020/733.pdf)

###### [VM12]
[V. Vasić and M. Mikuc. Security Agility Solution Independent of the Underlaying Protocol Architecture. In AT, 918 of CEUR Workshop Proceedings, pages 128–137. CEUR-WS.org, 2012.](https://www.semanticscholar.org/paper/Security-Agility-Solution-Independent-of-the-Vasic-Mikuc/489054a1f28eb26b1baa1a9f0caff2306c821695.)

###### [WAG+21]
[Wiesmaier, A., Alnahawi, N., Grasmeyer, T., Geißler, J., Zeier, A., Bauspieß, P., & Heinemann, A. (2021). On PQC Migration and Crypto-Agility. arXiv preprint arXiv:2106.09599.](https://arxiv.org/abs/2106.09599)

###### [WaSt20]
[Wang, W., & Stöttinger, M. (2020). Post-Quantum Secure Architectures for Automotive Hardware Secure Modules. IACR Cryptol. ePrint Arch., 2020, 26](https://eprint.iacr.org/2020/026.pdf)

###### [WiSo21]
[Barker, William, and Murugiah Souppaya. [Project Description] Migration to Post-Quantum Cryptography (Draft). National Institute of Standards and Technology, 2021.](https://www.nccoe.nist.gov/sites/default/files/library/project-descriptions/pqc-migration-project-description-draft.pdf)

###### [Wh+17]
[Whyte, William, et al. "Quantum-safe hybrid (QSH) key exchange for Transport Layer Security (TLS) version 1.3." IETF Draft (2017)](https://datatracker.ietf.org/doc/html/draft-whyte-qsh-tls13-06)

###### [WvdG20]
[D. Weller and R. van der Gaag. 2020. Incorporating post-quantum cryptographyin a microservice environment. (2020), 36](https://homepages.staff.os3.nl/~delaat/rp/2019-2020/p13/report.pdf)

###### [Xag21]
[Keita Xagawa. NTRU leads to Anonymous, Robust Public-Key Encryption. Cryptology ePrint Archive, Report 2021/741. 2021](https://eprint.iacr.org/2021/741)

###### [YXF+18]
[Yuan, Y., Xiao, J., Fukushima, K., Kiyomoto, S., & Takagi, T. (2018). Portable Implementation of Postquantum Encryption Schemes and Key Exchange Protocols on JavaScript-Enabled Platforms. Security and Communication Networks, 2018.](https://www.hindawi.com/journals/scn/2018/9846168/)

###### [Zei20]
[A. Zeier. 08.12.2020. eucrite 1.0 API.](https://use-a-pqclib.h-da.io/eucrite-documentation/)

###### [ZGF20]
[D. Zoni, A. Galimberti and W. Fornaciari, "Efficient and Scalable FPGA-Oriented Design of QC-LDPC Bit-Flipping Decoders for Post-Quantum Cryptography," in IEEE Access, vol. 8, pp. 163419-163433, 2020, doi: 10.1109/ACCESS.2020.3020262.](https://ieeexplore.ieee.org/document/9180360)

###### [Zim15]
[Zimmer, Dipl-Inf Ephraim. "Post-Quantum Kryptographie für IPsec." Sicherheit in vernetzten Systemen-22. DFN-Konferenz. Ed. by Christian Paulsen. DFN-CERT. 2015.](https://svs.informatik.uni-hamburg.de/publications/2015/2015-02-24-Zimmer-DFN-PQC-fuer-IPsec.pdf)

###### [ZWH19]
[A. Zeier, A. Wiesmaier, and A. Heinemann. API Usability of Stateful Signature Schemes. In The 14th International Workshop on Security (IWSEC), LNCS 11689, pages 1–20. Springer Switzerland, August 2019](https://link.springer.com/chapter/10.1007/978-3-030-26834-3_13)

###### [ZWH21]
[A. Zeier, A. Wiesmaier, and A. Heinemann. Zur Integration von Post-Quantum Verfahren in bestehende Softwarepodukte. In German Federal Office for Information Security (BSI), editor, Tagungsband zum 17. Deutschen IT-Sicherheitskongress, pages 381 – 391. SecuMedia Verlag, Ingelheim, Germany, March 2021.](https://arxiv.org/pdf/2102.00157v1)

###### [ZYD+20]
[F. Zhang, B. Yang, X. Dong, S. Guilley, Z. Liu, W. He, F. Zhang, and K. Ren. Side-Channel Analysis and Countermeasure Design on ARM- based Quantum-Resistant SIKE. IEEE Transactions on Computers, pages 1–1, 2020. Conference Name: IEEE Transactions on Computers. doi: 10.1109/TC.2020.3020407.](https://ieeexplore.ieee.org/document/9181442)
