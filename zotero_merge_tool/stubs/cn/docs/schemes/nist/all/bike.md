---
title: "BIKE"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 1
---
[BIKE](https://bikesuite.org/) 是一种基于准循环低密度奇偶校验法的比特翻转密钥封装算法。[Specs.](https://bikesuite.org/files/v4.1/BIKE_Spec.2020.10.22.1.pdf)
