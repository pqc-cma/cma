---
title: "NTRU"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 9
---
[NTRU](https://ntru.org/) 是基于格最短向量问题的公钥加密机制. [Specs.](https://ntru.org/f/ntru-20190330.pdf)

- 为确定性所必需的参数：
	- $n$：正整数
  - $q$：正整数

- 非必需参数：
  - $p$：正整数（一般是$3$）
