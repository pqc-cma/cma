---
title: "CRYSTALS-Dilithium"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 2
---
[CRYSTALS-Dilithium](https://pq-crystals.org/dilithium/) 是基于模格上最短向量问题困难性的数字签名算法。[Specs.](https://pq-crystals.org/dilithium/data/dilithium-specification-round3-20210208.pdf)

- 为确定性所必需的参数：
  - $\tau$：正整数

- 并非必需的参数：
  - $q$：正整数（一般是$8380417$）
  - $d$：正整数（一般是$13$）
  - $\gamma_1$：正整数
  - $\gamma_2$：正浮点数
  - $(k,l)$：正整数
  - $\eta$：正整数
