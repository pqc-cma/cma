---
title: "Falcon"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 3
---
[Falcon](https://falcon-sign.info/) is a lattice-based signature scheme based on the short integer solution problem (SIS) over NTRU lattices. [Specs.](https://falcon-sign.info/falcon.pdf)

- 为确定性所必需的参数：
  - $n$：正整数

- 为确定性所必需的参数：
  - $q$：正整数 (一般是 $12289$)
  - $\sigma$：正浮点数
  - $\sigma_{min}$：正浮点数
  - $\sigma_{max}$：正浮点数
  - $\lfloor \beta^{2} \rfloor$：正整数
