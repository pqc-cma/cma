mandatory_files = ['docs/agility/agility.md', 'docs/evaluation/hardware/automotive.md', 'docs/evaluation/hardware/embedded.md', 'docs/evaluation/hardware/fpga.md', 'docs/evaluation/hardware/general.md', 'docs/evaluation/hardware/iot.md', 'docs/evaluation/hardware/micro.md', 'docs/evaluation/software/all/blockchain.md', 'docs/evaluation/software/all/dnssec.md', 'docs/evaluation/software/all/dtls.md', 'docs/evaluation/software/all/eid.md', 'docs/evaluation/software/all/ikev2.md', 'docs/evaluation/software/all/macsec.md', 'docs/evaluation/software/all/others.md', 'docs/evaluation/software/all/pap.md', 'docs/evaluation/software/all/pgp.md', 'docs/evaluation/software/all/pki.md', 'docs/evaluation/software/all/pkinit.md', 'docs/evaluation/software/all/rtp.md', 'docs/evaluation/software/all/sftp.md', 'docs/evaluation/software/all/signal.md', 'docs/evaluation/software/all/smime.md', 'docs/evaluation/software/all/ssh.md', 'docs/evaluation/software/all/sso.md', 'docs/evaluation/software/all/tls.md', 'docs/evaluation/software/all/tor.md', 'docs/evaluation/software/all/vpn.md', 'docs/evaluation/software/all/wlan.md', 'docs/general.md', 'docs/migration/general.md', 'docs/migration/standards.md', 'docs/migration/testing.md', 'docs/refs.md', 'docs/schemes/code.md', 'docs/schemes/other.md', 'docs/schemes/hash.md', 'docs/schemes/isogeny.md', 'docs/schemes/lattice.md', 'docs/schemes/multivariate.md', 'docs/security/analysis.md', 'docs/security/sca.md']

notes_field_name = 'annote'
stubs_folder = 'stubs'

languages = ['en', 'cn']

entrytemplate = '- [{0}]({1}): {2} [[{3}]]({4}#{5})\n'

refstemplate = '###### [{0}]\n[{1}]({2})\n\n'

titles = ['title1:', 'title2:']

titletemplate = '\n##### **{0}**\n'

