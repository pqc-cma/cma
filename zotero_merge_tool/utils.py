from conf import *
import os
import sys
import re

from bibtexparser.customization import *

def check_entry_fields(entry):
    if not notes_field_name in entry:
        print("BibTex Entry does not have field '" + notes_field_name + "':\n\n")
        print("Make sure your config file does use the correct Note field name for your bibtex format.")
        print(entry)
        sys.exit(1)
    if not "title" in entry:
        print("BibTex Entry does not have field 'title':\n\n")
        print(entry)
        sys.exit(1)
    if not "url" in entry:
        print("BibTex Entry does not have field 'url':\n\n")
        print(entry)
        sys.exit(1)
    if not "year" in entry:
        print("BibTex Entry does not have field 'year':\n\n")
        print(entry)
        sys.exit(1)
    if not "author" in entry:
        print("BibTex Entry does not have field 'author':\n\n")
        print(entry)
        sys.exit(1)

    for lang in languages:
        if not "description:{}:".format(lang) in entry[notes_field_name]:
            print("BibTex Entry does not have description text for language: " + lang + "\n\n")
            print(entry)
            sys.exit(1)

    if not any(('file:' in x for x in entry["keywords"].split(', '))):
        print("BibTex Entry does not have a file tag:\n\n")
        print(entry)
        sys.exit(1)



# bibtex parsing helper function
def customizations(record):
    record = author(record)
    record = add_plaintext_fields(record)
    return record

# fix annote bug: merge all annote fields
def clean_bib(bibtexfile):
    with open(bibtexfile) as bibfile:
        bibstring = bibfile.read()
    original_bibstring = bibstring
    n = 1
    while n != 0:
        regex = '(^\\s+{0} = (.(?!}},\\n}}\\n))*?)}},?\\n\\s+{0} = {{'.format(notes_field_name)
        bibstring, n = re.subn(regex, r'\1;;', bibstring, flags=re.DOTALL | re.MULTILINE)

    if len(bibstring) == len(original_bibstring):
        print("Annote Fix did not work")
        sys.exit(1)

    return bibstring

# create a dictionary with mandatory filename as key and stub + outputfilename tuple as value
def create_dict(mandatory_files, directory, stubs_folder, lang):

    # list all output directory .md files
    md_files = []
    for (dirname, subshere, fileshere) in os.walk(directory):
        for filename in fileshere:
            if '.md' in filename and os.path.sep + lang + os.path.sep in dirname:
                md_files.append(os.path.join(dirname, filename))

    # list all existing stub files
    stub_files = []
    for (dirname, subshere, fileshere) in os.walk(os.path.join(stubs_folder, lang)):
        for filename in fileshere:
            stub_files.append(os.path.join(dirname, filename))
        
    file_dict = {}
    for mfile in mandatory_files:
        dict_md_files = [x for x in md_files if mfile in x]

        if len(dict_md_files) == 0:
            print('Missing corresponding md_file in output directory for:', mfile)
            sys.exit(1)
        elif len(dict_md_files) > 1:
            print('More than 1 corresponding md_file in output directory for:',  mfile)
            sys.exit(1)

        dict_stub_files = [x for x in stub_files if mfile in x]

        if len(dict_stub_files) == 0:
            print('Missing corresponding stub_file in stub_folder for: ',  mfile)
            sys.exit(1)
        elif len(dict_stub_files) > 1:
            print('More than 1 corresponding stub_file in stub_folder for:', mfile)
            sys.exit(1)

        file_dict[mfile] =  (dict_md_files[0], dict_stub_files[0])

    return file_dict


def generate_abbrev(entry):
    # abbreviaton, shorten to 3 letters
    abbrev = [x[0] for x in getnames(entry["plain_author"])]
    if len(abbrev) > 3:
        abbrev = abbrev[0:3]
        abbrev.append('+')
    abbrev += entry["year"][-2:]
    return ''.join(abbrev)

def generate_refs(bibdata, mfile, values):
    with open(values[1]) as refs_stub, open(values[0], 'w') as refs_output:
        refs_output.write(refs_stub.read())
        refs_dict = {}
        for entry in bibdata.entries:
            abbrev = generate_abbrev(entry)

            # name strings are seperated by comma by this function
            # we remove it so that each full name can be seperated with a comma
            # from the next one

            namelist = getnames(entry["plain_author"])
            for i in range(0, len(namelist)):
                namelist[i] = namelist[i].replace(',', '')

            refs_dict[abbrev] = refstemplate.format(abbrev, ', '.join(namelist) + '. ' + entry["year"] + '. ' + entry["plain_title"], entry["plain_url"])
        for key in sorted(refs_dict):
            refs_output.write(refs_dict[key])


def generate_entry(entry, lang, file_dict, mfile):

    description = get_annote(entry, ';;', lang)
    title = entry["plain_title"]
    # We cannot use '+' in entry links
    abbrev = generate_abbrev(entry).replace('+', '')
    url = entry["plain_url"]
    rel_path = os.path.relpath(file_dict['docs/refs.md'][0], file_dict[mfile][0])

    ref_entry = entrytemplate.format(title, url, description, abbrev, rel_path.replace('.md',''), abbrev.lower())
    return ref_entry

def get_annote(entry, sep, lang):
    annote_list = entry["plain_" + notes_field_name].split(sep)
    key = ''.join(('description:', lang, ':'))
    for x in annote_list:
        if key in x:
            # return x without whitespace and 
            # remove the description identifier
            return x.replace(key, '').strip()

def generate_entry_dict(bibdata, mandatory_files):
    entry_dict = {}

    for mfile in mandatory_files:
        entry_dict[mfile] = []

    for entry in bibdata.entries:
        keywords = entry["keywords"].split(', ')
        for mfile in mandatory_files:
            for keyword in keywords:
                if keyword.replace('file:', '') in mfile:
                    entry_dict[mfile].append(entry)
                    break

    return entry_dict


def process_entries(entry_list, lang, file_dict, mfile):

    output_list = []

    for entry in entry_list:
        output_list.append(generate_entry(entry, lang, file_dict, mfile))

    return output_list
