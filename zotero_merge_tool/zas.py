"""
This is "zotero automation script" using python3 and
bibtexparser:

https://bibtexparser.readthedocs.io

"""

from utils import *

import bibtexparser
from bibtexparser.bwriter import BibTexWriter

import argparse
import os

argparser = argparse.ArgumentParser()
argparser.add_argument('-d', '--directory', help='output directory', required=True)
argparser.add_argument('-b', '--bibtex', help='BibTexfile', required=True)
args = argparser.parse_args()

directory = args.directory
bibtexfile = args.bibtex

""" 
create a dictionary of dictonaries for each language
"""

lang_dict = {}
for lang in languages:
    lang_dict[lang] = create_dict(mandatory_files, directory, stubs_folder, lang)

"""
cleanup the plain .bib file
We need this because Zotero generated multiple 'annote' fields which contain the 
reference description and bibtexparser does only parse one of those fields. 
The simple solutions is to remove all fields that do not contain the 'description:' string
"""

bibstring = clean_bib(bibtexfile)


# start bibtex parsing
parser = bibtexparser.bparser.BibTexParser(common_strings=True)
parser.ignore_nonstandard_types = False
parser.customization = customizations
bibdata = bibtexparser.loads(bibstring, parser=parser)

"""
check every bibtex entry for the fields: 
- annote
- title
- url
- author
- year
- description text for every language in annote field

abort the program if any of those fields are not found
"""

for entry in bibdata.entries:
    check_entry_fields(entry)


"""
To minimize the amount of times that our script will parse over the bibfile entries
we generate a dict with mfile as key and all the entries which contain that mfile in its keywords
as values
"""

entry_dict = generate_entry_dict(bibdata, mandatory_files)

"""
for every mandatory file in the config list, open its corresponding stub file
in the 'stubs' folder and append every generated bibtex entry to it that has the mandatory
filename tag in the keywords field.
"""

for lang, file_dict in lang_dict.items():
    for mfile, values in file_dict.items():
        # special handling of refs
        if 'refs.md' in mfile:
            generate_refs(bibdata, mfile, values)
            continue

        # contains titles, entries grouped under them and entries without titles in sorted order
        output_list = process_entries(entry_dict[mfile], lang, file_dict, mfile)

        with open(values[1]) as stub, open(values[0], "w") as output_file:
            # write stub first
            output_file.write(stub.read())

            # now write the rest of the file
            output_file.write(''.join(sorted(output_list)))
