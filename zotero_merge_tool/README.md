zas: zotero automation script

Preliminaries:

- python3
- bibtexparser (https://bibtexparser.readthedocs.io)

Usage:

`python3 zas.py -d path/to/cma-directory -b path/to/bibtexfile`


Important Notes:

- bibtexfile has to have the following entries for each references:
  - annote field starting with 'description:' directly followed by language e.g. 'en:' (named 'Notes' in Zotero)
  - keyword field starting with 'file:' references the final filename (named 'Tags' in Zotero)
  - title field
  - author field
  - year field
  - url field

- for every file that has to be generated, a corresponding entry in the config file (config.py)
and a corresponding stub file in the stubs folder has to exist with the same name. it does not matter where 
in the directory path this file resides but the name has to be unique!

- if for any of the mandatory filenames either the target directory or the stubs folder
don't contain a corresponding file, the program aborts with an error message.

- for every language subdirectory there has to be an entry in the config language list aswell as all the above preliminaries. 


Example:

To generate all the entries (plus any new ones) for the file 'related.md' there has to be

- a bibtex with all the needed fields, tags and keywords.
- a single file for each language with that name in our output directory.
- a string named 'related.md' in our mandatory file list in config.py
- a stub file in each language folder inside the stub folder. 
