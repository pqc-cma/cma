# Download base image
FROM registry.gitlab.com/pages/hugo/hugo_extended

LABEL description="Hugo build environment"

RUN apk upgrade
RUN apk add --update npm nodejs python3 git bash vim nano
RUN npm udpate
RUN npm install -D --save autoprefixer postcss-cli postcss-cli markmap-lib markmap-cli

COPY . .

RUN chmod +x ./docker_build.sh

EXPOSE 1313 80

ENTRYPOINT ["./docker_build.sh"]
