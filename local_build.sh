#!/bin/sh

# Ruquirements:
# sh npm nodejs python3

HOST=''
MINDMAP_OUT_PATH='./layouts/shortcodes/mindmap.html'
MINDMAP_IN_PATH='./mindmap_gen/mindmap.md'
MINDMAP_SCRIPT_PATH='./mindmap_gen/mindmap_gen.py'
CONTENT_PATH='./content/en'

npm update
npm install -D --save autoprefixer postcss-cli postcss-cli markmap-lib markmap-cli

python3 $MINDMAP_SCRIPT_PATH -d $CONTENT_PATH
npx markmap-cli -o $MINDMAP_OUT_PATH $MINDMAP_IN_PATH
sed -i 's/100v/60v/g' $MINDMAP_OUT_PATH

hugo serve --bind=$HOST
