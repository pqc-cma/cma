[![pipeline status](https://gitlab.com/pqc-cma/cma/badges/dev/pipeline.svg)](https://gitlab.com/pqc-cma/cma/-/commits/dev)

---

[Hugo][] [Docsy][] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [Preview your site](#preview-your-site)


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

If you don't have your own GitLab-Runner, you may have problems running the CI/CD pipeline on forked merge requests. To test your modifications locally follow the steps below!

## Building and running locally from scratch

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project (Make sure to also recursively clone the submodules)
2. [Install][] Hugo extended version
3. Install dependencies required for npm
  - `npm update`
  - `npm install -D --save autoprefixer postcss-cli markmap-lib markmap-cli`
4. Run mindmap generation script (note path variables below)
  - `python3 $MINDMAP_SCRIPT_PATH -d $CONTENT_PATH`
  - `npx markmap-cli -o $MINDMAP_OUT_PATH $MINDMAP_IN_PATH`
  - `sed -i 's/100v/60v/g' $MINDMAP_OUT_PATH`
6. Preview your project on local host using the command `hugo server` on `localhost:1313`
7. Add or edit content etc.
8. Generate the website locally (optional) with the command `hugo --minify --gc --destination public`

### Path Variables
- `MINDMAP_OUT_PATH='./layouts/shortcodes/mindmap.html'`
- `MINDMAP_IN_PATH='./mindmap_gen/mindmap.md'`
- `MINDMAP_SCRIPT_PATH='./mindmap_gen/mindmap_gen.py'`
- `CONTENT_PATH='./content/en'`

Read more at Hugo's [documentation][].

## Running locally with sh script

1. Fork, clone or download this project (Make sure to also recursively clone the submodules)
2. [Install][] Hugo extended version
3. Run local_build.sh with admin privileges
6. Preview your project on local host `localhost:1313`
7. Add or edit content etc.

## Running with Docker

1. Fork, clone or download this project (Make sure to also recursively clone the submodules)
2. Build the docker image using the Dockerfile with the command `sudo docker build -t cma:latest .`
3. Run the Docker image in a container with the command `sudo docker run -it -p 8080:1313 cma:latest`
4. Preview your project on local host `localhost:8080`
4. Add or edit content inside the docker container (connect to running container in a separate terminal)

Read more at [docker][].

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[docker]: https://docs.docker.com/
[docsy]: https://github.com/google/docsy/
