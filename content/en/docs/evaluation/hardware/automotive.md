---
title: "Automotive Evaluations"
linktitle: "Automotive Evaluations"
date: 2021-05-05T22:35:41+02:00
draft: false
type: docs
weight: 5
---
- [Post-Quantum Secure Architectures for Automotive Hardware Secure Modules](https://eprint.iacr.org/2020/026): Building a post-quantum secure automotive HSM is feasible and can meet the hard requirements imposed by a modern vehicle ECU [[WS20]](../../../refs#ws20)
- [Post-quantum lightweight identity-based two-party authenticated key exchange protocol for Internet of Vehicles with probable security](https://www.sciencedirect.com/science/article/pii/S0140366421003686): Lattice-based two-party authenticated key agreement (LB-ID-2PAKA) protocol using identity-based cryptography (IBC). [[GRS22]](../../../refs#grs22)
- [Strengthening Post-Quantum Security for Automotive Systems](https://ieeexplore.ieee.org/document/9217638/): Presenting an improved and timing attack resistent implementation of ThreeBears for a microcontroller used in automotive systems. [[FVS20]](../../../refs#fvs20)
- [Suitability of 3rd Round Signature Candidates for Vehicle-to-Vehicle Communication –Extended Abstract](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/bindel-suitability-abstract-pqc2021.pdf): Analysis of PQC signature algorithms for vehicle-to-vehicle (V2V) communication focusing on performance and functionality. [[NSH21]](../../../refs#nsh21)
