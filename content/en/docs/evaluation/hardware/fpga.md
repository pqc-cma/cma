---
title: "FPGA Evaluations"
linktitle: "FPGA Evaluations"
date: 2021-05-05T22:35:41+02:00
draft: false
type: docs
weight: 2
---
- [A Compact Hardware Implementation of CCA-Secure Key Exchange Mechanism CRYSTALS-KYBER on FPGA](https://tches.iacr.org/index.php/TCHES/article/view/8797): Standalone hardware design of CRYSTALS-KYBER. [[XL21]](../../../refs#xl21)
- [Algorithm-agile cryptographic coprocessor based on FPGAs](https://doi.org/10.1117/12.359537): This contribution describes the design and implementation of an algorithm-agile cryptographic co-processor board. [[PCC99]](../../../refs#pcc99)
- [An Efficient Implementation of the NewHope Key Exchange on FPGAs](https://ieeexplore.ieee.org/document/8930047/): Implementation of NewHope on a Xilinx Artix-7 7020 FPGA platform. [[XL20]](../../../refs#xl20)
- [Efficient and Scalable FPGA-Oriented Design of QC-LDPC Bit-Flipping Decoders for Post-Quantum Cryptography](https://ieeexplore.ieee.org/document/9180360): Performance speedup of five times using the Xilinx Artix-7 200 FPGA [[ZGF20]](../../../refs#zgf20)
- [FPGA-based Accelerator for Post-Quantum Signature Scheme SPHINCS-256](https://tches.iacr.org/index.php/TCHES/article/view/831): Hardware accelerator implementation for SPHINCS-256 outperforming RSA and ECDSA in terms of throughput, area, and latency. [[ACZ18]](../../../refs#acz18)
- [High Performance Post-Quantum Key Exchange on FPGAs](https://eprint.iacr.org/2017/690): High performance hardware architecture for NewHope. [[KLC17]](../../../refs#klc17)
- [High-Performance Hardware Implementation of CRYSTALS-Dilithium](https://eprint.iacr.org/2021/1451): High-performance implementation of CRYSTALSDilithium targeting FPGAs. [[BNG21]](../../../refs#bng21)
- [High-Speed Hardware Architectures and Fair FPGA Benchmarking of CRYSTALS-Kyber, NTRU, and Saber](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/gaj-high-speed-hardware-gmu-pqc2021.pdf): This paper presents high-speed hardware architectures for CRYSTALS-Kyber, NTRU-HPS, NTRU-HRSS and Saber. [[DMG21]](../../../refs#dmg21)
- [Post-Quantum Cryptography on FPGA Based on Isogenies on Elliptic Curves](https://ieeexplore.ieee.org/document/7725935/): Isogeny-based schemes can be implemented with high efficiency on reconfigurable hardware [[KAM17]](../../../refs#kam17)
- [Rethinking Secure FPGAs: Towards a Cryptography-Friendly Configurable Cell Architecture and Its Automated Design Flow](https://ieeexplore.ieee.org/document/8457664/): Proposing fine-grained configurable cell array specifically tailored for the implementation of cryptographic algorithms. [[MCR18]](../../../refs#mcr18)
- [Ultra-Fast Modular Multiplication Implementation for Isogeny-Based Post-Quantum Cryptography](https://ieeexplore.ieee.org/document/9020384/): Improved unconventional-radix finite-field multiplication (IFFM) algorithm reducing computational complexity by about 20\% [[TLW19]](../../../refs#tlw19)
