---
title: "Security Protocols & Applications"
linktitle: "Security Protocols & Applications"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 2
layout: no_index
---
An overview of the standard security protocols and applications that make use of cryptographic systems and cipher suites, and their current state of PQC integration.

|Protocol/Application|Description|Security Aspect|Mechanism|Usage|OSI-Layer|
|--------------------|-----------|---------------|---------|-----|---------|
|**Data Link Layer**|
|[MACsec]({{< ref "/macsec" >}} "MACsec")|Media Access Control Security|Confidentiality|Pre-Shared Keys|Ethernet|2|
|[CHAP]({{< ref "/pap" >}} "CHAP")|Challenge-Handshake Authentication Protocol|User Authentication|Hash, Challenge & Pre-Shared Secret|PPP (Point-to-Point)|2|
|[PAP]({{< ref "/pap" >}} "PAP")|Password Authentication Protocol|User Authentication|Username & Password|PPP (Point-to-Point)|2|
|**Network Layer**|
|[AH]({{< ref "/ikev2" >}} "AH")|Authentication Header|Data Authentication & Integrity|Hash & Pre-Shared Secret|IPsec|3-4|
|[ESP]({{< ref "/ikev2" >}} "ESP")|Encapsulating Security Payload|Data Authentication, Integrity & Confidentiality|Hash, Pre-Shared Secret & Encryption|IPsec|3-4|
|[IKEv2]({{< ref "/ikev2" >}} "IKEv2")|Internet Key Exchange|User Authentication and Confidentiality|Diffie-Hellman Key Agreement & X.509 Certificates|IPsec|3-4|
|**Transport Layer**|
|[TLS/SSL]({{< ref "/tls" >}} "TLS/SSL")|Transport Layer Security/Secure Sockets Layer|User Authentication & Confidentiality|Diffie-Hellman Key Agreement & X.509 Certificates|TCP|4-7|
|[DTLS]({{< ref "/dtls" >}} "DTLS")|Datagram Transport Layer Security|User Authentication & Confidentiality|Diffie-Hellman Key Agreement & X.509 Certificates|UDP|4|
|[SRTP/ZRTP]({{< ref "/rtp" >}} "SRTP/ZRTP")|(Secure) Real-Time Transport Protocol|Data Authentication, Integrity & Confidentiality|Diffie-Hellman and Hash Key Agreement, Encryption & MAC|RTP|4-7|
|**Application Layer**|
|[SSH]({{< ref "/ssh" >}} "SSH")|Secure Shell Protocol|User Authentication, Integrity & Confidentiality|Diffie-Hellman Key Agreement, X.509 Certificates, MAC, Password or Public Key Authentication & Encryption|-|7|
|[OpenVPN]({{< ref "/vpn" >}} "OpenVPN")|Open Virtual Private Network|User Authentication, Integrity, Confidentiality|Pre-Shared Keys, Diffie-Hellman Key Agreement (TLS), X.509 Certificates, Username & Password, MAC, Encryption|VPN|2-7|
|[WireGuard]({{< ref "/vpn" >}} "WireGuard")|WireGuard Virtual Private Network|Data Authentication, Integrity, Confidentiality|Diffie-Hellman Key Agreement, MAC & Encryption|VPN|2-7|
|[X.509]({{< ref "/pki" >}} "Certificates")|International Telecommunications Union (ITU) Standard|User Authentication, Integrity, Confidentiality|Digital Signatures & Certificates|Public Key Infrastructure (PKI)|7|
|[DNSSEC]({{< ref "/dnssec" >}} "DNSSEC")|Domain Name System Security Extensions|Data Authentication & Integrity|Digital Signatures & Certificates|IP|7|
|[S-MIME/PGP-MIME]({{< ref "/smime" >}} "S-MIME/PGP-MIME")|Secure Multipurpose Internet Mail Extension|Data Authentication, Integrity & Confidentiality|Hybrid Encryption & Digital Signatures|E-Mail|7|
|[PGP/GPG]({{< ref "/pgp" >}} "PGP/GPG")|Pretty Good Privacy/GNU Privacy Guard|Data Authentication, Integrity & Confidentiality|Hybrid Encryption & Digital Signatures|E-Mail|7|
|[Kerberos/PKINIT]({{< ref "/pkinit" >}} "Kerberos/PKINIT")|Authentication Service|User Authentication|Username, Password & Encryption|Network Communication|7|
|[SSO (OAuth/LDAP/SAML/RADIUS)]({{< ref "/sso" >}} "SSO (OAuth/LDAP/SAML/RADIUS)")|Single Sign-on|User Authentication|Depends on Used Protocol|Network Communication|7|
|[SFTP]({{< ref "/sftp" >}} "SFTP")|SSH File Transfer Protocol|User Authentication and Confidentiality|SSH & Encryption|File Transfer|7|
|[eIDs]({{< ref "/eid" >}} "eIDs")|Electronic Identification Documents|Data / User Authentication, Integrity & Confidentiality|Diffie-Hellman Key Agreement, Certificates & Encryption|Secure eCard / Terminal Communication|7|
|[Signal]({{< ref "/signal" >}} "Signal")|Signal Protocol|Data / User Authentication, Integrity & Confidentiality|Diffie-Hellman Key Agreement & Encryption|Secure Instant Messaging Protocol|7|
|[Blockchain]({{< ref "/blockchain" >}} "Blockchain")|DLTs (Distributed Ledgers)|Data / User Authentication, Integrity & Confidentiality|Hash, Digital Signatures, & Encryption|Peer-to-Peer Distributed Networks|2-7|
|[TOR]({{< ref "/tor" >}} "TOR")|Tor Network|Data / User Authentication, Integrity & Confidentiality|Diffie-Hellman Key Agreement & Encryption|Onion Routing|2-7|
-----------------------------------------------------------------------------------

##### [Wireless Networks]({{< ref "/wlan" >}} "Wireless Networks")
- WPA/IEEE 802.x1/EAP
- UMTS/LTE/5G
- Bluetooth/IR/ZigBee
- WSN

##### [Others]({{< ref "/others" >}} "Others")
- PKCS  (Public Key Cryptography Standards)
- CMS (Cryptographic Message Syntax)
- OPC UA (Open Platform Communication Unified Architecture)
- L2TP (Layer 2 Tunneling Protocol)
- SSTP (Secure Socket Tunneling Protocol)
- GRE (Generic Routing Encapsulation)
- QUIC (Quick)
- S-RPC (Secure Remote Procedure Call)
- FinTS/HBCI
- PCT
- SET
- Telnet
