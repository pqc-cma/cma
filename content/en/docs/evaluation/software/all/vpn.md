---
title: "Virtual Private Network (VPN)"
linktitle: "Virtual Private Network (VPN)"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 8
---
- [Achieving 128-bit Security against Quantum Attacks in OpenVPN](https://essay.utwente.nl/70677/1/2016-08-09%20MSc%20Thesis%20Simon%20de%20Vries%20final%20color.pdf): Evaluation of how Niederreiter can be used for quantum-secure key exchanges by implementing it in OpenVPN. [[d16]](../../../../refs#d16)
- [KEM-based Hybrid Forward Secrecy for Noise](https://github.com/noiseprotocol/noise_hfs_spec/blob/025f0f60cb3b94ad75b68e3a4158b9aac234f8cb/output/noise_hfs.pdf): Proposed a draft showing how to integrate post-quantum KEMs into Noise Protocol Framework which is originally designed to use DH key exchange protocol [[P20]](../../../../refs#p20)
- [Microsoft/PQCrypto-VPN](https://github.com/Microsoft/PQCrypto-VPN): Fork of the OpenVPN software combined with post-quantum cryptography. [[M21]](../../../../refs#m21)
- [PQ-WireGuard: we did it again.](https://csrc.nist.gov/Presentations/2021/pq-wireguard-we-did-it-again): Introduction of three variants of the WireGuard protocol offering post-quantum security. [[RGR21]](../../../../refs#rgr21)
- [Post-Quantum Cryptography in WireGuard VPN](https://link.springer.com/10.1007/978-3-030-63095-9_16): Defined four levels of security using 3 handshakes with classical and hybrid post-quantum KEMs of WireGuard VPN and designed their respective protocol extensions. [[KMR20]](../../../../refs#kmr20)
- [Post-quantum WireGuard](https://eprint.iacr.org/2020/379): Modified the original DH-like key exchange protocol to KEM which is more common in the post-quantum setting. Combined a CCA-secure KEM and a CPA-secure KEM to satisfy the need which disallows the fragmentation [[HNS20]](../../../../refs#hns20)
- [Tiny WireGuard Tweak](https://link.springer.com/chapter/10.1007%2F978-3-030-23696-0_1): alteration to the WireGuard protocol provides transitional post-quantum security. [[AMW19]](../../../../refs#amw19)
- [Towards Quantum-Safe VPNs and Internet](https://eprint.iacr.org/2019/1277): Evaluation of CPU overhead and security using OpenSSL and OpenVPN [[vvA19]](../../../../refs#vva19)
