---
title: "Others"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 20
---
- [Incorporating post-quantum cryptography in a microservice environment](https://rp.os3.nl/2019-2020/p13/report.pdf): Performance of PQC in a microservice architecture similar to classical cryptography. [[W20]](../../../../refs#w20)
- [Quantum-resistant authentication algorithms for satellite-based augmentation systems](https://onlinelibrary.wiley.com/doi/abs/10.1002/navi.287): Introduces the cryptographic primitives necessary to understand the vulnerabilities in modern day cryptography due to quantum computing and investigates the use of TESLA and EC-Schnorr algorithms in broadcast systems. [[NWE19]](../../../../refs#nwe19)
