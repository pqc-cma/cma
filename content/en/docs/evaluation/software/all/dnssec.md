---
title: "DNSSEC"
linktitle: "DNSSEC"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 9
---
- [Retrofitting post-quantum cryptography in internet protocols: a case study of DNSSEC](https://dl.acm.org/doi/10.1145/3431832.3431838): Evaluate three PQC-Algorithms that are suitable for DNSSEC within certain constraints [[Mdv20]](../../../../refs#mdv20)
