---
title: "MACsec"
linktitle: "MACsec"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 1
---
- [Post-quantum MACsec key agreement for ethernet networks](https://doi.org/10.1145/3407023.3409220): Implementation of a McEliece-based key establishment for MKA and XMSS hash-based signature scheme for entity authentication. [[CS20]](../../../../refs#cs20)
