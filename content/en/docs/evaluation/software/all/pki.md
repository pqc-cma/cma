---
title: "Public Key Infrastructure (PKI) & Certificates"
linktitle: "Public Key Infrastructure (PKI) & Certificate"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 15
---
- [Composite Keys and Signatures For Use In Internet PKI](https://datatracker.ietf.org/doc/html/draft-ounsworth-pq-composite-sigs-03): This document defines the structures CompositePublicKey, CompositeSignatureValue, and CompositeParams, which are sequences of the respective structure for each component algorithm. [[OP20]](../../../../refs#op20)
- [Next-Generation Web Public-Key Infrastructure Technologies](https://eprints.qut.edu.au/128643): New decentralized approach to certificate management based on generic blockchains (DPKIT), compatible with existing PKIs. [[H19]](../../../../refs#h19)
- [QChain: Quantum-resistant and Decentralized PKI using Blockchain](https://caislab.kaist.ac.kr/publication/paper_files/2018/SCIS'18_HC_BC.pdf): Quantum-resistant decentralized PKI system using blockchain [[AK18]](../../../../refs#ak18)
- [The Viability of Post-quantum X.509 Certificates](http://eprint.iacr.org/2018/063): Present suitable parameters for software signature use cases and good signature candidates for TLS 1.3 authentication. [[KPD18]](../../../../refs#kpd18)
- [X.509-Compliant Hybrid Certificates for the Post-Quantum Transition](https://joss.theoj.org/papers/10.21105/joss.01606): Parallel usage of two independent cryptographic schemes within public key infrastructures enabling a stepwise transition to post-quantum secureand hybrid algorithms [[BBG19]](../../../../refs#bbg19)
