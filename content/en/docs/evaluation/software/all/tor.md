---
title: "Onion Routing & Tor Network"
linktitle: "Onion Routing & Tor Network"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 18
---
- [A note on Post Quantum Onion Routing](https://eprint.iacr.org/2021/111): Analysis of the current cryptographic approaches for making TOR quantum-resistant. [[E21]](../../../../refs#e21)
- [Circuit-extension handshakes for Tor achieving forward secrecy in a quantum world](https://www.sciendo.com/article/10.1515/popets-2016-0037): Proposed a hybrid Tor handshake framework and instantiated using an NTRU based KEM [[SWZ16]](../../../../refs#swz16)
- [Post-Quantum Forward-Secure Onion Routing: (Future Anonymity in Today’s Budget)](http://link.springer.com/10.1007/978-3-319-28166-7_13): Proposed a hybrid onion routing protocol which simultaneously use DH key exchange and an algorithm based on RLWE problem [[GK15]](../../../../refs#gk15)
- [QSOR: Quantum-safe Onion Routing](https://arxiv.org/pdf/2001.03418.pdf): Analyzed the influence of introducing PQC schemes. Implemented and evaluated the SweetOnions protocol in hybrid and PQ-only setting [[TRH20]](../../../../refs#trh20)
