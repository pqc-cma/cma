---
title: "eIDs"
title: "eIDs"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 19
---
- [Analyzing Requirements for Post Quantum Secure Machine Readable Travel Documents](https://dl.gi.de/handle/20.500.12116/36494): Potential quantum-resistant replacements that are suitable for eID protocols and the resource-constrained environment of embedded security chips [[M21]](../../../../refs#m21)
- [How Quantum Computers threat security of PKIs and thus eIDs](http://dl.gi.de/handle/20.500.12116/36504): Open Question regarding PQC for secure, electronic identification. [[VF21]](../../../../refs#vf21)
- [Implementing and Evaluating the Quantum Resistant Cryptographic Scheme Kyber on a Smart Card](http://www.diva-portal.org/smash/get/diva2:1464485/FULLTEXT01.pdf):  [[E20]](../../../../refs#e20)
- [Post-quantum Certificates for Electronic Travel Documents](http://link.springer.com/10.1007/978-3-030-66504-3_4): A special-purpose PQ public key infrastructure for the next generation of electronic travel documents [[PM20]](../../../../refs#pm20)
- [Quantum computers will compromise the security of identity documents](https://www.eurosmart.com/quantum-computers-will-compromise-the-security-of-identity-documents/): The effects of quantum computers on the securiy of electronic identity documents [[E19]](../../../../refs#e19)
- [Strengthening trust in the identity life cycle: Enhancing electronic machine readable travel documents due to advances in security protocols and infrastructure](https://refubium.fu-berlin.de/handle/fub188/24055): Improving the security of birth certificates, simpler and post-quantum resistant security protocols, and infrastructure improvements in the identity life cycle. [[B19]](../../../../refs#b19)
