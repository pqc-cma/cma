---
title: "Signal Protocol"
linktitle: "Signal Protocol"
date: 2021-11-02T20:28:24+08:00
draft: false
type: docs
weight: 7
---
The Signal protocol is a secure instant messaging protocol that underlies the security of numerous applications such as WhatsApp, Skype, Facebook Messenger among many others.
- [An Efficient and Generic Construction for Signal's Handshake (X3DH): Post-Quantum, State Leakage Secure, and Deniable](https://eprint.iacr.org/2021/616): Generic construction to support PQC migration in handshake of Signal protocol and provided an implementation [[HKK21]](../../../../refs#hkk21)
- [Hybrid Signal protocol for post-quantum email encryption](https://eprint.iacr.org/2021/875): Discussed how hybrid mode can be used in Signal protocol replacing DH with split KEMs i.e. AKE form KEMs and signatures for ephemeral (commutative) 2nd party contributions [[SSK21]](../../../../refs#ssk21)
- [Post-Quantum Key Exchange in Telegram's MTProto Protocol](https://ntnuopen.ntnu.no/ntnu-xmlui/handle/11250/2781229): Analysis of the integration of SABER KEM into Telegram's protocol MTProto. [[R21]](../../../../refs#r21)
- [Post-Quantum Signal Key Agreement with SIDH](https://eprint.iacr.org/2021/1187): Definition of a formal security model for the original signal protocol and a secure replacement for the Signal X3DH key exchange protocol based on SIDH. [[DG21]](../../../../refs#dg21)
- [Post-quantum Asynchronous Deniable Key Exchange and the Signal Handshake](https://eprint.iacr.org/2021/769): Showed how to construct an asynchronous deniable AKE protocol using a designated verifier signature that can be efficiently instantiated using PQC algorithms [[BFG21]](../../../../refs#bfg21)
- [The Double Ratchet: Security Notions, Proofs, and Modularization for the Signal Protocol](https://link.springer.com/chapter/10.1007%2F978-3-030-17653-2_5): Modular generalized Signal protocol enabling post-quantum security. [[ACD19]](../../../../refs#acd19)
