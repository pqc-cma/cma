---
title: "IPsec/IKEv2"
linktitle: "IPsec/IKEv2"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 3
---
- [A formal analysis of IKEv2’s post quantum extension](https://www.doi.org/xx.xxx//xxxx): Analysis of post-quantum cryptographic techniques for securing IoT networks ongoing research efforts in the field. [[GGG21]](../../../../refs#ggg21)
- [Alternative Approach for Mixing Preshared Keys in IKEv2 for Post-quantum Security](https://datatracker.ietf.org/doc/html/draft-smyslov-ipsecme-ikev2-qr-alt-04): Extending IKEv2 so that the shared secret exchanged between peers has resistance against quantum computer attacks including the initial IKEv2 SA [[S21]](../../../../refs#s21)
- [Framework to Integrate Post-quantum Key Exchanges into Internet Key Exchange Protocol Version 2 (IKEv2)](https://datatracker.ietf.org/doc/html/draft-tjhai-ipsecme-hybrid-qske-ikev2-04): Extending IKEv2 so that the shared secret exchanged between peers has resistance against quantum computer attacks. [[TTg19]](../../../../refs#ttg19)
- [Intermediate Exchange in the IKEv2 Protocol](https://datatracker.ietf.org/doc/html/draft-ietf-ipsecme-ikev2-intermediate-06): This documents defines a new exchange, called Intermediate Exchange, for the Internet Key Exchange protocol Version 2 (IKEv2) which helps to avoid IP fragmentation of large IKE messages. [[S21]](../../../../refs#s21)
- [Mixing Preshared Keys in the Internet Key Exchange Protocol Version 2 (IKEv2) for Post-quantum Security](https://rfc-editor.org/rfc/rfc8784.txt): Extension of IKEv2 to allow it to be resistant to a quantum computer by using pre-shared keys [[FKM20]](../../../../refs#fkm20)
- [Post-Quantum Kryptographie für IPsec](https://svs.informatik.uni-hamburg.de/publications/2015/2015-02-24-Zimmer-DFN-PQC-fuer-IPsec.pdf): Integration of the Niederreiter scheme in IPsec IKEv2 handshake [[Z15]](../../../../refs#z15)
- [Postquantum Preshared Keys for IKEv2](https://datatracker.ietf.org/doc/html/draft-fluhrer-qr-ikev2-03): This document describes an extension of IKEv2 to allow it to be resistant to a Quantum Computer, by using preshared keys. [[FMK16]](../../../../refs#fmk16)
- [Towards a Verifiably SecureQuantum-Resistant Key Exchangein IKEv2](https://www.nm.ifi.lmu.de/pub/Diplomarbeiten/heid19/PDF-Version/heid19.pdf): PQC integration in IKEv2 as a new PQ-IKEv2 protocol [[H19]](../../../../refs#h19)
