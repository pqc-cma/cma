---
title: "How to Contribute"
linktitle: "How to Contribute"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 13
---
Your contributions are always welcome!

To contribute to this site login with your existing GitLab account, fork our repository on https://gitlab.com/pqc-cma/cma and send a merge request (aka pull request) with your update.

#### How-To:
This site uses the Hugo Docsy Documentation theme. All pages are in Markdown and can be edited with any normal text editor. To add new references or any useful material you need to visit the Gitlab repository, fork the project, and then edit the files you wish to update. After that just send a merge request with your commit. You may also create new pages if your contribution doesn't fit into any of the existing pages. Please pay attention to the syntax and formatting. In case you are not sure how to do so, just try, and we can gladly correct any mistakes in your merge request.

#### Forking and Merging:
The repository can be forked using the [Fork](https://gitlab.com/pqc-cma/cma/-/forks/new) button on the project's home page (upper right corner). Edit the content files you intend to modify (e.g. add new content or edit existing content etc.) on the development branch. Afterwards you may send a merge request to the master branch on the original project repository.

#### Repository Structure:
The project tree consists of the following levels:
- Level 1: Hugo content and theme directories, and configuration files including gitlab-ci.yml, config.toml, .gitmodules, and .gitignore
- Level 2: Hugo Docsy theme, Hugo layouts, mindmap_gen (used to generate mindmaps using Python scripts), and language dependent content directories.
- Level 3: The docs directory that contains the actual HTML and Markdown content files to generate the pages of the website.

For adding and editing contents only the content files in the docs directory are relevant!

#### Content Dependencies:
New content added to the categorized pages aren't added automatically to the References page. We are still working on a solution that is more convenient and less error-prone. For the time being, please follow the same pattern for citing and bibliography entries.

**Example:**
[Quantum Safe Cryptography and Security: An introduction, benefits, enablers and challenges](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf): Important use cases for cryptography and potential migration strategies to transition to post-quantum cryptography [[CCD+15]](../refs#ccd15).

The associated bibliography entry in refs.md would be:
###### [CCD+15]
[M.  Campagna,  L.  Chen,  O.  Dagdelen,  J.  Ding,  J  Fernick,  N.  Gisin,  D.  Hayford,  T.  Jennewein,  N.  Lütkenhaus,  and  M.  Mosca.  2015.Quantum  SafeCryptography  and  Security:  An  introduction,  benefits,  enablers  and  chal-lenges.European Telecommunications Standards InstituteETSI White Paper,8  (June  2015),  1–64.](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf)


Please note that the anchor [CCD+15] is the same as the citation [[CCD+15]], whereas the hook is always in lower case and without special characters as in (../refs#ccd15). The hook is also dependent of the directory level in relation to the refs.md file i.e. go back "../" and look for the anchor "#ccd15" in "refs.md".

#### Guidelines:
* Use the development branch.
* Add one link per Pull Request.
    * Make sure the PR title is in the format of `Add content-name`.
    * Write down the reason why the contribution is suitable.
* Add the link: `* [content-name](http://example.com/) - A short description ends with a period.`
    * Keep descriptions concise and **short**.
* Add a section if needed.
    * Add the section description.
    * Add the section title to Table of Contents.
* Search previous Pull Requests or Issues before making a new one, as yours may be a duplicate.
* Check your spelling and grammar.
* Remove any trailing whitespace.

#### Build Dependencies:
To build locally follow the steps in the README.md.

P.S: Contributions aren't restricted to the members of the faculty at our university!
