---
title: "Contributors"
linktitle: "Contributors"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 14
---
This site was initiated by the research groups [Applied Cyber-Security](https://fbi.h-da.de/forschung/arbeitsgruppen/applied-cyber-security-darmstadt) and [User Centered Security](https://fbi.h-da.de/index.php?id=764) of [Darmstadt University of Applied Sciences](https://h-da.de/), funded by [ATHENE National Research Center for Applied Cybersecurity](https://www.athene-center.de).

A list of the contributors:

| Name                           | Affiliation | Contribution |
|--------------------------------|-------------|--------------|
|Alexander Wiesmaier|Darmstadt University of Applied Sciences|Original Paper / Community Site|
|Alexander Zeier|MTG AG Darmstadt|Original Paper|
|Andreas Heinemann|Darmstadt University of Applied Sciences|Original Paper|
|Dominik Heinz|Darmstadt University of Applied Sciences|Community Site / Development |
|Julian Geißler|Darmstadt University of Applied Sciences|Original Paper|
|Nouri Alnahawi|Darmstadt University of Applied Sciences|Original Paper / Community Site / Development / Updates |
|Peter Hecht|Buenos Aires University|Community Site|
|Pia Bauspieß|Darmstadt University of Applied Sciences|Original Paper|
|Robin Meunier|Darmstadt University of Applied Sciences|PQC Algorithm Specifications / Protocol Integration|
|Tobias Grasmeyer|Darmstadt University of Applied Sciences|Original Paper / Community Site / Development |
|Xi Hu|Chongqing University|Content and Chinese Translation in Community Site / Protocol Integration|
