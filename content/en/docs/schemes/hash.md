---
title: "Hash-Based Schemes"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 3
---
- [Improving the Performance of the Picnic Signature Scheme](https://eprint.iacr.org/2020/427):  [[KZ20]](../../refs#kz20)
- [Robust Multi-Property Combiners for Hash Functions](http://link.springer.com/10.1007/s00145-013-9148-7):  [[FLP14]](../../refs#flp14)
- [State Management for Hash-Based Signatures](https://eprint.iacr.org/2016/357):  [[MKF16]](../../refs#mkf16)
- [The SPHINCS+ Signature Framework](https://dl.acm.org/doi/10.1145/3319535.3363229):  [[BHK19]](../../refs#bhk19)
- [XMSS: eXtended Merkle Signature Scheme](https://rfc-editor.org/rfc/rfc8391.txt):  [[HBG18]](../../refs#hbg18)
