---
title: "SIKE"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 14
---
[SIKE](https://sike.org/) is an isogeny-based key-encapsulation suite based on pseudo-random walks in supersingular isogeny graphs [Specs.](https://sike.org/files/SIDH-spec.pdf).
