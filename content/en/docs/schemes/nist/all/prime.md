---
title: "NTRU-Prime"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 11
---
[NTRU-Prime](https://ntruprime.cr.yp.to/) is a small lattice-based key-encapsulation mechanism (KEM) [Specs.](https://ntruprime.cr.yp.to/nist/ntruprime-20201007.pdf).
