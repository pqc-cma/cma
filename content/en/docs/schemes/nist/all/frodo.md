---
title: "FrodoKEM"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 4
---
[FrodoKEM](https://frodokem.org/) is a key-encapsulation based on generic lattices [Specs.](https://frodokem.org/files/FrodoKEM-specification-20210604.pdf).
