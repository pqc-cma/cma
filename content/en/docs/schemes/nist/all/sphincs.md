---
title: "SPHINCS+"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 15
---
[SPHINCS+](https://sphincs.org/) a stateless hash-based signature scheme [Specs.](https://sphincs.org/data/sphincs+-round3-specification.pdf).
