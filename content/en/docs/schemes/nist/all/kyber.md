---
title: "CRYSTALS-Kyber"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 7
---
[CRYSTALS-Kyber](https://pq-crystals.org/kyber/) is a key-encapsulation mechanism (KEM) based on the learning with errors (LWE) problem over module lattices [Specs.](https://pq-crystals.org/kyber/data/kyber-specification-round3-20210131.pdf).

- Required parameter for definiteness:
  - $k$: Positive integer

- Non-required parameter for definiteness:
  - $n$: Positive integer
  - $q$: Positive integer
  - $\eta_1$: Positive integer
  - $\eta_2$: Positive integer
  - $(d_1, d_2)$: Positive integer
