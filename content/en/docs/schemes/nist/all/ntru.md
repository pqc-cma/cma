---
title: "NTRU"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 9
---
[NTRU](https://ntru.org/) is a public-key cryptosystem based on the lattice shortest vector problem (SVP) [Specs.](https://ntru.org/f/ntru-20190330.pdf).

- Required parameter for definiteness:
	- $n$: Positive integer
  - $q$: Positive integer

- Non-required parameter for definiteness:
  - $p$: Positive integer (always $3$)
