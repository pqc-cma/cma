---
title: "Classic McEliece"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 8
---
[Classic McEliece](https://classic.mceliece.org/) is a code-Based public-key cryptosystem based on linear decoding problem of random binary Goppa codes [Specs.](https://classic.mceliece.org/nist/mceliece-20201010.pdf). 

- Required parameter for definiteness:
  - $m$: Positive integer
  - $n$: Positive integer
  - $t$: Positive integer
  - $\mu$: Integer (default 0)
  - $\nu$: Integer (default 0)

- Non-required parameter for definiteness:
  - $f(z)$: Polynomial
  - $F(z)$: Polynomial
