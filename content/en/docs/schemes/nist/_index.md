---
title: "NIST PQC Algorithms"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 1
layout: no_index
---
The current state of PQC is represented by the ongoing [NIST PQC standardization process](https://www.nist.gov/pqcrypto)
- [Submission Requirements and Evaluation Criteria  for the Post-Quantum Cryptography Standardization Process](https://csrc.nist.rip/groups/ST/post-quantum-crypto/documents/call-for-proposals-final-dec-2016.pdf).
- [Status report on the first round](https://nvlpubs.nist.gov/nistpubs/ir/2019/NIST.IR.8240.pdf).
- [ Status report on the second round](https://nvlpubs.nist.gov/nistpubs/ir/2020/NIST.IR.8309.pdf).

#### **NIST PQC candidate algorithms:**

| Algorithm                           | Description | Type | NIST Round |
|-------------------------------------|-------------|------|------------|
| [BIKE]({{< ref "/bike" >}} "BIKE") | Bit flipping key-encapsulation based on QC-MDPC (Quasi-Cyclic Moderate Density Parity-Check) | Public-key Encryption and Key-establishment | Round Three Alternative |
| [CRYSTALS-Dilithium]({{< ref "/dilithium" >}} "CRYSTALS-Dilithium") | Digital signature scheme based on the hardness of lattice problems over module lattices | Digital Signature | Round 3 Finalist |
| [Falcon]({{< ref "/falcon" >}} "Falcon") | Lattice-based signature scheme based on the short integer solution problem (SIS) over NTRU lattices | Digital Signature | Round 3 Finalist |
| [FrodoKEM]({{< ref "/frodo" >}} "FrodoKEM")| Key-encapsulation from generic lattices | Public-key Encryption and Key-establishment | Round Three Alternative |
| [GeMSS]({{< ref "/gemss" >}} "GeMSS") | Multivariate signature scheme producing small signatures | Digital Signature | Round Three Alternative |
| [HQC]({{< ref "/hqc" >}} "HQC") | Hamming quasi-cyclic code-based public-key encryption scheme | Public-key Encryption and Key-establishment | Round Three Alternative |
| [CRYSTALS-Kyber]({{< ref "/kyber" >}} "KYBER")| IND-CCA2-secure key-encapsulation mechanism (KEM) based on hard problems over module lattices | Public-key Encryption and Key-establishment | Round 3 Finalist |
| [Classic McEliece]({{< ref "/mceliece" >}} "Classic McEliece") | Code-based public-key cryptosystem based on random binary Goppa codes | Public-key Encryption and Key-establishment | Round 3 Finalist |
| [NTRU]({{< ref "/ntru" >}} "NTRU") | Public-key cryptosystem based on lattice-based cryptography | Public-key Encryption and Key-establishment | Round 3 Finalist |
| [NTRU-Prime]({{< ref "/prime" >}} "NTRU-Prime")  | Small lattice-based key-encapsulation mechanism (KEM) | Public-key Encryption and Key-establishment | Round 3 Alternative |
| [Picnic]({{< ref "/picnic" >}} "Picnic") | Digital signature algorithm based on the zero-knowledge proof system and symmetric key primitives | Digital Signature | Round 3 Alternative |
| [Rainbow]({{< ref "/rainbow" >}} "Rainbow")| Public-key cryptosystem based on the hardness of solving a set of random multivariate quadratic systems | Digital Signature | Round 3 Finalist |
| [SABER]({{< ref "/saber" >}} "SABER") | IND-CCA2-secure key-encapsulation mechanism (KEM) based on the hardness of the module learning with rounding problem (MLWR) | Public-key Encryption and Key-establishment | Round 3 Finalist |
| [SIKE]({{< ref "/sike" >}} "SIKE")| Isogeny-based key-encapsulation suite based on pseudo-random walks in supersingular isogeny graphs | Public-key Encryption and Key-establishment | Round 3 Alternative |
| [SPHINCS+]({{< ref "/sphincs" >}} "SPHINCS+") | A stateless hash-based signature scheme | Digital Signature | Round 3 Alternative |
