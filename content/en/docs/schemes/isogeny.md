---
title: "Isogeny-Based Schemes"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 6
---
- [A Note on Post-Quantum Authenticated Key Exchange from Supersingular Isogenies](https://eprint.iacr.org/2018/267):  [[L18]](../../refs#l18)
- [CSI-RAShi: Distributed key generation for CSIDH.](https://eprint.iacr.org/2020/1323):  [[BDP20]](../../refs#bdp20)
- [CSIDH on the surface](https://eprint.iacr.org/2019/1404):  [[CD20]](../../refs#cd20)
- [CSIDH: An Efficient Post-Quantum Commutative Group Action](https://eprint.iacr.org/2018/383):  [[CLM18]](../../refs#clm18)
- [CTIDH: faster constant-time CSIDH](https://eprint.iacr.org/2021/633):  [[BBC21]](../../refs#bbc21)
- [Memory Optimization Techniques for Computing Discrete Logarithms in Compressed SIKE.](https://eprint.iacr.org/2021/368):  [[HKP21]](../../refs#hkp21)
- [PQDH: A Quantum-Safe Replacement for Diffie-Hellman based on SIDH](https://eprint.iacr.org/2019/730):  [[SH19]](../../refs#sh19)
- [Post-quantum Group Key Agreement Scheme](https://eprint.iacr.org/2020/873):  [[BZ21]](../../refs#bz21)
- [Pre- and Post-quantum Diffie–Hellman from Groups, Actions, and Isogenies \textbar SpringerLink](https://link.springer.com/chapter/10.1007/978-3-030-05153-2_1):  [[S18]](../../refs#s18)
- [Supersingular Isogeny Diffie-Hellman Authenticated Key Exchange](https://eprint.iacr.org/2018/730):  [[FTT18]](../../refs#ftt18)
- [The Case for SIKE: A Decade of the Supersingular Isogeny Problem](https://eprint.iacr.org/2021/543):  [[C21]](../../refs#c21)
