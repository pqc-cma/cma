---
title: "Projects and Initiatives"
linktitle: "Projects and Initiatives"
date: 2021-05-06T00:12:55+02:00
draft: false
type: docs
weight: 10
menu:
  main:
    weight: 2
---
- [NCCoE](https://www.nccoe.nist.gov/projects/building-blocks/post-quantum-cryptography):
NCCoE project Migration to Post-Quantum Cryptography.

- [Open Quantum Safe](https://openquantumsafe.org/):
An open-source project that aims to support the development and prototyping of quantum-resistant cryptography.

- [Quantum RISC](https://www.quantumrisc.de/):
Next Generation Cryptography for Embedded Systems.

- [Eclipse CogniCrypt]( https://www.eclipse.org/cognicrypt/):
Secure Integration of Cryptographic Software.

- [BSI-Project: Secure Implementation of a Universal Crypto Library](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Kryptografie/Kryptobibliothek-Botan/kryptobibliothek-botan_node.html) More information (in German language) can be found in the [project summary](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Projektzusammenfassung_Botan.pdf)

- [Aquorypt](https://www.forschung-it-sicherheit-kommunikationssysteme.de/projekte/aquorypt): Quantum-Resistant Cryptography integration in software applications.

- [PQC4MED](https://www.forschung-it-sicherheit-kommunikationssysteme.de/projekte/pqc4med): Protecting medical data in the post-quantum era.

- [CrypoEng pqdb](https://github.com/cryptoeng/pqdb): A comprehensive list of post-quantum crypto schemes and their properties.

- [PQC Integration](https://cspub.h-da.io/pqc/):
PQC integration projects initiated by the research groups for Applied Cyber-Security and User-Centered Security at Darmstadt University of Applied Sciences.

- [DemoQuanDT](https://www.forschung-it-sicherheit-kommunikationssysteme.de/projekte/demoquandt):
Quantum-Key-Exchange in German telecommunication networks in the context of IT-Security.

- [PoQuID](https://www.mi.fu-berlin.de/inf/groups/ag-idm/projects/poquid/index.html):
Migration of cryptographic standards in official electronic documents towards quantum-computer-secure algorithms.
