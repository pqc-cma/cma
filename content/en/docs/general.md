---
title: "Related Work"
linktitle: "Related Work"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 4
---
A collection of survey papers dealing with general challenges and recommendations regarding the migration to post-quantum cryptography and crypto-agility.

*A full reference list can  be found in the [references](../refs) section. All references are listed in alphabetical order.*
- [Biggest Failures in Security](https://drops.dagstuhl.de/opus/volltexte/2020/11981/pdf/dagrep_v009_i011_p001_19451.pdf): A variety of problems in achieving IT security and possible strategies to solve them [[AVV19]](../refs#avv19)
- [Cybersecurity in an era with quantum computers: will we be ready?](https://ieeexplore.ieee.org/document/8490169): Assessment of the risks of quantum computers and their impact on the security of IT systems [[M15]](../refs#m15)
- [Eberbacher Gespräch: Next Generation Crypto](https://www.sit.fraunhofer.de/de/eberbach-crypto/): Key challenges forthree groups of industry differently involved in cryptography [[KNW18]](../refs#knw18)
- [From Pre-Quantum to Post-Quantum IoT Security: A Survey on Quantum-Resistant Cryptosystems for the Internet of Things](https://ieeexplore.ieee.org/abstract/document/8932459): Challenges for PQC in IoT and comparison of the performance of PQC algorithms [[F20]](../refs#f20)
- [Getting Ready for Post-Quantum Cryptography:: Explore Challenges Associated with Adoption and Use of Post-Quantum Cryptographic Algorithms](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.05262020-draft.pdf): Challenges Associated with Adopting and Using Post-Quantum Cryptographic Algorithms [[BPS20]](../refs#bps20)
- [Identifying Research Challenges in Post Quantum Cryptography Migration and Cryptographic Agility](http://arxiv.org/abs/1909.07353): A wide range of topics and challenges at a high abstraction level grouped into categories of PQC migration and crypto-agility [[OPp19]](../refs#opp19)
- [Introduction to post-quantum cryptography](http://link.springer.com/10.1007/978-3-540-88702-7_1): Introduction to post-quantum cryptography [[B09]](../refs#b09)
- [Kryptografie quantensicher gestalten](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Broschueren/Kryptografie-quantensicher-gestalten.pdf?__blob=publicationFile&v=4): Overview of the recent development in quantum technology and recommendations for migration to quantum resistant cryptography [[d21]](../refs#d21)
- [Migration zu Post-Quanten-Kryptografie - Handlungsempfehlungen des BSI](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Post-Quanten-Kryptografie.html): Recommendations for action on migration to PQC by the BSI (German Federal Office for Information Security) [[B20]](../refs#b20)
- [On PQC Migration and Crypto-Agility](https://arxiv.org/abs/2106.09599): A literature survey and a snapshot of the discovered challenges and solutions categorized in different areas offering a single entry-point into the subject reflecting the current state [[WAG21]](../refs#wag21)
- [On the Role of Hash-based Signatures in Quantum-Safe Internet of Things: Current Solutions and Future Directions](https://ieeexplore.ieee.org/document/9152977): Applications of one of the post-quantum signatures called Hash-Based Signature (HBS) schemes for the security of IoT devices in the quantum era [[SHK20]](../refs#shk20)
- [On the State of Post-Quantum Cryptography Migration](https://dl.gi.de/bitstream/handle/20.500.12116/37746/J1-2.pdf?sequence=1&isAllowed=y): Survey on the current state of the migration process towards PQ-Secure algorithms [[AWG21]](../refs#awg21)
- [Post Quantum Cryptography: Readiness Challenges and the Approaching Storm](http://arxiv.org/abs/2101.01269): Literature-study article about the upcomming threat of quantum computers against the currently widely used cryptography. [[CLO21]](../refs#clo21)
- [Post-Quantum Crypto for dummies](https://www.utimaco.com/de/post-quantum-crypto-dummies): Introduction to post-quantum cryptography [[WNG18]](../refs#wng18)
- [Post-Quantum Cryptography and 5G Security: Tutorial](https://doi.org/10.1145/3317549.3324882): On advancing the 3GPP 5G standards and NIST post-quantum cryptography standards in tandem, with the goal of launching a "quantum ready" 5G core network [[CMC19]](../refs#cmc19)
- [Post-quantum cryptography](http://www.nature.com/articles/nature23461): Literature-study of the ongoing development of PQC-schemes and algorithms as well as the ongoing security-scruitiniering of them. [[BL17]](../refs#bl17)
- [Practical Post-Quantum Cryptography](https://www.sit.fraunhofer.de/fileadmin/dokumente/studien_und_technical_reports/Practical.PostQuantum.Cryptography_WP_FraunhoferSIT.pdf?_=1503992279): White paper from the Fraunhofer Institute for Secure Information Technology SIT addressing challenges of PQC migration and comparison of PQC algorithms [[NW17]](../refs#nw17)
- [Quantencomputerresistente Kryptografie: Aktuelle Aktivitäten und Fragestellungen](https://www.secumedia-shop.net/Deutschland-Digital-Sicher-30-Jahre-BSI): A brief evaluation of the current state of both post-quantum and quantum cryptography [[HLL21]](../refs#hll21)
- [Quantum Safe Cryptography and Security: An introduction, benefits, enablers and challenges](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf): Important use cases for cryptography and potential migration strategies to transition to post-quantum cryptography [[CCD15]](../refs#ccd15)
- [Quantum computers will compromise the security of identity documents](https://www.eurosmart.com/quantum-computers-will-compromise-the-security-of-identity-documents/): The effects of quantum computers on the securiy of electronic identity documents [[E19]](../refs#e19)
- [Report on Post-Quantum Cryptography](https://nvlpubs.nist.gov/nistpubs/ir/2016/NIST.IR.8105.pdf): NIST Internal Report of the current state of quantum computers and post-quantum cryptography. [[CJL16]](../refs#cjl16)
- [Sicherheitsrisiko Quantencomputer](https://hub.kpmg.de/whitepaper-security-risks-of-quantum-computing?utm_campaign=Whitepaper%20-%20Security%20Risks%20of%20Quantum%20Computing%3A%20Recommended%20Actions%20for%20Post-quantum%20Cryptography&utm_source=AEM): Recommendations for post-quanum cryptography [[A19]](../refs#a19)
- [SoK: Challenges of Post-Quantum Digital Signing in Real-world Applications](https://eprint.iacr.org/2019/1374): Applications’ signing requirements against all 6 NIST’s post-quantum cryptography contest round 3 candidate algorithms [[TSZ19]](../refs#tsz19)
- [Standardisierung von Post-Quanten-Kryptografie und Empfehlungen des BSI](https://www.secumedia-shop.net/p/deutschland-digital-sicher-30-jahre-bsi-tg-band-2021): Overview of the current state of standardization of post Quantum cryptography with respect to the BSI recommendations. [[HKW21]](../refs#hkw21)
