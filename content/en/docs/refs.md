---
title: "References"
linktitle: "References"
date: 2021-05-06T00:13:12+02:00
draft: false
type: docs
weight: 12
---
###### [A19]
[AG KPMG. 2019. Sicherheitsrisiko Quantencomputer](https://hub.kpmg.de/whitepaper-security-risks-of-quantum-computing?utm_campaign=Whitepaper%20-%20Security%20Risks%20of%20Quantum%20Computing%3A%20Recommended%20Actions%20for%20Post-quantum%20Cryptography&utm_source=AEM)

###### [A20]
[Aumasson Jean-Philippe. 2020. SPHINCS+ Submission to the NIST post-quantum project, v.3](https://sphincs.org/data/sphincs+-round3-specification.pdf)

###### [A21]
[Avanzi Roberto. 2021. CRYSTALS-Kyber Algorithm Specifications And Supporting Documentation(version 3.01)](https://pq-crystals.org/kyber/data/kyber-specification-round3-20210131.pdf)

###### [AAA+19]
[Alagic Gorjan, Alperin-Sheriff Jacob, Apon Daniel, Cooper David, Dang Quynh, Liu Yi-Kai, Miller Carl, Moody Dustin, Peralta Rene, Perlner Ray, Robinson Angela, Smith-Tone Daniel. 2019. Status report on the first round of the NIST post-quantum cryptography standardization process](https://nvlpubs.nist.gov/nistpubs/ir/2019/NIST.IR.8240.pdf)

###### [ABB+10]
[Acar Tolga, Belenkiy Mira, Bellare Mihir, Cash David. 2010. Cryptographic Agility and Its Relation to Circular Encryption](https://link.springer.com/chapter/10.1007/978-3-642-13190-5_21)

###### [ABB+20]
[Alkim Erdem, Barreto Paulo S. L. M., Bindel Nina, Krämer Juliane, Longa Patrick, Ricardini Jefferson E.. 2020. The Lattice-Based Digital Signature Scheme qTESLA](http://link.springer.com/10.1007/978-3-030-57808-4_22)

###### [ABC+19]
[Andrikos Christos, Batina Lejla, Chmielewski Lukasz, Lerman Liran, Mavroudis Vasilios, Papagiannopoulos Kostas, Perin Guilherme, Rassias Giorgos, Sonnino Alberto. 2019. Location, Location, Location: Revisiting Modeling and Exploitation for Location-Based Side Channel Leakages](http://link.springer.com/10.1007/978-3-030-34618-8_10)

###### [ABE+21]
[Aranha Diego F., Berndt Sebastian, Eisenbarth Thomas, Seker Okan, Takahashi Akira, Wilke Luca, Zaverucha Greg. 2021. Side-Channel Protections for Picnic Signatures](https://eprint.iacr.org/2021/735.pdf)

###### [ABF+17]
[Acar Yasemin, Backes Michael, Fahl Sascha, Garfinkel Simson, Kim Doowon, Mazurek Michelle L., Stransky Christian. 2017. Comparing the Usability of Cryptographic APIs](http://ieeexplore.ieee.org/document/7958576/)

###### [ABG+19]
[Aragon Nicolas, Blazy Olivier, Gaborit Philippe, Hauteville Adrien, Zémor Gilles. 2019. Durandal: A Rank Metric Based Signature Scheme](https://eprint.iacr.org/2018/1192)

###### [ABL+17]
[Aggarwal Divesh, Brennen Gavin, Lee Troy, Santha Miklos, Tomamichel Marco. 2017. Quantum Attacks on Bitcoin, and How to Protect Against Them](https://www.researchgate.net/publication/320727053_Quantum_Attacks_on_Bitcoin_and_How_to_Protect_Against_Them)

###### [ACC+21]
[Alkim Erdem, Cheng Dean Yun-Li, Chung Chi-Ming Marvin, Evkan Hülya, Huang Leo Wei-Lun, Hwang Vincent, Li Ching-Lin Trista, Niederhagen Ruben, Shih Cheng-Jhih, Wälde Julian, Yang Bo-Yin. 2021. Polynomial Multiplication in NTRU Prime](https://tches.iacr.org/index.php/TCHES/article/view/8733)

###### [ACD+18]
[Albrecht Martin R., Curtis Benjamin R., Deo Amit, Davidson Alex, Player Rachel, Postlethwaite Eamonn W., Virdia Fernando, Wunderer Thomas. 2018. Estimate All the \LWE, NTRU\ Schemes!](https://eprint.iacr.org/2018/331)

###### [ACD19]
[Alwen Joël, Coretti Sandro, Dodis Yevgeniy. 2019. The Double Ratchet: Security Notions, Proofs, and Modularization for the Signal Protocol](https://link.springer.com/chapter/10.1007%2F978-3-030-17653-2_5)

###### [ACL+20]
[Amiet Dorian, Curiger Andreas, Leuenberger Lukas, Zbinden Paul. 2020. Defeating NewHope with a Single Trace.](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-44223-1_11.pdf)

###### [ACZ18]
[Amiet Dorian, Curiger Andreas, Zbinden Paul. 2018. FPGA-based Accelerator for Post-Quantum Signature Scheme SPHINCS-256](https://tches.iacr.org/index.php/TCHES/article/view/831)

###### [ADG+19]
[Apon Daniel, Dachman-Soled Dana, Gong Huijing, Katz Jonathan. 2019. Constant-Round Group Key Exchange from the Ring-LWE Assumption](https://link.springer.com/chapter/10.1007/978-3-030-25510-7_11)

###### [ADP+16]
[Alkim Erdem, Ducas Léo, Pöppelmann Thomas, Schwabe Peter. 2016. Post-quantum Key Exchange—A New Hope](https://www.usenix.org/conference/usenixsecurity16/technical-sessions/presentation/alkim)

###### [ADP18]
[Albrecht Martin R., Deo Amit, Paterson Kenneth G.. 2018. Cold Boot Attacks on Ring and Module LWE Keys Under the NTT](https://ia.cr/2018/672)

###### [AFJ14]
[Azarderakhsh Reza, Fishbein Dieter, Jao David. 2014. Eﬃcient Implementations of A Quantum-Resistant Key-Exchange Protocol on Embedded systems](https://cacr.uwaterloo.ca/techreports/2014/cacr2014-20.pdf)

###### [AHH+18]
[Albrecht Martin R., Hanser Christian, Hoeller Andrea, Pöppelmann Thomas, Virdia Fernando, Wallner Andreas. 2018. Implementing RLWE-based Schemes Using an RSA Co-Processor](https://tches.iacr.org/index.php/TCHES/article/view/7338)

###### [AJO+20]
[Alagic Gorjan, Jeffery Stacey, Ozols Maris, Poremba Alexander. 2020. On Quantum Chosen-Ciphertext Attacks and Learning with Errors](https://www.mdpi.com/2410-387X/4/1/10)

###### [AK18]
[An Hyeongcheo, Kim Kwangjo. 2018. QChain: Quantum-resistant and Decentralized PKI using Blockchain](https://caislab.kaist.ac.kr/publication/paper_files/2018/SCIS'18_HC_BC.pdf)

###### [AKK+21]
[Azarderakhsh Reza, Khatib Rami El, Koziel Brian, Langenberg Brandon. 2021. Hardware Deployment of Hybrid PQC](https://eprint.iacr.org/2021/541)

###### [ALC+21]
[Allende Marcos, León Diego, Cerón Sergio, Leal Antonio, Pareja Adrián, Silva Marcelo, Pardo Alejandro, Jones Duncan, Worrall David, Merriman Ben, Gilmore Jonathan, Kitchener Nick, Venegas-Andraca Salvador. 2021. Quantum-resistance in blockchain networks](https://arxiv.org/abs/2106.06640)

###### [AMD+21]
[Abdulgadir Abubakr, Mohajerani Kamyar, Dang Viet, Kaps Jens-Peter, Gaj Kris. 2021. A Lightweight Implementation of Saber Resistant Against Side-Channel Attacks](https://eprint.iacr.org/2021/1452)

###### [AMP+20]
[Apon Daniel, Moody Dustin, Perlner Ray A, Smith-Tone Daniel, Verbel Javier A. 2020. Combinatorial Rank Attacks Against the Rectangular Simple Matrix Encryption Scheme.](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-44223-1_17.pdf)

###### [AMW19]
[Appelbaum Jacob, Martindale Chloe, Wu Peter. 2019. Tiny WireGuard Tweak](https://link.springer.com/chapter/10.1007%2F978-3-030-23696-0_1)

###### [ANI+19]
[Akiyama Koichiro, Nakamura Shuhei, Ito Masaru, Hirata-Kohno Noriko. 2019. A key exchange protocol relying on polynomial maps](https://www.worldscientific.com/doi/full/10.1142/S2661335219500035)

###### [AOT+20]
[Aranha Diego F., Orlandi Claudio, Takahashi Akira, Zaverucha Greg. 2020. Security of Hedged Fiat–Shamir Signatures Under Fault Attacks](https://eprint.iacr.org/2019/956.pdf)

###### [APS15]
[Albrecht Martin R., Player Rachel, Scott Sam. 2015. On the concrete hardness of Learning with Errors](https://eprint.iacr.org/2015/046.pdf)

###### [AR21]
[Askeland Amund, Rønjom Sondre. 2021. A Side-Channel Assisted Attack on NTRU](https://eprint.iacr.org/2021/790)

###### [AS15]
[Aysu Aydin, Schaumont Patrick. 2015. Precomputation Methods for Faster and Greener Post-Quantum Cryptography on Emerging Embedded Platforms](https://eprint.iacr.org/2015/288)

###### [AS20]
[An SangWoo, Seo Seog Chung. 2020. Efficient Parallel Implementations of LWE-Based Post-Quantum Cryptosystems on Graphics Processing Units](https://www.mdpi.com/2227-7390/8/10/1781)

###### [ASW+22]
[Alnahawi N., Schmitt N., Wiesmaier A., Graßmeyer A. Heinemann T.. 2022. On the State of Crypto Agility](https://www.secumedia-shop.net/p/cyber-sicherheit-ist-chefinnen-und-chefsache-tg-band-2022)

###### [ATK+19]
[AbdelHafeez Mahmoud, Taha Mostafa, Khaled Elsayed Esam M., AbdelRaheem Mohamed. 2019. A Study on Transmission Overhead of Post Quantum Cryptography Algorithms in Internet of Things Networks](https://ieeexplore.ieee.org/document/9021842/)

###### [ATT+18]
[Aysu Aydin, Tobah Youssef, Tiwari Mohit, Gerstlauer Andreas, Orshansky Michael. 2018. Horizontal side-channel vulnerabilities of post-quantum key exchange protocols](https://ieeexplore.ieee.org/document/8383894)

###### [AVV+19]
[Armknecht Frederik, Verbauwhede Ingrid, Volkamer Melanie, Yung Moti. 2019. Biggest Failures in Security](https://drops.dagstuhl.de/opus/volltexte/2020/11981/pdf/dagrep_v009_i011_p001_19451.pdf)

###### [AWD+21]
[Alkim Erdem, W. Bos Joppe, Ducas Léo, Longa Patrick, Mironov Ilya, Naehrig Michael, Nikolaenko Valeria, Peikert Chris, Raghunathan Ananth, Stebila Douglas. 2021. FrodoKEM - Learning With Errors Key Encapsulation](https://frodokem.org/files/FrodoKEM-specification-20210604.pdf)

###### [AWG+21]
[Alnahawi Nouri, Wiesmaier Alexander, Grasmeyer Tobias, Geißler Julian, Bauspieß Pia, Heinemann Andreas. 2021. On the State of Post-Quantum Cryptography Migration](https://dl.gi.de/bitstream/handle/20.500.12116/37746/J1-2.pdf?sequence=1&isAllowed=y)

###### [AZC+19]
[Astrand L. Hornquist, Zhu L., Cullen M., Hudson G.. 2019. Public Key Cryptography for Initial Authentication in Kerberos (PKINIT) Algorithm Agility](https://www.rfc-editor.org/info/rfc8636)

###### [B09]
[Bernstein Daniel J.. 2009. Introduction to post-quantum cryptography](http://link.springer.com/10.1007/978-3-540-88702-7_1)

###### [B10]
[Bernstein Daniel J.. 2010. Grover vs. McEliece](https://rd.springer.com/content/pdf/10.1007%2F978-3-642-12929-2_6.pdf)

###### [B16]
[Braithwaite Matt. 2016. Experimenting with Post-Quantum Cryptography](https://security.googleblog.com/2016/07/experimenting-with-post-quantum.html)

###### [B18]
[Bernstein Daniel. 2018. PQCRYPTO Post-Quantum Cryptography for Long-Term Security](https://pqcrypto.eu.org/deliverables/d5.2-final.pdf)

###### [B19]
[Buchmann Nicolas. 2019. Strengthening trust in the identity life cycle: Enhancing electronic machine readable travel documents due to advances in security protocols and infrastructure](https://refubium.fu-berlin.de/handle/fub188/24055)

###### [B20]
[Bernstein Daniel J. 2020. NTRU Prime: Round 3](https://ntruprime.cr.yp.to/nist/ntruprime-20201007.pdf)

###### [B21]
[Bernstein Daniel J.. 2021. Fast verified post-quantum software, part 1: RAM subroutines](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/bernstein-fast-verified-pq-software-pqc2021.pdf)

###### [BB21]
[Bardet Magali, Briaud Pierre. 2021. An algebraic approach to the Rank Support Learning problem](https://arxiv.org/abs/2103.03558)

###### [BBC+21]
[Bernstein Daniel J, Brumley Billy Bob, Chen Ming-Shing, Tuveri Nicola. 2021. OpenSSLNTRU: Faster post-quantum TLS key exchange](https://eprint.iacr.org/2021/826)

###### [BBF+19]
[Bindel Nina, Brendel Jacqueline, Fischlin Marc, Goncalves Brian, Stebila Douglas. 2019. Hybrid Key Encapsulation Mechanisms and Authenticated Key Exchange](http://link.springer.com/10.1007/978-3-030-25510-7_12)

###### [BBG+19]
[Bindel Nina, Braun Johannes, Gladiator Luca, Stöckert Tobias, Wirth Johannes. 2019. X.509-Compliant Hybrid Certificates for the Post-Quantum Transition](https://joss.theoj.org/papers/10.21105/joss.01606)

###### [BBK16]
[Bindel Nina, Buchmann Johannes, Krämer Juliane. 2016. Lattice-Based Signature Schemes and their Sensitivity to Fault Attacks](https://eprint.iacr.org/2016/415)

###### [BBP+21]
[Barenghi Alessandro, Biasse Jean-François, Persichetti Edoardo, Santini Paolo. 2021. LESS-FM: Fine-tuning Signatures from a Code-based Cryptographic Group Action.](https://eprint.iacr.org/2021/396)

###### [BBR16]
[Buchmann Dr Johannes, Bindel Nina, Rieß Susanne. 2016. An Analysis of Lattice-Based Key Exchange Protocols](https://download.hrz.tu-darmstadt.de/media/FB20/Dekanat/Publikationen/CDC/Susanne_Riess.master.pdf)

###### [BBR18]
[Bindel Nina, Buchmann Johannes, Rieß Susanne. 2018. Comparing apples with apples: performance analysis of lattice-based authenticated key exchange protocols](http://link.springer.com/10.1007/s10207-017-0397-6)

###### [BC20]
[Banerjee Utsav, Chandrakasan Anantha P.. 2020. Efficient Post-Quantum TLS Handshakes using Identity-Based Key Exchange from Lattices](https://ieeexplore.ieee.org/abstract/document/9148829)

###### [BC21]
[Bombar Maxime, Couvreur Alain. 2021. Decoding supercodes of Gabidulin codes and applications to cryptanalysis](https://arxiv.org/abs/2103.02700)

###### [BCD+16]
[Bos Joppe, Costello Craig, Ducas Leo, Mironov Ilya, Naehrig Michael, Nikolaenko Valeria, Raghunathan Ananth, Stebila Douglas. 2016. Frodo: Take off the Ring! Practical, Quantum-Secure Key Exchange from LWE](https://dl.acm.org/doi/10.1145/2976749.2978425)

###### [BCG+07]
[Baldi M., Chiaraluce F., Garello R., Mininni F.. 2007. Quasi-Cyclic Low-Density Parity-Check Codes in the McEliece Cryptosystem](https://ieeexplore.ieee.org/document/4288832)

###### [BCG+09]
[Berger Thierry P., Cayrel Pierre-Louis, Gaborit Philippe, Otmani Ayoub. 2009. Reducing Key Length of the McEliece Cryptosystem](https://www.researchgate.net/publication/221462135_Reducing_Key_Length_of_the_McEliece_Cryptosystem)

###### [BCM21]
[Behnia Rouzbeh, Chen Yilei, Masny Daniel. 2021. On Removing Rejection Conditions in Practical Lattice-Based Signatures](https://eprint.iacr.org/2021/924)

###### [BCN+14]
[Bos Joppe W., Costello Craig, Naehrig Michael, Stebila Douglas. 2014. Post-quantum key exchange for the TLS protocol from the ring learning with errors problem](https://eprint.iacr.org/2014/599)

###### [BCO+20]
[Boschini Cecilia, Camenisch Jan, Ovsiankin Max, Spooner Nicholas. 2020. Efficient Post-quantum SNARKs for RSIS and RLWE and Their Applications to Privacy.](https://eprint.iacr.org/2020/1190)

###### [BCS13]
[Bernstein Daniel J., Chou Tung, Schwabe Peter. 2013. McBits: Fast Constant-Time Code-Based Cryptography](https://eprint.iacr.org/2015/610)

###### [BD15]
[Braga Alexandre, Dahab Ricardo. 2015. A Survey on Tools and Techniques for the Programming and Verification of Secure Cryptographic Software](https://www.researchgate.net/publication/283730120_A_Survey_on_Tools_and_Techniques_for_the_Programming_and_Verification_of_Secure_Cryptographic_Software)

###### [BD16]
[Braga Alexandre, Dahab Ricardo. 2016. Towards a Methodology for the Development of Secure Cryptographic Software](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7861647)

###### [BDK+18]
[Bos Joppe, Ducas Leo, Kiltz Eike, Lepoint T, Lyubashevsky Vadim, Schanck John M., Schwabe Peter, Seiler Gregor, Stehle Damien. 2018. CRYSTALS - Kyber: A CCA-Secure Module-Lattice-Based KEM](https://ieeexplore.ieee.org/document/8406610/)

###### [BDK+21]
[Bai Shi, Ducas Léo, Kiltz Eike, Lepoint Tancrède, Lyubashevsky Vadim, Schwabe Peter, Seiler Gregor, Stehlé Damien. 2021. CRYSTALS-Dilithium - Algorithm Specifications and Supporting Documentation](https://pq-crystals.org/dilithium/data/dilithium-specification-round3-20210208.pdf)

###### [BDP+20]
[Beullens Ward, Disson Lucas, Pedersen Robi, Vercauteren Frederik. 2020. CSI-RAShi: Distributed key generation for CSIDH.](https://eprint.iacr.org/2020/1323)

###### [BEP+21]
[Bert Pauline, Eberhart Gautier, Prabel Lucas, Roux-Langlois Adeline, Sabt Mohamed. 2021. Implementation of Lattice Trapdoors on Modules and Applications](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-81293-5_11.pdf)

###### [BFG+19]
[Brendel Jacqueline, Fischlin Marc, Günther Felix, Janson Christian, Stebila D.. 2019. Challenges in Proving Post-Quantum Key Exchanges Based on Key Encapsulation Mechanisms](https://eprint.iacr.org/2019/1356/20191127:081325)

###### [BFG+21]
[Brendel Jacqueline, Fiedler Rune, Günther Felix, Janson Christian, Stebila Douglas. 2021. Post-quantum Asynchronous Deniable Key Exchange and the Signal Handshake](https://eprint.iacr.org/2021/769)

###### [BGR17]
[Bhattacharya Sauvik, Garcia-Morchon Oscar, Rietman Ronald. 2017. spKEX: An optimized lattice-based key-exchange](https://eprint.iacr.org/2017/709)

###### [BHK+19]
[Bernstein Daniel J., Hülsing Andreas, Kölbl Stefan, Niederhagen Ruben, Rijneveld Joost, Schwabe Peter. 2019. The SPHINCS+ Signature Framework](https://dl.acm.org/doi/10.1145/3319535.3363229)

###### [BKN+20]
[Bürstinghaus-Steinbach Kevin, Krauß Christoph, Niederhagen Ruben, Schneider Michael. 2020. Post-Quantum TLS on Embedded Systems](https://eprint.iacr.org/2020/308)

###### [BKS19]
[Botros Leon, Kannwischer Matthias J., Schwabe Peter. 2019. Memory-Efficient High-Speed Implementation of Kyber on Cortex-M4](http://link.springer.com/10.1007/978-3-030-23696-0_11)

###### [BL17]
[Bernstein Daniel J., Lange Tanja. 2017. Post-quantum cryptography](http://www.nature.com/articles/nature23461)

###### [BL20]
[Bernstein Daniel J., Lange Tanja. 2020. McTiny: Fast High-Confidence Post-Quantum Key Erasure for Tiny Network Servers](https://www.usenix.org/conference/usenixsecurity20/presentation/bernstein)

###### [BM17]
[Both Leif, May Alexander. 2017. Optimizing BJMM with Nearest Neighbors : Full Decoding in 2 2 n / 21 and McEliece Security](https://www.cits.ruhr-uni-bochum.de/imperia/md/content/may/paper/bjmm+.pdf)

###### [BM18]
[Both Leif, May Alexander. 2018. Decoding Linear Codes with High Error Rate and Its Impact for LPN Security](https://rd.springer.com/content/pdf/10.1007%2F978-3-319-79063-3_2.pdf)

###### [BM21]
[Blanks Tamar Lichter, Miller Stephen D. 2021. Generating cryptographically-strong random lattice bases](https://arxiv.org/abs/2102.06344)

###### [BMF+21]
[Bellizia Davide, Mrabet Nadia El, Fournaris Apostolos P., Pontie Simon, Regazzoni Francesco, Standaert Francois-Xavier, Tasso Elise, Valea Emanuele. 2021. Post-Quantum Cryptography: Challenges and Opportunities for Robust and Secure HW Design](https://ieeexplore.ieee.org/document/9568301/)

###### [BNG21]
[Beckwith Luke, Nguyen Duc Tri, Gaj Kris. 2021. High-Performance Hardware Implementation of CRYSTALS-Dilithium](https://eprint.iacr.org/2021/1451)

###### [BOG20]
[Bischof Mario, Oder Tobias, Güneysu Tim. 2020. Efficient Microcontroller Implementation of BIKE](http://link.springer.com/10.1007/978-3-030-41025-4_3)

###### [BPS20]
[Barker William, Polk William, Souppaya Murugiah. 2020. Getting Ready for Post-Quantum Cryptography:: Explore Challenges Associated with Adoption and Use of Post-Quantum Cryptographic Algorithms](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.05262020-draft.pdf)

###### [BRP20]
[Borges Fábio, Reis Paulo Ricardo, Pereira Diogo. 2020. A Comparison of Security and its Performance for Key Agreements in Post-Quantum Cryptography](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9153901)

###### [BRV20]
[Bos Joppe, Renes Joost, Vredendaal Christine. 2020. Post-Quantum Cryptography with Contemporary Co-Processors](https://eprint.iacr.org/2020/1303)

###### [BS08]
[Biswas Bhaskar, Sendrier Nicolas. 2008. McEliece Cryptosystem Implementation: Theory and Practice](https://link.springer.com/chapter/10.1007/978-3-540-88403-3_4)

###### [BS20]
[Bindel Nina, Schanck John M. 2020. Decryption Failure Is More Likely After Success.](https://eprint.iacr.org/2019/1392.pdf)

###### [BS21]
[Barker William, Souppaya Murugiah. 2021. [Project Description] Migration to Post-Quantum Cryptography (Draft)](https://csrc.nist.gov/publications/detail/white-paper/2021/06/04/migration-to-post-quantum-cryptography/draft)

###### [BSN+19]
[Basu Kanad, Soni Deepraj, Nabeel Mohammed, Karri Ramesh. 2019. NIST Post-Quantum Cryptography - A Hardware Evaluation Study](https://eprint.iacr.org/2019/047)

###### [BSN21]
[Barker William, Souppaya Murugiah, Newhouse William. 2021. [Project Description] Migration to Post-Quantum Cryptography](https://csrc.nist.gov/publications/detail/white-paper/2021/08/04/migration-to-post-quantum-cryptography/final)

###### [BUC19]
[Banerjee Utsav, Ukyab Tenzin S., Chandrakasan Anantha P.. 2019. Sapphire: A Configurable Crypto-Processor for Post-Quantum Lattice-based Protocols](https://tches.iacr.org/index.php/TCHES/article/view/8344)

###### [BUG+21]
[Berthet Quentin, Upegui Andres, Gantel Laurent, Duc Alexandre, Traverso Giulia. 2021. An Area-Efficient SPHINCS+ Post-Quantum Signature Coprocessor](https://ieeexplore.ieee.org/document/9460703/)

###### [BZ21]
[Bobrysheva Julia, Zapechnikov Sergey. 2021. Post-quantum Group Key Agreement Scheme](https://eprint.iacr.org/2020/873)

###### [Bd20]
[Beullens Ward, de Saint Guilhem Cyprien Delpech. 2020. LegRoast: Efficient post-quantum signatures from the Legendre PRF.](https://eprint.iacr.org/2020/128)

###### [C17]
[Chen Debiao He Kim-Kwang Raymond Choo Jianhua Dongqing Xu. 2017. Provably Secure Three-party Password Authenticated Key Exchange Protocol Based On Ring Learning With Error](https://eprint.iacr.org/2017/360)

###### [C19]
[Campbell Robert. 2019. Evaluation of Post-Quantum Distributed Ledger Cryptography](https://jbba.scholasticahq.com/article/7679.pdf)

###### [C20]
[Corporation ISARA. 2020. Managing Cryptographic and Quantum Risk](https://www.isara.com/downloads/guides/Managing%20Cryptographic%20and%20Quantum%20Risk.pdf)

###### [C21]
[Costello Craig. 2021. The Case for SIKE: A Decade of the Supersingular Isogeny Problem](https://eprint.iacr.org/2021/543)

###### [CBH+18]
[Chalkias Konstantinos, Brown James, Hearn Mike, Lillehagen Tommy, Nitto Igor, Schroeter Thomas. 2018. Blockchained Post-Quantum Signatures](https://ieeexplore.ieee.org/document/8726842)

###### [CC20]
[Crockett Eric, Campagna Matt. 2020. Internet-Draft: Hybrid Post-Quantum Key Encapsulation Methods (PQ KEM) for Transport Layer Security 1.2 (TLS)](https://datatracker.ietf.org/doc/html/draft-campagna-tls-bike-sike-hybrid-03)

###### [CC21]
[Chen Ming-Shing, Chou Tung. 2021. Classic McEliece on the ARM Cortex-M4](https://eprint.iacr.org/2021/492)

###### [CCA+21]
[Chowdhury Sreeja, Covic Ana, Acharya Rabin Yu, Dupee Spencer, Ganji Fatemeh, Forte Domenic. 2021. Physical security in the post-quantum era: A survey on side-channel analysis, random number generators, and physically unclonable functions](http://link.springer.com/10.1007/s13389-021-00255-w)

###### [CCD+15]
[Campagna Matthew, Chen Lidong, Dagdelen O., Ding Jintai, Fernick J, Gisin Nicolas, Hayford Donald, Jennewein Thomas, Lütkenhaus Norbert, Mosca Michele. 2015. Quantum Safe Cryptography and Security: An introduction, benefits, enablers and challenges](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf)

###### [CCU+20]
[Chou Tung, Cid Carlos, UiB Simula, Gilcher Jan, Lange Tanja, Maram Varun, Misoczki Rafael, Niederhagen Ruben, Paterson Kenneth G, Persichetti Edoardo, others . 2020. Classic McEliece: conservative code-based cryptography](https://classic.mceliece.org/nist/mceliece-20201010.pdf)

###### [CCW+14]
[Chang Yun-An, Chen Ming-Shing, Wu Jong-Shian, Yang Bo-Yin. 2014. Postquantum SSL/TLS for Embedded Systems](http://ieeexplore.ieee.org/document/6978621/)

###### [CD20]
[Castryck Wouter, Decru Thomas. 2020. CSIDH on the surface](https://eprint.iacr.org/2019/1404)

###### [CDF+20]
[Cremers Cas, Düzlü Samed, Fiedler Rune, Fischlin Marc, Janson Christian. 2020. BUFFing signature schemes beyond unforgeability and the case of post-quantum signatures](https://eprint.iacr.org/2020/1525)

###### [CDG+17]
[Chase Melissa, Derler David, Goldfeder Steven, Orlandi Claudio, Ramacher Sebastian, Rechberger Christian, Slamanig Daniel, Zaverucha Greg. 2017. Post-Quantum Zero-Knowledge and Signatures from Symmetric-Key Primitives](https://eprint.iacr.org/2017/279)

###### [CDG+20]
[Cheng Hao, Dinu Daniel, Großschädl Johann, Rønne Peter B., Ryan Peter Y. A.. 2020. A Lightweight Implementation of NTRU Prime for the Post-quantum Internet of Things](http://link.springer.com/10.1007/978-3-030-41702-4_7)

###### [CDH+19]
[Chen Cong, Danba Oussama, Hostein Jerey, Hülsing Andreas, Rijneveld Joost, M. Schanck John, Schwabe Peter, Whyte William, Zhang Zhenfei. 2019. NTRU - Algorithm Specifications And Supporting Documentation](https://ntru.org/f/ntru-20190330.pdf)

###### [CDJ+20]
[Costello Craig, De Feo Luca, Jao David, Longa Patrick, Naehrig Michael, Renes Joost. 2020. Supersingular Isogeny Key Encapsulation](https://sike.org/files/SIDH-spec.pdf)

###### [CDS+20]
[Cohen Alejandro, D'Oliveira Rafael G. L., Salamatian Salman, Medard Muriel. 2020. Network Coding-Based Post-Quantum Cryptography](http://arxiv.org/abs/2009.01931)

###### [CGH+21]
[Chen Jiahui, Gan Wensheng, Hu Muchuang, Chen Chien-Ming. 2021. On the Construction of a Post-Quantum Blockchain](https://www.researchgate.net/publication/350667420_On_the_Construction_of_a_Post-Quantum_Blockchain)

###### [CGR+21]
[Cheng Hao, GroBschadl Johann, Ronne Peter B., Ryan Peter Y.A.. 2021. AVRNTRU: Lightweight NTRU-based Post-Quantum Cryptography for 8-bit AVR Microcontrollers](https://ieeexplore.ieee.org/document/9474033/)

###### [CHK+21]
[Chung Chi-Ming Marvin, Hwang Vincent, Kannwischer Matthias J., Seiler Gregor, Shih Cheng-Jhih, Yang Bo-Yin. 2021. NTT Multiplication for NTT-unfriendly Rings:](https://tches.iacr.org/index.php/TCHES/article/view/8791)

###### [CJL+16]
[Chen Lily, Jordan Stephen, Liu Yi-Kai, Moody Dustin, Peralta Rene, Perlner Ray, Smith-Tone Daniel. 2016. Report on Post-Quantum Cryptography](https://nvlpubs.nist.gov/nistpubs/ir/2016/NIST.IR.8105.pdf)

###### [CKM21]
[Campos Fabio, Krämer Juliane, Müller Marcel. 2021. Safe-Error Attacks on SIKE and CSIDH](https://eprint.iacr.org/2021/1132)

###### [CKY21]
[Chou Tung, Kannwischer Matthias J., Yang Bo-Yin. 2021. Rainbow on Cortex-M4](https://eprint.iacr.org/2021/532)

###### [CLM+18]
[Castryck Wouter, Lange Tanja, Martindale Chloe, Panny Lorenz, Renes Joost. 2018. CSIDH: An Efficient Post-Quantum Commutative Group Action](https://eprint.iacr.org/2018/383)

###### [CLO21]
[Campagna Matt, LaMacchia Brian, Ott David. 2021. Post Quantum Cryptography: Readiness Challenges and the Approaching Storm](http://arxiv.org/abs/2101.01269)

###### [CMC19]
[Clancy T. Charles, McGwier Robert W., Chen Lidong. 2019. Post-Quantum Cryptography and 5G Security: Tutorial](https://doi.org/10.1145/3317549.3324882)

###### [CPS19]
[Crockett Eric, Paquin Christian, Stebila Douglas. 2019. Prototyping post-quantum and hybrid key exchange and authentication in TLS and SSH](https://eprint.iacr.org/2019/858)

###### [CS19]
[Costello Craig, Smith Benjamin. 2019. The supersingular isogeny problem in genus 2 and beyond](https://arxiv.org/abs/1912.00701)

###### [CS20]
[Cho Joo Yeon, Sergeev Andrew. 2020. Post-quantum MACsec key agreement for ethernet networks](https://doi.org/10.1145/3407023.3409220)

###### [CU16]
[Chen Liqun, Urian Rainer. 2016. Algorithm Agility – Discussion on TPM 2.0 ECC Functionalities](http://link.springer.com/10.1007/978-3-319-49100-4_6)

###### [D19]
[D’Anvers Jan-Pieter. 2019. SABER: Mod-LWR based KEM (Round 3 Submission)](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/files/saberspecround3.pdf)

###### [D20]
[D’Anvers Jan-Pieter. 2020. A Side-channel Resistant Implementation of SABER](https://eprint.iacr.org/2020/733.pdf)

###### [D21]
[Dahmen-Lhuissier Sabine. 2021. ETSI - Quantum - Safe Cryptography, Computing Cryptography](https://www.etsi.org/technologies/quantum-safe-cryptography)

###### [DAL+17]
[Ding Jintai, Alsayigh Saed, Lancrenon Jean, Rv Saraswathy, Snook Michael. 2017. Provably Secure Password Authenticated Key Exchange Based on RLWE for the Post-Quantum World](http://link.springer.com/10.1007/978-3-319-52153-4_11)

###### [DBK21]
[Dabra Vivek, Bala Anju, Kumari Saru. 2021. Reconciliation based key exchange schemes using lattices: a review](https://ideas.repec.org/a/spr/telsys/v77y2021i2d10.1007_s11235-021-00759-0.html)

###### [DDS+20]
[Ding Jintai, Deaton Joshua, Schmidt Kurt, Vishakha , Zhang Zheng. 2020. Cryptanalysis of the Lifted Unbalanced Oil Vinegar Signature Scheme](https://eprint.iacr.org/2019/1490.pdf)

###### [DFA+19]
[Dang Viet B., Farahmand Farnoud, Andrzejczak Michal, Gaj Kris. 2019. Implementing and Benchmarking Three Lattice-Based Post-Quantum Cryptography Algorithms Using Software/Hardware Codesign](https://ieeexplore.ieee.org/document/8977901/)

###### [DFA+20]
[Dang Viet Ba, Farahmand Farnoud, Andrzejczak Michal, Mohajerani Kamyar, Nguyen Duc Tri, Gaj Kris. 2020. Implementation and Benchmarking of Round 2 Candidates in the NIST Post-Quantum Cryptography Standardization Process Using Hardware and Software/Hardware Co-design Approaches](https://eprint.iacr.org/2020/795)

###### [DG21]
[Dobson Samuel, Galbraith Steven D.. 2021. Post-Quantum Signal Key Agreement with SIDH](https://eprint.iacr.org/2021/1187)

###### [DGG+18]
[Deneuville Jean-Christophe, Gaborit Philippe, Guo Qian, Johansson Thomas. 2018. Ouroboros-E: An Efficient Lattice-based Key-Exchange Protocol](https://ieeexplore.ieee.org/document/8437940/)

###### [DGK+20]
[Drucker Nir, Gueron Shay, Kostic Dusan, Ding J, Tillich JP. 2020. QC-MDPC Decoders with Several Shades of Gray.](https://eprint.iacr.org/2019/1423)

###### [DHA+21]
[Dr. Rachid El Bansarkhani, Hans-Peter Fischer, Andreas Schwab, Dr. Michael Riecker, Dr. Juliane Krämer. 2021. Definition of a quantum computer resistant encryption scheme](https://www.din.de/en/innovation-and-research/din-spec-en/current-din-specs/wdc-beuth:din21:336773230)

###### [DHM+20]
[Das Bhargav, Holcomb Amelia, Mosca Michele, Pereira Geovandro. 2020. PQ-Fabric: A Permissioned Blockchain Secure from Both Classical and Quantum Attacks](https://ieeexplore.ieee.org/document/9461070)

###### [DHP20]
[Dowling Benjamin, Hansen Torben Brandt, Paterson Kenneth G. 2020. Many a Mickle Makes a Muckle: A Framework for Provably Quantum-Secure Hybrid Key Exchange](https://eprint.iacr.org/2020/099.pdf)

###### [DJC+19]
[Ding Hang-chao, Jiang Han, Cai Jie, Wang Chen-guang, Zou Jing, Xu Qiu-Liang. 2019. Research on Key Exchange Protocol Based on LWE](https://ieeexplore.ieee.org/document/9023645)

###### [DK21]
[Duc Tri Nguyen, Kris Gaj. 2021. Optimized Software Implementations of CRYSTALS-Kyber, NTRU, and Saber Using NEON-Based Special Instructions of ARMv8](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/hess-fast-quantum-safe-pqc2021.pdf)

###### [DKL+18]
[Ducas Léo, Kiltz Eike, Lepoint Tancrède, Lyubashevsky Vadim, Schwabe Peter, Seiler Gregor, Stehlé Damien. 2018. CRYSTALS-Dilithium: A Lattice-Based Digital Signature Scheme](https://tches.iacr.org/index.php/TCHES/article/view/839)

###### [DKS+18]
[D'Anvers Jan-Pieter, Karmakar Angshuman, Sinha Roy Sujoy, Vercauteren Frederik. 2018. Saber: Module-LWR Based Key Exchange, CPA-Secure Encryption and CCA-Secure KEM](https://link.springer.com/chapter/10.1007/978-3-319-89339-6_16)

###### [DLW19]
[Dong Xiaoyang, Li Zheng, Wang Xiaoyun. 2019. Quantum cryptanalysis on some generalized Feistel schemes](http://link.springer.com/10.1007/s11432-017-9436-7)

###### [DMG21]
[Dang Viet Ba, Mohajerani Kamyar, Gaj Kris. 2021. High-Speed Hardware Architectures and Fair FPGA Benchmarking of CRYSTALS-Kyber, NTRU, and Saber](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/gaj-high-speed-hardware-gmu-pqc2021.pdf)

###### [DMV+15]
[Duta Cristina-Loredana, Mocanu Bogdan-Costel, Vladescu Florin-Alexandru, Gheorghe L., Tapus N.. 2015. Evaluation Framework for Security and Resource Consumption of Cryptographic Algorithms](https://www.semanticscholar.org/paper/Evaluation-Framework-for-Security-and-Resource-of-Duta-Mocanu/c9e56cd2c10ccd3f01f911739eb369a7770abf61)

###### [DP17]
[Ding Jintai, Petzoldt Albrecht. 2017. Current State of Multivariate Cryptography](https://ieeexplore.ieee.org/document/8012305)

###### [DS05]
[Ding Jintai, Schmidt Dieter. 2005. Rainbow, a New Multivariable Polynomial Signature Scheme](https://link.springer.com/chapter/10.1007/11496137_12)

###### [DZD+20]
[Ding Jintai, Zhang Zheng, Deaton Joshua, Wang Lih-Chung. 2020. A Complete Cryptanalysis of the Post-Quantum Multivariate Signature Scheme Himq-3](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-61078-4_24.pdf)

###### [E19]
[Eurosmart . 2019. Quantum computers will compromise the security of identity documents](https://www.eurosmart.com/quantum-computers-will-compromise-the-security-of-identity-documents/)

###### [E20]
[Eriksson Hampus. 2020. Implementing and Evaluating the Quantum Resistant Cryptographic Scheme Kyber on a Smart Card](http://www.diva-portal.org/smash/get/diva2:1464485/FULLTEXT01.pdf)

###### [E21]
[Evgnosia-Alexandra Kelesidis. 2021. A note on Post Quantum Onion Routing](https://eprint.iacr.org/2021/111)

###### [EGH+09]
[Eisenbarth Thomas, Güneysu Tim, Heyse Stefan, Paar Christof. 2009. MicroEliece: McEliece for Embedded Devices](https://link.springer.com/chapter/10.1007/978-3-642-04138-9_4)

###### [EGM+20]
[Escribano Pablos José Ignacio, González Vasco María Isabel, Marriaga Misael Enrique, Pérez del Pozo Ángel Luis. 2020. Compiled Constructions towards Post-Quantum Group Key Exchange: A Design from Kyber](https://www.mdpi.com/2227-7390/8/10/1853)

###### [ENS17]
[El Hajji Said, Nitaj Abderrahmane, Souidi El Mamoun. 2017. Codes, Cryptology and Information Security: Second International Conference, C2SI 2017, Rabat, Morocco, April 10–12, 2017, Proceedings - In Honor of Claude Carlet](http://link.springer.com/10.1007/978-3-319-55589-8)

###### [EZS+19]
[Esgin Muhammed F., Zhao Raymond K., Steinfeld Ron, Liu Joseph K., Liu Dongxi. 2019. MatRiCT: Efficient, Scalable and Post-Quantum Blockchain Confidential Transactions Protocol](https://eprint.iacr.org/2019/1287)

###### [F18]
[Fritzmann Tim. 2018. Error-Correcting Codes for Lattice-Based Key Exchange](https://dl.gi.de/handle/20.500.12116/17023)

###### [F20]
[Fouque Pierre-Alain. 2020. Falcon: Fast-Fourier Lattice-based Compact Signatures over NTRU Specification v1.2](https://falcon-sign.info/falcon.pdf)

###### [FF20]
[Fernández-Caramés Tiago, Fraga-Lamas Paula. 2020. Towards Post-Quantum Blockchain: A Review on Blockchain Cryptography Resistant to Quantum Computing Attacks](https://ieeexplore.ieee.org/abstract/document/8967098)

###### [FGR+21]
[Fouque Pierre-Alain, Gérard François, Rossi Mélissa, Yu Yang. 2021. Zalcon: an alternative FPA-free NTRU sampler for Falcon](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/yang-zalcon-pqc2021.pdf)

###### [FHR21]
[Fauzi Prastudy, Hovd Martha Norberg, Raddum H\textbackslashaavard. 2021. A Practical Adaptive Key Recovery Attack on the LGM (GSW-like) Cryptosystem](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-81293-5_25.pdf)

###### [FKI+20]
[Furue Hiroki, Kinjo Koha, Ikematsu Yasuhiko, Wang Yacheng, Takagi Tsuyoshi. 2020. A Structural Attack on Block-Anti-Circulant UOV at SAC 2019](https://link.springer.com/content/pdf/10.1007%252F978-3-030-44223-1_18.pdf)

###### [FKM+20]
[Fluhrer Scott, Kampanakis Panos, McGrew David, Smyslov Valery. 2020. Mixing Preshared Keys in the Internet Key Exchange Protocol Version 2 (IKEv2) for Post-quantum Security](https://rfc-editor.org/rfc/rfc8784.txt)

###### [FLP14]
[Fischlin Marc, Lehmann Anja, Pietrzak Krzysztof. 2014. Robust Multi-Property Combiners for Hash Functions](http://link.springer.com/10.1007/s00145-013-9148-7)

###### [FMK16]
[Fluhrer Scott, McGrew David, Kampanakis Panos. 2016. Postquantum Preshared Keys for IKEv2](https://datatracker.ietf.org/doc/html/draft-fluhrer-qr-ikev2-03)

###### [FP21]
[Fouotsa Tako Boris, Petit Christophe. 2021. SimS: a Simplification of SiGamal](https://eprint.iacr.org/2021/218)

###### [FSM+19]
[Fritzmann Tim, Sharif Uzair, Muller-Gritschneder Daniel, Reinbrecht Cezar, Schlichtmann Ulf, Sepulveda Johanna. 2019. Towards Reliable and Secure Post-Quantum Co-Processors based on RISC-V](https://ieeexplore.ieee.org/document/8715173/)

###### [FSS20]
[Fritzmann Tim, Sigl Georg, Sepúlveda Johanna. 2020. RISQ-V: Tightly Coupled RISC-V Accelerators for Post-Quantum Cryptography](https://tches.iacr.org/index.php/TCHES/article/view/8683)

###### [FSX+12]
[Fujioka Atsushi, Suzuki Koutarou, Xagawa Keita, Yoneyama Kazuki. 2012. Strongly Secure Authenticated Key Exchange from Factoring, Codes, and Lattices](https://www.iacr.org/archive/pkc2012/72930468/72930468.pdf)

###### [FSX+13]
[Fujioka Atsushi, Suzuki Koutarou, Xagawa Keita, Yoneyama Kazuki. 2013. Practical and post-quantum authenticated key exchange from one-way secure key encapsulation mechanism](https://dl.acm.org/doi/10.1145/2484313.2484323)

###### [FTT+18]
[Fujioka Atsushi, Takashima Katsuyuki, Terada Shintaro, Yoneyama Kazuki. 2018. Supersingular Isogeny Diffie-Hellman Authenticated Key Exchange](https://eprint.iacr.org/2018/730)

###### [FVS19]
[Fritzmann Tim, Vith Jonas, Sepúlveda Johanna. 2019. Post-quantum key exchange mechanism for safety critical systems](https://hss-opus.ub.ruhr-uni-bochum.de/opus4/6653)

###### [FVS20]
[Fritzmann Tim, Vith Jonas, Sepulveda Johanna. 2020. Strengthening Post-Quantum Security for Automotive Systems](https://ieeexplore.ieee.org/document/9217638/)

###### [G17]
[Gheorghiu Vlad. 2017. Quantum-Proofing the Blockchain](https://www.blockchainresearchinstitute.org/project/quantum-proofing-the-blockchain/)

###### [G18]
[Gaj Kris. 2018. Challenges and Rewards of Implementing and Benchmarking Post-Quantum Cryptography in Hardware](https://dl.acm.org/doi/10.1145/3194554.3194615)

###### [G96]
[Grover Lov K.. 1996. A fast quantum mechanical algorithm for database search](http://portal.acm.org/citation.cfm?doid=237814.237866)

###### [GAB19]
[Grote Olaf, Ahrens Andreas, Benavente-Peces Cesar. 2019. A Review of Post-quantum Cryptography and Crypto-agility Strategies](https://ieeexplore.ieee.org/document/8755433/)

###### [GCC+18]
[Gao Yu-Long, Chen Xiu-Bo, Chen Yu-Ling, Sun Ying, Niu Xin-Xin, Yang Yi-Xian. 2018. A Secure Cryptocurrency Scheme Based on Post-Quantum Blockchain](https://ieeexplore.ieee.org/document/8340794)

###### [GDL+17]
[Gao Xinwei, Ding Jintai, Li Lin, RV Saraswathy, Liu Jiqiang. 2017. Efficient Implementation of Password-Based Authenticated Key Exchange from RLWE and Post-Quantum TLS](https://eprint.iacr.org/2017/1192)

###### [GDS+19]
[Gao Xinwei, Ding Jintai, Saraswathy R.v., Li Lin, Liu Jiqiang. 2019. Comparison analysis and efficient implementation of reconciliation-based RLWE key exchange protocol](https://www.inderscienceonline.com/doi/abs/10.1504/IJHPCN.2019.097505)

###### [GFL19]
[Gazdag Stefan-Lukas, Friedl Markus, Loebenberger Daniel. 2019. Post-Quantum Software Updates](http://dl.gi.de/handle/20.500.12116/25014)

###### [GGG+21]
[Gazdag Stefan-Lukas, Grundner-Culemann Sophia, Guggemos Tobias, Heider Tobias, Loebenberger Daniel. 2021. A formal analysis of IKEv2’s post quantum extension](https://www.doi.org/xx.xxx//xxxx)

###### [GHK+21]
[Gonzalez Ruben, Hülsing Andreas, Kannwischer Matthias J., Krämer Juliane, Lange Tanja, Stöttinger Marc, Waitz Elisabeth, Wiggers Thom, Yang Bo-Yin. 2021. Verifying Post-Quantum Signatures in 8 kB of RAM](https://eprint.iacr.org/2021/662.pdf)

###### [GHL+16]
[Groot Bruinderink Leon, Hülsing Andreas, Lange Tanja, Yarom Yuval. 2016. Flush, Gauss, and Reload – A Cache Attack on the BLISS Lattice-Based Signature Scheme](https://rd.springer.com/content/pdf/10.1007%2F978-3-662-53140-2_16.pdf)

###### [GK15]
[Ghosh Satrajit, Kate Aniket. 2015. Post-Quantum Forward-Secure Onion Routing: (Future Anonymity in Today’s Budget)](http://link.springer.com/10.1007/978-3-319-28166-7_13)

###### [GKS20]
[Gagliardoni Tommaso, Krämer Juliane, Struck Patrick. 2020. Quantum indistinguishability for public key encryption](https://arxiv.org/abs/2003.00578)

###### [GKT13]
[Gagliano Roque, Kent Stephen, Turner Sean. 2013. Algorithm Agility Procedure for the Resource Public Key Infrastructure (RPKI)](https://rfc-editor.org/rfc/rfc6916.txt)

###### [GLD+17]
[Gao Xinwei, Li Lin, Ding Jintai, Liu Jiqiang, Saraswathy R. V., Liu Zhe. 2017. Fast Discretized Gaussian Sampling and Post-quantum TLS Ciphersuite](http://link.springer.com/10.1007/978-3-319-72359-4_33)

###### [GM20]
[Gunsing AC, Mennink BJM. 2020. Collapseability of Tree Hashes](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-44223-1_28.pdf)

###### [GMP21]
[Grubbs Paul, Maram Varun, Paterson Kenneth G.. 2021. Anonymous, Robust Post-Quantum Public Key Encryption](https://eprint.iacr.org/2021/708)

###### [GMR20]
[Greuet A, Montoya S, Renault G. 2020. Speeding-up Ideal Lattice-Based Key Exchange Using a RSA/ECC Coprocessor](https://eprint.iacr.org/2020/1602.pdf)

###### [GMS19]
[Ghosh Santosh, Misoczki Rafael, Sastry Manoj. 2019. Lightweight Post-Quantum-Secure Digital Signature Approach for IoT Motes](https://eprint.iacr.org/2019/122)

###### [GO17]
[Guneysu Tim, Oder Tobias. 2017. Towards lightweight Identity-Based Encryption for the post-quantum-secure Internet of Things](http://ieeexplore.ieee.org/document/7918335/)

###### [GPB+17]
[Guillen Oscar M., Poppelmann Thomas, Bermudo Mera Jose M., Bongenaar Elena Fuentes, Sigl Georg, Sepulveda Johanna. 2017. Towards post-quantum security for IoT endpoints with NTRU](http://ieeexplore.ieee.org/document/7927079/)

###### [GRS+22]
[Gupta Daya Sagar, Ray Sangram, Singh Tajinder, Kumari Madhu. 2022. Post-quantum lightweight identity-based two-party authenticated key exchange protocol for Internet of Vehicles with probable security](https://www.sciencedirect.com/science/article/pii/S0140366421003686)

###### [GSE20]
[Gellersen Tim, Seker Okan, Eisenbarth Thomas. 2020. Differential Power Analysis of the Picnic Signature Scheme.](https://eprint.iacr.org/2020/267)

###### [GWH+20]
[Guo Tingting, Wang Peng, Hu Lei, Ye Dingfeng. 2020. Attack Beyond-Birthday-Bound MACs in Quantum Setting.](https://eprint.iacr.org/2020/1595)

###### [GdK21]
[Genêt Aymeric, de Guertechin Natacha Linard, Kalu Novak. 2021. Full key recovery side-channel attack against ephemeral SIKE on the Cortex-M4](https://eprint.iacr.org/2021/858.pdf)

###### [H15]
[Housley R.. 2015. Guidelines for Cryptographic Algorithm Agility and Selecting Mandatory-to-Implement Algorithms](https://www.rfc-editor.org/info/rfc7696)

###### [H17]
[Hecht Pedro. 2017. Post-Quantum Cryptography(PQC): Generalized ElGamal Cipher over GF(251\textasciicircum8)](http://arxiv.org/abs/1702.03587)

###### [H18]
[Hecht Pedro. 2018. PQC: Triple Decomposition Problem Applied To GL(d, Fp) - A Secure Framework For Canonical Non-Commutative Cryptography](http://arxiv.org/abs/1810.08983)

###### [H19]
[Heider Tobias. 2019. Towards a Verifiably SecureQuantum-Resistant Key Exchangein IKEv2](https://www.nm.ifi.lmu.de/pub/Diplomarbeiten/heid19/PDF-Version/heid19.pdf)

###### [H20]
[Hecht Pedro. 2020. Algebraic Extension Ring Framework for Non-Commutative Asymmetric Cryptography](https://arxiv.org/abs/2002.08343)

###### [H21]
[Hecht Pedro. 2021. PQC: R-Propping of Burmester-Desmedt Conference Key Distribution System](http://rgdoi.net/10.13140/RG.2.2.22638.43846)

###### [HBG+18]
[Huelsing Andreas, Butin Denis, Gazdag Stefan-Lukas, Rijneveld Joost, Mohaisen Aziz. 2018. XMSS: eXtended Merkle Signature Scheme](https://rfc-editor.org/rfc/rfc8391.txt)

###### [HHP+21]
[Hamburg Mike, Hermelink Julius, Primas Robert, Samardjiska Simona, Schamberger Thomas, Streit Silvan, Strieder Emanuele, van Vredendaal Christine. 2021. Chosen Ciphertext k-Trace Attacks on Masked CCA2 Secure Kyber](https://eprint.iacr.org/2021/956.pdf)

###### [HKK+21]
[Hashimoto Keitaro, Katsumata Shuichi, Kwiatkowski Kris, Prest Thomas. 2021. An Efficient and Generic Construction for Signal's Handshake (X3DH): Post-Quantum, State Leakage Secure, and Deniable](https://eprint.iacr.org/2021/616)

###### [HKL+21]
[Heinz Daniel, Kannwischer Matthias J., Land Georg, Schwabe Peter, Sprenkels Daan. 2021. First-Order Masked Kyber on ARM Cortex-M4](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/heinz-first-order-pqc2021.pdf)

###### [HKP21]
[Hutchinson Aaron, Karabina Koray, Pereira Geovandro. 2021. Memory Optimization Techniques for Computing Discrete Logarithms in Compressed SIKE.](https://eprint.iacr.org/2021/368)

###### [HKW21]
[Hagemeier Dr. Heike, Kousidis Dr. Stavros, Wunderer Dr. Thomas. 2021. Standardisierung von Post-Quanten-Kryptografie und Empfehlungen des BSI](https://www.secumedia-shop.net/p/deutschland-digital-sicher-30-jahre-bsi-tg-band-2021)

###### [HLL+21]
[Hemmert Dr. Tobias, Lochter Manfred, Loebenberger Daniel, Margraf Marian, Reinhardt Stephanie, Sigl Georg. 2021. Quantencomputerresistente Kryptografie: Aktuelle Aktivitäten und Fragestellungen](https://www.secumedia-shop.net/Deutschland-Digital-Sicher-30-Jahre-BSI)

###### [HLX21]
[He Pengzhou, Lee Chiou-Yng, Xie Jiafeng. 2021. Compact Coprocessor for KEM Saber: Novel Scalable Matrix Originated Processing](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/xie-compact-coprocessor-pqc2021.pdf)

###### [HNS+20]
[Hülsing Andreas, Ning Kai-Chun, Schwabe Peter, Weber Florian, Zimmermann Philip R.. 2020. Post-quantum WireGuard](https://eprint.iacr.org/2020/379)

###### [HOK+18]
[Howe James, Oder Tobias, Krausz Markus, Güneysu Tim. 2018. Standard Lattice-Based Key Encapsulation on Embedded Devices](https://tches.iacr.org/index.php/TCHES/article/view/7279)

###### [HPA21]
[Howe James, Prest Thomas, Apon Daniel. 2021. SoK: How (not) to Design and Implement Post-quantum Cryptography](https://link.springer.com/chapter/10.1007/978-3-030-75539-3_19)

###### [HPP21]
[Hermelink Julius, Pessl Peter, Pöppelmann Thomas. 2021. Fault-Enabled Chosen-Ciphertext Attacks on Kyber](https://link.springer.com/10.1007/978-3-030-92518-5_15)

###### [HPR+20]
[Howe James, Prest Thomas, Ricosset Thomas, Rossi Mélissa. 2020. Isochronous Gaussian Sampling: From Inception to Implementation.](https://eprint.iacr.org/2019/1411)

###### [HRS+17]
[Hülsing Andreas, Rijneveld Joost, Schanck John, Schwabe Peter. 2017. High-Speed Key Encapsulation from NTRU](https://eprint.iacr.org/2017/667)

###### [HZH+20]
[Huesmann Rolf, Zeier Alexander, Heinemann Andreas, Wiesmaier Alexander. 2020. Zur Benutzbarkeit und Verwendung von API-Dokumentationen](http://arxiv.org/abs/2007.04983)

###### [HvG13]
[Heyse Stefan, von Maurich Ingo, Güneysu Tim. 2013. Smaller Keys for Code-Based Cryptography: QC-MDPC McEliece Implementations on Embedded Devices](https://eprint.iacr.org/2015/425)

###### [J20]
[Jarecki Stanislaw. 2020. Topics in Cryptology – CT-RSA 2020: The Cryptographers’ Track at the RSA Conference 2020, San Francisco, CA, USA, February 24–28, 2020, Proceedings](http://link.springer.com/10.1007/978-3-030-40186-3)

###### [J21]
[Jonathan Bradbury Basil Hess. 2021. Fast Quantum-Safe Cryptography on IBM Z](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/hess-fast-quantum-safe-pqc2021.pdf)

###### [JAK+19]
[Jalali Amir, Azarderakhsh Reza, Kermani Mehran Mozaffari, Jao David. 2019. Supersingular Isogeny Diffie-Hellman Key Exchange on 64-Bit ARM](https://ieeexplore.ieee.org/abstract/document/7970184)

###### [JGH+20]
[Jiang Shaoquan, Gong Guang, He Jingnan, Nguyen Khoa, Wang Huaxiong. 2020. PAKEs: New Framework, New Techniques and More Efficient Lattice-Based Constructions in the Standard Model](https://eprint.iacr.org/2020/140)

###### [JKE+21]
[Julien Duman, Kathrin Hövelmanns, Eike Kiltz, Vadim Lyubashevsky, Gregor Seiler. 2021. Faster Kyber and Saber via a Generic Fujisaki-Okamoto Transform for Multi-User Security in the QROM](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/duman-faster-kyber-pqc2021.pdf)

###### [JS19]
[Jaques Samuel, Schanck John M.. 2019. Quantum Cryptanalysis in the RAM Model: Claw-Finding Attacks on SIKE](http://link.springer.com/10.1007/978-3-030-26948-7_2)

###### [KAA21]
[Karabulut Emre, Alkim Erdem, Aysu Aydin. 2021. Single-Trace Side-Channel Attacks on ω-Small Polynomial Sampling: With Applications to NTRU, NTRU Prime, and CRYSTALS-DILITHIUM](https://ieeexplore.ieee.org/document/9702284/)

###### [KAK18]
[Koziel Brian, Azarderakhsh Reza, Kermani Mehran Mozaffari. 2018. A High-Performance and Scalable Hardware Architecture for Isogeny-Based Cryptography](https://ieeexplore.ieee.org/document/8315051/)

###### [KAM+17]
[Koziel Brian, Azarderakhsh Reza, Mozaffari Kermani Mehran, Jao David. 2017. Post-Quantum Cryptography on FPGA Based on Isogenies on Elliptic Curves](https://ieeexplore.ieee.org/document/7725935/)

###### [KGB+18]
[Kannwischer Matthias J., Genêt Aymeric, Butin Denis, Krämer Juliane, Buchmann Johannes. 2018. Differential Power Analysis of XMSS and SPHINCS](http://link.springer.com/10.1007/978-3-319-89641-0_10)

###### [KGC+20]
[Kumar V. B. Y., Gupta N., Chattopadhyay A., Kasper M., Krauß C., Niederhagen R.. 2020. Post-Quantum Secure Boot](https://ieeexplore.ieee.org/document/9116252/)

###### [KGP+19]
[Kuznetsov A. A., Gorbenko Yu І., Prokopovych-Tkachenko D. I., Lutsenko М. S., Pastukhov M. V.. 2019. NIST PQC: CODE-BASED CRYPTOSYSTEMS](https://www.dl.begellhouse.com/journals/0632a9d54950b268,42c7cdd7728557a2,6b97dcff290ba987.html)

###### [KHR+16]
[Khalid A., Howe J., Rafferty C., O'Neill M.. 2016. Time-independent discrete Gaussian sampling for post-quantum cryptography](http://ieeexplore.ieee.org/document/7929543/)

###### [KJA+16]
[Koziel Brian, Jalali Amir, Azarderakhsh Reza, Jao David, Mozaffari-Kermani Mehran. 2016. NEON-SIDH: Efficient Implementation of Supersingular Isogeny Diffie-Hellman Key Exchange Protocol on ARM](https://link.springer.com/chapter/10.1007/978-3-319-48965-0_6)

###### [KK18]
[Kiefer Franziskus, Kwiatkowski Kris. 2018. Hybrid ECDHE-SIDH Key Exchange for TLS](https://datatracker.ietf.org/doc/html/draft-kiefer-tls-ecdhe-sidh-00)

###### [KKP20]
[Koteshwara S., Kumar M., Pattnaik P.. 2020. Performance Optimization of Lattice Post-Quantum Cryptographic Algorithms on Many-Core Processors](https://ieeexplore.ieee.org/document/9238630)

###### [KKS+20]
[Kuznetsov A., Kiian A., Smirnov O., Cherep A., Kanabekova M., Chepurko I.. 2020. Testing of Code-Based Pseudorandom Number Generators for Post-Quantum Application](https://ieeexplore.ieee.org/document/9125045)

###### [KL19]
[Krämer Juliane, Loiero Mirjam. 2019. Fault Attacks on UOV and Rainbow](http://link.springer.com/10.1007/978-3-030-16350-1_11)

###### [KL21]
[Kirshanova Elena, Laarhoven Thijs. 2021. Lower bounds on lattice sieving and information set decoding](https://eprint.iacr.org/2021/785)

###### [KLC+17]
[Kuo Po-Chun, Li Wen-Ding, Chen Yu-Wei, Hsu Yuan-Che, Peng Bo-Yuan, Cheng Chen-Mou, Yang Bo-Yin. 2017. High Performance Post-Quantum Key Exchange on FPGAs](https://eprint.iacr.org/2017/690)

###### [KLK+18]
[Kuznetsov Alexandr, Lutsenko Maria, Kiian Nastya, Makushenko Tymur, Kuznetsova Tetiana. 2018. Code-based key encapsulation mechanisms for post-quantum standardization](https://ieeexplore.ieee.org/document/8409144)

###### [KMR20]
[Kniep Quentin M., Müller Wolf, Redlich Jens-Peter. 2020. Post-Quantum Cryptography in WireGuard VPN](https://link.springer.com/10.1007/978-3-030-63095-9_16)

###### [KNW18]
[Kreutzer Michael, Niederhagen Ruben, Waidner Michael. 2018. Eberbacher Gespräch: Next Generation Crypto](https://www.sit.fraunhofer.de/de/eberbach-crypto/)

###### [KOV+18]
[Khalid Ayesha, Oder Tobias, Valencia Felipe, O' Neill Maire, Güneysu Tim, Regazzoni Francesco. 2018. Physical Protection of Lattice-Based Cryptography: Challenges and Solutions](https://dl.acm.org/doi/10.1145/3194554.3194616)

###### [KPD+18]
[Kampanakis Panos, Panburana Peter, Daw Ellie, Geest Daniel Van. 2018. The Viability of Post-quantum X.509 Certificates](http://eprint.iacr.org/2018/063)

###### [KPP20]
[Kannwischer Matthias J., Pessl Peter, Primas Robert. 2020. Single-Trace Attacks on Keccak](https://tches.iacr.org/index.php/TCHES/article/view/8590)

###### [KRS+19]
[Kannwischer Matthias J., Rijneveld Joost, Schwabe Peter, Stoffelen Ko. 2019. pqm4: Testing and Benchmarking NIST PQC on ARM Cortex-M4](https://eprint.iacr.org/2019/844)

###### [KS19]
[Kampanakis Panos, Sikeridis Dimitrios. 2019. Two PQ Signature Use-cases: Non-issues, challenges and potential solutions](https://eprint.iacr.org/2019/1276)

###### [KS20]
[Krämer Juliane, Struck Patrick. 2020. Encryption Schemes Using Random Oracles: From Classical to Post-Quantum Security.](https://eprint.iacr.org/2020/129)

###### [KSF+20]
[Kampanakis Panos, Steblia Douglas, Friedl Markus, Hansen Torben, Sikeridis Dimitrios. 2020. Post-quantum public key algorithms for the Secure Shell (SSH) protocol](https://datatracker.ietf.org/doc/html/draft-kampanakis-curdle-pq-ssh-00)

###### [KSL+19]
[Kwiatkowski Krzysztof, Sullivan Nick, Langley Adam, Levin Dave, Mislove Alan. 2019. Measuring TLS key exchange with post-quantum KEM](https://csrc.nist.gov/CSRC/media/Events/Second-PQC-Standardization-Conference/documents/accepted-papers/kwiatkowski-measuring-tls.pdf)

###### [KV19]
[Kwiatkowski Kris, Valenta Luke. 2019. The TLS Post-Quantum Experiment](https://blog.cloudflare.com/the-tls-post-quantum-experiment/)

###### [KZ20]
[Kales Daniel, Zaverucha Greg. 2020. Improving the Performance of the Picnic Signature Scheme](https://eprint.iacr.org/2020/427)

###### [L16]
[Langley Adam. 2016. ImperialViolet - CECPQ1 results](https://www.imperialviolet.org/2016/11/28/cecpq1.html)

###### [L18]
[Longa Patrick. 2018. A Note on Post-Quantum Authenticated Key Exchange from Supersingular Isogenies](https://eprint.iacr.org/2018/267)

###### [L19]
[Langley Adam. 2019. Real-world measurements of structured-lattices and supersingular isogenies in TLS](https://www.imperialviolet.org/2019/10/30/pqsivssl.html)

###### [L20]
[Lisovets Oleksiy. 2020. EM Side-channel Analysis on Smartphone Early Boot Encryption](https://www.seceng.ruhr-uni-bochum.de/media/attachments/files/2021/04/Master_thesis_Oleksiy_Lisovets.pdf)

###### [LCC+19]
[Li Chao-Yang, Chen Xiu-Bo, Chen Yu-Ling, Hou Yan-Yan, Li Jian. 2019. A New Lattice-Based Signature Scheme in Post-Quantum Blockchain Network](https://ieeexplore.ieee.org/document/8579537)

###### [LCG18]
[Liu Zhe, Choo Kim-Kwang Raymond, Grossschadl Johann. 2018. Securing Edge Devices in the Post-Quantum Internet of Things Using Lattice-Based Cryptography](http://ieeexplore.ieee.org/document/8291132/)

###### [LH20]
[LeGrow Jason, Hutchinson Aaron. 2020. An Analysis of Fault Attacks on CSIDH](https://eprint.iacr.org/2020/1006.pdf)

###### [LJd+21]
[Leonardo A. D. S. Ribeiro, José Paulo da Silva Lima, de Queiroz Ruy J. G. B. , Amirton Bezerra Chagas, Jonysberg P. Quintino, Fabio Q. B. da Silva, Andre L. M. Santos, José Roberto Junior Ribeiro. 2021. Saber Post-Quantum Key Encapsulation Mechanism (KEM): Evaluating Performance in Mobile Devices and Suggesting Some Improvements](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ribeiro-saber-pq-key-pqc2021.pdf)

###### [LL13]
[Lei Xinyu, Liao Xiaofeng. 2013. NTRU-KE: A Lattice-based Public Key Exchange Protocol](https://eprint.iacr.org/2013/718)

###### [LLJ20]
[Lohachab Ankur, Lohachab Anu, Jangra Ajay. 2020. A comprehensive survey of prominent cryptographic aspects for securing communication in post-quantum IoT networks](https://linkinghub.elsevier.com/retrieve/pii/S2542660520300159)

###### [LLK+06]
[Li Xiangdong, Leung Lin, Kwan , Zhang Xiaowen, Kahanda , Anshel . 2006. Post-Quantum Diffie-Hellman and Symmetric Key Exchange Protocols](https://ieeexplore.ieee.org/document/1652122)

###### [LLP+13]
[Lee Kyungroul, Lee Youngjun, Park Junyoung, Yim Kangbin, You Ilsun. 2013. Security Issues on the CNG Cryptography Library (Cryptography API: Next Generation)](http://ieeexplore.ieee.org/document/6603762/)

###### [LSH20]
[Lee Sangtae, Shin Youngjoo, Hur Junbeom. 2020. Return of version downgrade attack in the era of TLS 1.3](https://dl.acm.org/doi/10.1145/3386367.3431310)

###### [LW19]
[Li Zengpeng, Wang Ding. 2019. Achieving One-Round Password-based Authenticated Key Exchange over Lattices](https://ieeexplore.ieee.org/abstract/document/8826379)

###### [M02]
[Maseberg Jan Sönke. 2002. Fail-Safe-Konzept für Public-Key-Infrastrukturen](http://elib.tu-darmstadt.de/diss/000246)

###### [M15]
[Mosca Michele. 2015. Cybersecurity in an era with quantum computers: will we be ready?](https://ieeexplore.ieee.org/document/8490169)

###### [M18]
[Mundra Sunil. 2018. Enterprise Agility: Accelerating enterprise agility](https://learning.oreilly.com/library/view/-/9781788990646)

###### [M20]
[Ming Chen. 2020. A Composable Authentication Key Exchange Scheme with Post-Quantum Forward Secrecy](https://crad.ict.ac.cn/EN/10.7544/issn1000-1239.2020.20200472)

###### [M21]
[Mert Ahmet Can. 2021. A Unified Cryptoprocessor for Lattice-based Signature and Key-exchange](https://github.com/acmert/acmert.github.io/blob/8c9dd91b21c214044b9cbc7c2bc97705c19ac2f5/files/saber-dil-processor.pdf)

###### [M78]
[McEliece Robert J. 1978. A Public-Key Cryptosystem Based On Algebraic Coding Theory](https://tmo.jpl.nasa.gov/progress_report2/42-44/44N.PDF)

###### [MAA+20]
[Moody Dustin, Alagic Gorjan, Apon Daniel C, Cooper David A, Dang Quynh H, Kelsey John M, Liu Yi-Kai, Miller Carl A, Peralta Rene C, Perlner Ray A, Robinson Angela Y, Smith-Tone Daniel C, Alperin-Sheriff Jacob. 2020. Status report on the second round of the NIST post-quantum cryptography standardization process](https://nvlpubs.nist.gov/nistpubs/ir/2020/NIST.IR.8309.pdf)

###### [MAC+02]
[Moore S., Anderson R., Cunningham P., Mullins R., Taylor G.. 2002. Improving smart card security using self-timed circuits](https://www.cl.cam.ac.uk/~swm11/research/papers/async2002.pdf)

###### [MC20]
[Mouha Nicky, Celi Christopher. 2020. Extending NIST's CAVP Testing of Cryptographic Hash Function Implementations](https://www.springerprofessional.de/en/extending-nist-s-cavp-testing-of-cryptographic-hash-function-imp/17699818)

###### [MCD+21]
[Ma Chujiao, Colon Luis, Dera Joe, Rashidi Bahman, Garg Vaibhav. 2021. CARAF: Crypto Agility Risk Assessment Framework](https://academic.oup.com/cybersecurity/article/doi/10.1093/cybsec/tyab013/6289827)

###### [MCF19]
[McGrew David, Curcio Michael, Fluhrer Scott. 2019. Leighton-Micali Hash-Based Signatures](https://rfc-editor.org/rfc/rfc8554.txt)

###### [MCR18]
[Mentens Nele, Charbon Edoardo, Regazzoni Francesco. 2018. Rethinking Secure FPGAs: Towards a Cryptography-Friendly Configurable Cell Architecture and Its Automated Design Flow](https://ieeexplore.ieee.org/document/8457664/)

###### [MH19]
[Macaulay Tyson, Henderson Richard. 2019. Cryptographic Agility in practice: emerging use-cases](https://assets.website-files.com/5bd73d456f7b3f2db2bbbb95/5c76a740dcc2cc4646a06805_ISG_AgilityUseCases_Whitepaper-FINAL.pdf)

###### [MH21]
[Mashatan Atefeh, Heintzman Douglas. 2021. The Complex Path to Quantum Resistance: Is your organization prepared?](https://dl.acm.org/doi/10.1145/3466132.3466779)

###### [MK19]
[Marzougui Soundes, Krämer Juliane. 2019. Post-Quantum Cryptography in Embedded Systems](https://dl.acm.org/doi/10.1145/3339252.3341475)

###### [MKF+16]
[McGrew David, Kampanakis Panos, Fluhrer Scott, Gazdag Stefan-Lukas, Butin Denis, Buchmann Johannes. 2016. State Management for Hash-Based Signatures](https://eprint.iacr.org/2016/357)

###### [MO18]
[Mehrez Hassane Aissaoui, Othmane EL. 2018. The Crypto-Agility Properties](https://www.iiis.org/CDs2018/CD2018Summer/papers/HA536VG.pdf)

###### [MOG15]
[Maurich Ingo Von, Oder Tobias, Güneysu Tim. 2015. Implementing QC-MDPC McEliece Encryption](https://doi.org/10.1145/2700102)

###### [MPD+18]
[Malina Lukas, Popelova Lucie, Dzurenda Petr, Hajny Jan, Martinasek Zdenek. 2018. On Feasibility of Post-Quantum Cryptography on Small Devices](https://linkinghub.elsevier.com/retrieve/pii/S2405896318308474)

###### [MRD+20]
[Malina Lukas, Ricci Sara, Dzurenda Petr, Smekal David, Hajny Jan, Gerlich Tomas. 2020. Towards Practical Deployment of Post-quantum Cryptography on Constrained Platforms and Hardware-Accelerated Platforms](http://link.springer.com/10.1007/978-3-030-41025-4_8)

###### [MS18]
[Moustafa Nagy, Soukharev Vladimir. 2018. Crypto Agility Is a Must-Have for Data Encryption Standards](https://www.cigionline.org/articles/crypto-agility-must-have-data-encryption-standards/)

###### [MST21]
[Mattsson John Preuß, Smeets Ben, Thormarker Erik. 2021. Quantum-Resistant Cryptography](https://arxiv.org/abs/2112.00399)

###### [MVB+21]
[Michael Baentsch, Vlad Gheorghiu, Basil Hess, Christian Paquin, John Schanck, Douglas Stebila, Goutam Tamvada. 2021. Updates from the Open Quantum Safe Project](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/schanck-open-quantum-safe-project-pqc2021.pdf)

###### [MW18]
[Mindermann Kai, Wagner Stefan. 2018. Usability and Security Effects of Code Examples on Crypto APIs](https://ieeexplore.ieee.org/document/8514203/)

###### [MW20]
[Mindermann K., Wagner S.. 2020. Fluid Intelligence Doesn't Matter! Effects of Code Examples on the Usability of Crypto APIs](https://dl.acm.org/doi/10.1145/3377812.3390892)

###### [MZ17]
[Ma Fermi, Zhandry Mark. 2017. Encryptor Combiners: A Unified Approach to Multiparty NIKE, (H)IBE, and Broadcast Encryption](https://eprint.iacr.org/2017/152)

###### [Mdv+20]
[Müller Moritz, de Jong Jins, van Heesch Maran, Overeinder Benno, van Rijswijk-Deij Roland. 2020. Retrofitting post-quantum cryptography in internet protocols: a case study of DNSSEC](https://dl.acm.org/doi/10.1145/3431832.3431838)

###### [N11]
[Nelson David B.. 2011. Crypto-Agility Requirements for Remote Authentication Dial-In User Service (RADIUS)](https://rfc-editor.org/rfc/rfc6421.txt)

###### [N16]
[NIST . 2016. Submission Requirements and Evaluation Criteria  for the Post-Quantum Cryptography Standardization Process](https://csrc.nist.rip/groups/ST/post-quantum-crypto/documents/call-for-proposals-final-dec-2016.pdf)

###### [N21]
[Nguyen Phong Q.. 2021. Boosting the Hybrid Attack on NTRU: Torus LSH, Permuted HNF and Boxed Sphere](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/nguyen-boosting-hybridboost-pqc2021.pdf)

###### [NDR+17]
[Nejatollahi Hamid, Dutt Nikil, Ray Sandip, Regazzoni Francesco, Banerjee Indranil, Cammarota Rosario. 2017. Software and Hardware Implementation of Lattice-based Cryptography Schemes](https://www.researchgate.net/publication/320963262_Software_and_Hardware_Implementation_of_Lattice-based_Cryptography_Schemes)

###### [NDR+19]
[Nejatollahi Hamid, Dutt Nikil, Ray Sandip, Regazzoni Francesco, Banerjee Indranil, Cammarota Rosario. 2019. Post-Quantum Lattice-Based Cryptography Implementations: A Survey](https://doi.org/10.1145/3292548)

###### [NM17]
[National Academies of Sciences Engineering, Medicine . 2017. Cryptographic Agility and Interoperability: Proceedings of a Workshop](https://www.nap.edu/catalog/24636/cryptographic-agility-and-interoperability-proceedings-of-a-workshop)

###### [NSH+21]
[Nina Bindel, Sarah McCarthy, Hanif Rahbari, Geoff Twardokus. 2021. Suitability of 3rd Round Signature Candidates for Vehicle-to-Vehicle Communication –Extended Abstract](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/bindel-suitability-abstract-pqc2021.pdf)

###### [NW17]
[Niederhagen Ruben, Waidner Michael. 2017. Practical Post-Quantum Cryptography](https://www.sit.fraunhofer.de/fileadmin/dokumente/studien_und_technical_reports/Practical.PostQuantum.Cryptography_WP_FraunhoferSIT.pdf?_=1503992279)

###### [NWA+19]
[Noel M. D., Waziri O. V., Abdulhamid M. S., Ojeniyi A. J.. 2019. Stateful Hash-based Digital Signature Schemes for Bitcoin Cryptocurrency](https://ieeexplore.ieee.org/document/9043192)

###### [NWE19]
[Neish Andrew, Walter Todd, Enge Per. 2019. Quantum-resistant authentication algorithms for satellite-based augmentation systems](https://onlinelibrary.wiley.com/doi/abs/10.1002/navi.287)

###### [O20]
[O. Saarinen Markku-Juhani. 2020. Mobile Energy Requirements of the Upcoming NIST Post-Quantum Cryptography Standards](https://ieeexplore.ieee.org/document/9126751/)

###### [OHW+18]
[O’Neill Mark, Heidbrink Scott, Whitehead Jordan, Perdue Tanner, Dickinson Luke, Collett Torstein, Bonner Nick, Seamons Kent, Zappala Daniel. 2018. The Secure Socket API: TLS as an Operating System Service](https://www.usenix.org/conference/usenixsecurity18/presentation/oneill)

###### [OP20]
[Ounsworth Mike, Pala Massimiliano. 2020. Composite Keys and Signatures For Use In Internet PKI](https://datatracker.ietf.org/doc/html/draft-ounsworth-pq-composite-sigs-03)

###### [OPp19]
[Ott David, Peikert Christopher, participants other workshop. 2019. Identifying Research Challenges in Post Quantum Cryptography Migration and Cryptographic Agility](http://arxiv.org/abs/1909.07353)

###### [OSH+19]
[Oder Tobias, Speith Julian, Höltgen Kira, Güneysu Tim. 2019. Towards Practical Microcontroller Implementation of the Signature Scheme Falcon](http://link.springer.com/10.1007/978-3-030-25510-7_4)

###### [OSP+16]
[Oder Tobias, Schneider Tobias, Pöppelmann Thomas, Güneysu Tim. 2016. Practical CCA2-Secure and Masked Ring-LWE Implementation](https://eprint.iacr.org/2016/1109)

###### [P20]
[Perrin Trevor. 2020. KEM-based Hybrid Forward Secrecy for Noise](https://github.com/noiseprotocol/noise_hfs_spec/blob/025f0f60cb3b94ad75b68e3a4158b9aac234f8cb/output/noise_hfs.pdf)

###### [PCC+99]
[Paar Christof, Chetwynd Brendon Richard, Connor Thomas J., Deng Sheng Yung, Marchant Stephen J.. 1999. Algorithm-agile cryptographic coprocessor based on FPGAs](https://doi.org/10.1117/12.359537)

###### [PH16]
[Park Aesun, Han Dong-Guk. 2016. Chosen ciphertext Simple Power Analysis on software 8-bit implementation of ring-LWE encryption](https://ieeexplore.ieee.org/document/7835555)

###### [PM20]
[Pradel Gaëtan, Mitchell Chris J.. 2020. Post-quantum Certificates for Electronic Travel Documents](http://link.springer.com/10.1007/978-3-030-66504-3_4)

###### [PMS+21]
[Prasanna Ravi, Martianus Frederic Ezerman, Shivam Bhasin, Anupam Chattopadhyay, Sujoy Sinha Roy. 2021. On Generic Side-Channel Assisted Chosen Ciphertext Attacks on Lattice-based PKE/KEMs](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ravi-generic-side-channel-pqc2021.pdf)

###### [PMS19]
[Petrenko Kyrylo, Mashatan Atefeh, Shirazi Farid. 2019. Assessing the quantum-resistant cryptographic agility of routing and switching IT network infrastructure in a large-size financial organization](https://linkinghub.elsevier.com/retrieve/pii/S2214212618301212)

###### [PN19]
[Paul Sebastian, Niethammer Melanie. 2019. On the importance of cryptographic agility for industrial automation: Preparing industrial systems for the quantum computing era](https://www.degruyter.com/document/doi/10.1515/auto-2019-0019/html)

###### [PP21]
[Pessl Peter, Prokop Lukas. 2021. Fault Attacks on CCA-secure Lattice KEMs](https://tches.iacr.org/index.php/TCHES/article/view/8787)

###### [PRK+19]
[Pugh Sydney, Raunak M S, Kuhn D. Richard, Kacker Raghu. 2019. Systematic Testing of Post-Quantum Cryptographic Implementations Using Metamorphic Testing](https://ieeexplore.ieee.org/document/8785645/)

###### [PS13]
[Perlner Ray, Smith-Tone Daniel. 2013. A Classification of Differential Invariants for Multivariate Post-quantum Cryptosystems](http://link.springer.com/10.1007/978-3-642-38616-9_11)

###### [PS20]
[Paul Sebastian, Scheible Patrik. 2020. Towards Post-Quantum Security for Cyber-Physical Systems: Integrating PQC into Industrial M2M Communication](http://link.springer.com/10.1007/978-3-030-59013-0_15)

###### [PSK+18]
[Park Aesun, Shim Kyung-Ah, Koo Namhun, Han Dong-Guk. 2018. Side-Channel Attacks on Post-Quantum Signature Schemes based on Multivariate Quadratic Equations](https://tches.iacr.org/index.php/TCHES/article/view/7284)

###### [PST19]
[Paquin Christian, Stebila Douglas, Tamvada Goutam. 2019. Benchmarking Post-Quantum Cryptography in TLS](https://eprint.iacr.org/2019/1447)

###### [Q21]
[QuantiCor . 2021. Sicherheitsrisiko Quantencomputer](https://quanticor-security.de/whitepaper/)

###### [R21]
[Rognerud Robert. 2021. Post-Quantum Key Exchange in Telegram's MTProto Protocol](https://ntnuopen.ntnu.no/ntnu-xmlui/handle/11250/2781229)

###### [REB+21]
[Ravi Prasanna, Ezerman Martianus Frederic, Bhasin Shivam, Chattopadhyay Anupam, Roy Sujoy Sinha. 2021. On Generic Side-Channel Assisted Chosen Ciphertext Attacks on Lattice-based PKE/KEMs Towards key recovery attacks on NTRU-based PKE/KEMs](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ravi-generic-side-channel-pqc2021.pdf)

###### [RFS20]
[Roy Debapriya Basu, Fritzmann Tim, Sigl Georg. 2020. Efficient hardware/software co-design for post-quantum crypto algorithm SIKE on ARM and RISC-V based microcontrollers](https://dl.acm.org/doi/10.1145/3400302.3415728)

###### [RGR21]
[Raynal Mathilde, Genet Aymeric, Romailler Yolan. 2021. PQ-WireGuard: we did it again.](https://csrc.nist.gov/Presentations/2021/pq-wireguard-we-did-it-again)

###### [RHC+21]
[Ravi Prasanna, Howe James, Chattopadhyay Anupam, Bhasin Shivam. 2021. Lattice-Based Key Sharing Schemes: A Survey](https://dl.acm.org/doi/abs/10.1145/3422178)

###### [RKK21]
[Roth Johannes, Karatsiolis Evangelos, Krämer Juliane. 2021. Classic McEliece Implementation with Low Memory Footprint](https://link.springer.com/chapter/10.1007%2F978-3-030-68487-7_3)

###### [RM20]
[Raya Ali, Mariyappn K.. 2020. Diffie-Hellman Instantiations in Pre- and Post- Quantum World: A Review Paper](https://ieeexplore.ieee.org/document/9296172)

###### [RSC+20]
[Ravi Prasanna, Sinha Roy Sujoy, Chattopadhyay Anupam, Bhasin Shivam. 2020. Generic Side-channel attacks on CCA-secure lattice-based PKE and KEMs](https://tches.iacr.org/index.php/TCHES/article/view/8592)

###### [RTK+21]
[Richter Stefan, Thornton Matthew, Khan Imran, Scott Hamish, Jaksch Kevin, Vogl Ulrich, Stiller Birgit, Leuchs Gerd, Marquardt Christoph, Korolkova Natalia. 2021. Agile and versatile quantum communication: Signatures and secrets](https://journals.aps.org/prx/abstract/10.1103/PhysRevX.11.011038)

###### [S09]
[Sullivan Bryan. 2009. Cryptographic Agility](http://msdn.microsoft.com/en-us/magazine/ee321570.aspx)

###### [S15]
[Sushil Gerhard Chroust. 2015. Systemic Flexibility and Business Agility](https://link.springer.com/book/10.1007/978-81-322-2151-7)

###### [S17]
[Saarinen Markku-Juhani Olavi. 2017. Ring-LWE Ciphertext Compression and Error Correction: Tools for Lightweight Post-Quantum Cryptography](https://dl.acm.org/doi/10.1145/3055245.3055254)

###### [S18]
[Smith Benjamin. 2018. Pre- and Post-quantum Diffie–Hellman from Groups, Actions, and Isogenies \textbar SpringerLink](https://link.springer.com/chapter/10.1007/978-3-030-05153-2_1)

###### [S19]
[Steel Graham. 2019. Blog - Achieving 'Crypto Agility'](https://cryptosense.com/blog/achieving-crypto-agility)

###### [S20]
[Santoso Bagus. 2020. Generalization of Isomorphism of Polynomials with Two Secrets and Its Application to Public Key Encryption](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-44223-1_19.pdf)

###### [S21]
[Smyslov Valery. 2021. Alternative Approach for Mixing Preshared Keys in IKEv2 for Post-quantum Security](https://datatracker.ietf.org/doc/html/draft-smyslov-ipsecme-ikev2-qr-alt-04)

###### [S97]
[Shor Peter W.. 1997. Polynomial-Time Algorithms for Prime Factorization and Discrete Logarithms on a Quantum Computer](http://epubs.siam.org/doi/10.1137/S0097539795293172)

###### [SB19]
[Simion Emil, Burciu Paul. 2019. THE POWER OF NIST CRYPTOGRAPHIC STATISTICAL TESTS SUITE](https://www.researchgate.net/publication/333968632_THE_POWER_OF_NIST_CRYPTOGRAPHIC_STATISTICAL_TESTS_SUITE)

###### [SD18]
[Streit Silvan, De Santis Fabrizio. 2018. Post-Quantum Key Exchange on ARMv8-A: A New Hope for NEON Made Simple](https://ieeexplore.ieee.org/document/8107588/)

###### [SFG20]
[Stebila D., Fluhrer S., Gueron S.. 2020. Internet-Draft: Hybrid key exchange in TLS 1.3](https://tools.ietf.org/id/draft-stebila-tls-hybrid-design-03.html)

###### [SH19]
[Soukharev Vladimir, Hess Basil. 2019. PQDH: A Quantum-Safe Replacement for Diffie-Hellman based on SIDH](https://eprint.iacr.org/2019/730)

###### [SHK+20]
[Suhail Sabah, Hussain Rasheed, Khan Abid, Hong Choong Seon. 2020. On the Role of Hash-based Signatures in Quantum-Safe Internet of Things: Current Solutions and Future Directions](https://ieeexplore.ieee.org/document/9152977)

###### [SK20]
[Shahid Furqan, Khan Abid. 2020. Post-quantum distributed ledger for internet of things](https://www.researchgate.net/publication/339551882_Post-quantum_distributed_ledger_for_internet_of_things)

###### [SKD20]
[Sikeridis Dimitrios, Kampanakis Panos, Devetsikiotis Michael. 2020. Assessing the Overhead of Post-Quantum Cryptography in TLS 1.3 and SSH](https://www.researchgate.net/profile/Dimitrios-Sikeridis/publication/346646724_Assessing_the_Overhead_of_Post-Quantum_Cryptography_in_TLS_13_and_SSH/links/5ff3915992851c13feeb38fc/Assessing-the-Overhead-of-Post-Quantum-Cryptography-in-TLS-13-and-SSH.pdf)

###### [SM16]
[Stebila Douglas, Mosca Michele. 2016. Post-quantum Key Exchange for the Internet and the Open Quantum Safe Project](https://github.com/open-quantum-safe/liboqs)

###### [SMK+18]
[Spreitzer Raphael, Moonsamy Veelasha, Korak Thomas, Mangard Stefan. 2018. Systematic Classification of Side-Channel Attacks: A Case Study for Mobile Devices](https://repository.ubn.ru.nl/bitstream/2066/187230/1/187230.pdf)

###### [SN21]
[Suzuki Keita, Nuida Koji. 2021. An Improvement of a Key Exchange Protocol Relying on Polynomial Maps](http://arxiv.org/abs/2107.05924)

###### [SNA+21]
[Seyhan Kübra, Nguyen Tu N., Akleylek Sedat, Cengiz Korhan. 2021. Lattice-based cryptosystems for the security of resource-constrained IoT devices in post-quantum world: a survey](https://link.springer.com/10.1007/s10586-021-03380-7)

###### [SS17]
[Schanck John M., Stebila Douglas. 2017. A Transport Layer Security (TLS) Extension For Establishing An Additional Shared Secret](https://datatracker.ietf.org/doc/html/draft-schanck-tls-additional-keyshare-00)

###### [SSK+21]
[Stadler Sara, Sakaguti Vitor, Kaur Harjot, Fehlhaber Anna Lena. 2021. Hybrid Signal protocol for post-quantum email encryption](https://eprint.iacr.org/2021/875)

###### [SSP+19]
[Samardjiska Simona, Santini Paolo, Persichetti Edoardo, Banegas Gustavo. 2019. A Reaction Attack Against Cryptosystems Based on LRPC Codes](https://link.springer.com/chapter/10.1007/978-3-030-30530-7_10)

###### [SSW20]
[Schwabe Peter, Stebila Douglas, Wiggers Thom. 2020. Post-quantum TLS without handshake signatures](https://eprint.iacr.org/2020/534)

###### [SV20]
[Smith-Tone Daniel, Verbel Javier A. 2020. A Rank Attack Against Extension Field Cancellation.](https://link.springer.com/content/pdf/10.1007%252F978-3-030-44223-1_21.pdf)

###### [SWW14]
[Sun Chang-ai, Wang Zuoyi, Wang Guan. 2014. A property-based testing framework for encryption programs](https://doi.org/10.1007/s11704-014-3040-y)

###### [SWZ16]
[Schanck John M., Whyte William, Zhang Zhenfei. 2016. Circuit-extension handshakes for Tor achieving forward secrecy in a quantum world](https://www.sciendo.com/article/10.1515/popets-2016-0037)

###### [SXZ+19]
[Shen Ruping, Xiang Hong, Zhang Xin, Cai Bin, Xiang Tao. 2019. Application and Implementation of Multivariate Public Key Cryptosystem in Blockchain (Short Paper)](https://www.researchgate.net/publication/335225783_Application_and_Implementation_of_Multivariate_Public_Key_Cryptosystem_in_Blockchain_Short_Paper)

###### [So20]
[Smith-Tone Daniel, others . 2020. Practical Cryptanalysis of k-ary C*.](https://eprint.iacr.org/2019/841)

###### [T20]
[Thornton Matthew. 2020. Agile quantum cryptography and non-classical state generation: Agile quantum cryptography and non-classical state generation](http://hdl.handle.net/10023/21361)

###### [TAM+21]
[Thomas Espitau, Akira Takahashi, Mehdi Tibouchi, Alexandre Wallet. 2021. Mitaka: A Simpler, Parallelizable, Maskable Variant of Falcon](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/espitau-mitaka-pqc2021.pdf)

###### [TDE+21]
[Tasso Élise, De Feo Luca, El Mrabet Nadia, Pontié Simon. 2021. Resistance of Isogeny-Based Cryptographic Implementations to a Fault Attack](https://hal-cea.archives-ouvertes.fr/cea-03266892)

###### [THP+98]
[Tarman T. D., Hutchinson R. L., Pierson L. G., Sholander P. E., Wirzke E. L.. 1998. Algorithm-agile encryption in ATM networks](https://ieeexplore.ieee.org/document/708451)

###### [TLW19]
[Tian Jing, Lin Jun, Wang Zhongfeng. 2019. Ultra-Fast Modular Multiplication Implementation for Isogeny-Based Post-Quantum Cryptography](https://ieeexplore.ieee.org/document/9020384/)

###### [TMT+21]
[Tendayi Kamucheka, Michael Fahr, Tristen Teague, Alexander Nelson, David Andrews, Miaoqing Huang. 2021. Power-based Side Channel Attack Analysis on PQC Algorithms](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/kamucheka-power-based-pqc2021.pdf)

###### [TPD21]
[Tao Chengdong, Petzoldt Albrecht, Ding Jintai. 2021. Efficient Key Recovery for all HFE Signature Variants](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-84242-0_4.pdf)

###### [TRH+20]
[Tujner Zsolt, Rooijakkers Thomas, Heesch Maran, Önen Melek. 2020. QSOR: Quantum-safe Onion Routing](https://arxiv.org/pdf/2001.03418.pdf)

###### [TSS+18]
[Torres Wilson Alberto, Steinfeld Ron, Sakzad Amin, Liu Joseph K., Kuchta Veronika, Bhattacharjee Nandita, Au Man Ho, Cheng Jacob. 2018. Post-Quantum One-Time Linkable Ring Signature and Application to Ring Confidential Transactions in Blockchain (Lattice RingCT v1.0)](https://eprint.iacr.org/2018/379)

###### [TSZ19]
[Tan Teik, Szalachowski Pawel, Zhou Jianying. 2019. SoK: Challenges of Post-Quantum Digital Signing in Real-world Applications](https://eprint.iacr.org/2019/1374)

###### [TTg+19]
[Tjhai C., Tomlinson M., grbartle@cisco.com , Fluhrer Scott, Geest Daniel Van, Garcia-Morchon Oscar, Smyslov Valery. 2019. Framework to Integrate Post-quantum Key Exchanges into Internet Key Exchange Protocol Version 2 (IKEv2)](https://datatracker.ietf.org/doc/html/draft-tjhai-ipsecme-hybrid-qske-ikev2-04)

###### [TWZ20]
[Tao Yang, Wang Xi, Zhang Rui. 2020. Short Zero-Knowledge Proof of Knowledge for Lattice-Based Commitment.](https://rd.springer.com/chapter/10.1007/978-3-030-44223-1_15)

###### [U17]
[Ustimenko Vasyl. 2017. On Desynchronised Multivariate El Gamal Algorithm](https://eprint.iacr.org/2017/712)

###### [U18]
[Utimaco . 2018. post-quantum crypto agility](https://hsm.utimaco.com/solutions/applications/post-quantum-crypto-agility/)

###### [U20]
[Unruh Dominique. 2020. Post-Quantum Verification of Fujisaki-Okamoto](https://eprint.iacr.org/2020/962.pdf)

###### [UWK15]
[Ullmann Markus, Wieschebrink Christian, Kugler Dennis. 2015. Public Key Infrastructure and Crypto Agility Concept for Intelligent Transportation Systems](https://www.semanticscholar.org/paper/Public-Key-Infrastructure-and-Crypto-Agility-for-Ullmann-Wieschebrink/f644a11328e8f4e4243afd2b63a3fdb65012f468)

###### [UXT+22]
[Ueno Rei, Xagawa Keita, Tanaka Yutaro, Ito Akira, Takahashi Junko, Homma Naofumi. 2022. Curse of Re-encryption: A Generic Power/EM Analysis on Post-Quantum KEMs](https://tches.iacr.org/index.php/TCHES/article/view/9298)

###### [V21]
[Vassilev Apostol. 2021. Automation of the Cryptographic Module Validation Program (CMVP)](https://csrc.nist.gov/publications/detail/white-paper/2021/04/12/automation-of-the-cryptographic-module-validation-program-cmvp/draft)

###### [VF21]
[Vogt Sebastian, Funke Holger. 2021. How Quantum Computers threat security of PKIs and thus eIDs](http://dl.gi.de/handle/20.500.12116/36504)

###### [VM12]
[Vasic Valter, Mikuc Miljenko. 2012. Security Agility Solution Independent of the Underlaying Protocol Architecture.](https://www.semanticscholar.org/paper/Security-Agility-Solution-Independent-of-the-Vasic-Mikuc/489054a1f28eb26b1baa1a9f0caff2306c821695)

###### [VMV16]
[Vasić Valter, Mikuc Miljenko, Vuković Marin. 2016. Lightweight and adaptable solution for security agility.](http://itiis.org/journals/tiis/digital-library/manuscript/file/21049/TIISVol10No3-15.pdf)

###### [W20]
[Weller D L. 2020. Incorporating post-quantum cryptography in a microservice environment](https://rp.os3.nl/2019-2020/p13/report.pdf)

###### [WAG21]
[Wiesmaier Alexander, Alnahawi Nouri, Grasmeyer Tobias. 2021. On PQC Migration and Crypto-Agility](https://arxiv.org/abs/2106.09599)

###### [WGR22]
[Weger Violetta, Gassner Niklas, Rosenthal Joachim. 2022. A Survey on Code-Based Cryptography](http://arxiv.org/abs/2201.07119)

###### [WNG+18]
[Waidner Michael, Niederhagen Ruben, Grötker Thorsten, Reinelt Patrick. 2018. Post-Quantum Crypto for dummies](https://www.utimaco.com/de/post-quantum-crypto-dummies)

###### [WS20]
[Wang Wen, Stöttinger Marc. 2020. Post-Quantum Secure Architectures for Automotive Hardware Secure Modules](https://eprint.iacr.org/2020/026)

###### [WZF+17]
[Whyte W., Zhang Z., Fluhrer S., Garcia-Morchon O.. 2017. Internet-Draft: Quantum-Safe Hybrid (QSH) Key Exchange for Transport Layer Security (TLS) version 1.3](https://tools.ietf.org/html/draft-whyte-qsh-tls13-06)

###### [X21]
[Xagawa Keita. 2021. NTRU leads to Anonymous, Robust Public-Key Encryption](https://eprint.iacr.org/2021/741.pdf)

###### [XHY+20]
[Xin Guozhu, Han Jun, Yin Tianyu, Zhou Yuchao, Yang Jianwei, Cheng Xu, Zeng Xiaoyang. 2020. VPQC: A Domain-Specific Vector Processor for Post-Quantum Cryptography Based on RISC-V Architecture](https://ieeexplore.ieee.org/document/9061149/)

###### [XL20]
[Xing Yufei, Li Shuguo. 2020. An Efficient Implementation of the NewHope Key Exchange on FPGAs](https://ieeexplore.ieee.org/document/8930047/)

###### [XL21]
[Xing Yufei, Li Shuguo. 2021. A Compact Hardware Implementation of CCA-Secure Key Exchange Mechanism CRYSTALS-KYBER on FPGA](https://tches.iacr.org/index.php/TCHES/article/view/8797)

###### [YWL+18]
[Yin Wei, Wen Qiaoyan, Li Wenmin, Zhang Hua, Jin Zhengping. 2018. An Anti-Quantum Transaction Authentication Approach in Blockchain](https://ieeexplore.ieee.org/document/8242360)

###### [YWT20]
[Yasuda Takanori, Wang Yacheng, Takagi Tsuyoshi. 2020. Multivariate Encryption Schemes Based on Polynomial Equations over Real Numbers](https://link.springer.com/chapter/10.1007/978-3-030-44223-1_22)

###### [YXF+18]
[Yuan Ye, Xiao Junting, Fukushima Kazuhide, Kiyomoto Shinsaku, Takagi Tsuyoshi. 2018. Portable Implementation of Postquantum Encryption Schemes and Key Exchange Protocols on JavaScript-Enabled Platforms](https://www.hindawi.com/journals/scn/2018/9846168/)

###### [Z15]
[Zimmer Ephraim. 2015. Post-Quantum Kryptographie für IPsec](https://svs.informatik.uni-hamburg.de/publications/2015/2015-02-24-Zimmer-DFN-PQC-fuer-IPsec.pdf)

###### [Z20]
[Zaverucha Greg. 2020. The Picnic Signature Algorithm Specification](https://github.com/microsoft/Picnic/blob/master/spec/spec-v3.0.pdf)

###### [ZGF20]
[Zoni Davide, Galimberti Andrea, Fornaciari William. 2020. Efficient and Scalable FPGA-Oriented Design of QC-LDPC Bit-Flipping Decoders for Post-Quantum Cryptography](https://ieeexplore.ieee.org/document/9180360)

###### [ZLC+20]
[Zhang Cong, Liu Zilong, Chen Yuyang, Lu Jiahao, Liu Dongsheng. 2020. A Flexible and Generic Gaussian Sampler With Power Side-Channel Countermeasures for Quantum-Secure Internet of Things](https://ieeexplore.ieee.org/document/9037365)

###### [ZMR+21]
[Zhang Lei, Miranskyy Andriy, Rjaibi Walid, Stager Greg, Gray Michael, Peck John. 2021. Making Existing Software Quantum Safe: Lessons Learned](http://arxiv.org/abs/2110.08661)

###### [ZMR21]
[Zhang Lei, Miranskyy Andriy, Rjaibi Walid. 2021. Quantum Advantage and the Y2K Bug: A Comparison](https://arxiv.org/pdf/1907.10454.pdf)

###### [ZSS20]
[Zhao Raymond K, Steinfeld Ron, Sakzad Amin. 2020. COSAC: COmpact and Scalable Arbitrary-Centered Discrete Gaussian Sampling over Integers.](https://eprint.iacr.org/2019/1011)

###### [ZTJ+19]
[Zichen Li, Ting Xie, Juanmei Zhang, Ronghua Xu. 2019. Post Quantum Authenticated Key Exchange Protocol Based on Ring Learning with Errors Problem](https://crad.ict.ac.cn/EN/10.7544/issn1000-1239.2019.20180874)

###### [ZWH19]
[Zeier Alexander, Wiesmaier Alexander, Heinemann Andreas. 2019. API Usability of Stateful Signature Schemes](https://www.springerprofessional.de/en/api-usability-of-stateful-signature-schemes/17043992)

###### [ZWH21]
[Zeier Alexander, Wiesmaier Alexander, Heinemann Andreas. 2021. Zur Integration von Post-Quantum Verfahren in bestehende Softwareprodukte](http://arxiv.org/abs/2102.00157)

###### [ZWW+21]
[Zhang Peijun, Wang Lianhai, Wang Wei, Fu Kunlun, Wang Jinpeng. 2021. A Blockchain System Based on Quantum-Resistant Digital Signature](https://pdfs.semanticscholar.org/9247/3810d54de83c13713487005d706374d3f600.pdf)

###### [ZYD+20]
[Zhang Fan, Yang Bolin, Dong Xiaofei, Guilley Sylvain, Liu Zhe, He Wei, Zhang Fangguo, Ren Kui. 2020. Side-Channel Analysis and Countermeasure Design on ARM-based Quantum-Resistant SIKE](https://ieeexplore.ieee.org/document/9181442)

###### [ZZD+14]
[Zhang Jiang, Zhang Zhenfang, Ding Jintai, Snook Michael, Dagdelen Ozgur. 2014. Post-quantum Authenticated Key Exchange from Ideal Lattices](https://eprint.iacr.org/2014/589.pdf)

###### [ZZT+18]
[Zhang Huang, Zhang Fangguo, Tian Haibo, Au Man Ho. 2018. Anonymous Post-Quantum Cryptocash](http://link.springer.com/10.1007/978-3-662-58387-6_25)

###### [d16]
[de Vries Simon. 2016. Achieving 128-bit Security against Quantum Attacks in OpenVPN](https://essay.utwente.nl/70677/1/2016-08-09%20MSc%20Thesis%20Simon%20de%20Vries%20final%20color.pdf)

###### [d21]
[der Informationstechnik (BSI) Bundesamt für Sicherheit in. 2021. Kryptografie quantensicher gestalten](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Broschueren/Kryptografie-quantensicher-gestalten.pdf?__blob=publicationFile&v=4)

###### [dPT22]
[de Guertechin Natacha Linard, Pontié Simon, Tasso Élise. 2022. Zero-Value Side-Channel Attacks on SIKE](https://eprint.iacr.org/2022/054.pdf)

###### [dRd+21]
[da Silva Lima José Paulo, Ribeiro Leonardo A. D. S., de Queiroz Ruy J. G. B., Quintino Jonysberg P., da Silva Fabio Q. B., Santos Andre L. M., José Roberto. 2021. Evaluating Kyber post-quantum KEM in a mobile application](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ribeiro-evaluating-kyber-pqc2021.pdf)

###### [vG14]
[von Maurich Ingo, Güneysu Tim. 2014. Towards Side-Channel Resistant Implementations of QC-MDPC McEliece Encryption on Constrained Devices](https://www.researchgate.net/publication/285235340_Towards_Side-Channel_Resistant_Implementations_of_QC-MDPC_McEliece_Encryption_on_Constrained_Devices)

###### [vWM11]
[van Woudenberg Jasper G.J., Witteman Marc F., Menarini Federico. 2011. Practical Optical Fault Injection on Secure Microcontrollers](http://ieeexplore.ieee.org/document/6076471/)

###### [vvA+19]
[van Heesch Maran, van Adrichem Niels, Attema Thomas, Veugen Thijs. 2019. Towards Quantum-Safe VPNs and Internet](https://eprint.iacr.org/2019/1277)

###### [ØSV21]
[Øygarden Morten, Smith-Tone Daniel, Verbel Javier. 2021. On the Effect of Projection on Rank Attacks in Multivariate Cryptography](https://eprint.iacr.org/2021/655)

