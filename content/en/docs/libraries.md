---
title: "Libraries and Interfaces"
linktitle: "Libraries and Interfaces"
date: 2021-05-06T00:13:06+02:00
draft: false
type: docs
weight: 11
menu:
  main:
    weight: 4
---
- [liboqs](https://github.com/open-quantum-safe/liboqs):
An open source C library for quantum-safe cryptographic algorithms.

- [PQClean](https://github.com/PQClean/PQClean):
Portable C implementations of post-quantum cryptography.

- [The Legion of the Bouncy Castle](https://www.bouncycastle.org/specifications.html):
Java implementation of cryptographic algorithms containing a light-weight API suitable for use in any environment.

- [PQCCRYPTO](https://pqcrypto.eu.org/) A portfolio of high-security post-quantum public-key systems, providing efficient implementations of high-security post-quantum cryptography for a broad spectrum of real-world applications.

- [NaCL (Salt)](https://nacl.cr.yp.to/):
Software library for network communication, encryption, decryption, signatures, etc.

- [Libsodium](https://libsodium.gitbook.io/doc/):
Portable, cross-compilable, installable, packageable fork of NaCl, with a compatible API software library for encryption, decryption, signatures, password hashing etc.

- [LibHydrogen](https://github.com/jedisct1/libhydrogen):
Lightweight crypto library for constrained environments.

- [WASI Cryptography APIs](https://github.com/WebAssembly/wasi-crypto):
Development of cryptography API proposals for the WASI Subgroup of the [WebAssembly Community Group](https://www.w3.org/community/webassembly/)

- [Botan: Crypto and TLS for Modern C++](https://botan.randombit.net/) A  C++ cryptographic library implementing a range of practical systems, such as TLS protocol, X.509 certificates, modern AEAD ciphers, PKCS#11 and TPM hardware support, password hashing, and post quantum crypto schemes. Several other language bindings are available, including Python. Versions of Botan that are approved by the BSI can be found on the [Github repository](https://github.com/Rohde-Schwarz/botan)

- [eUCRITE API](https://cspub.h-da.io/eucrite/):
(Usable Cryptographic Interface) is a crypto API that places special emphasis on usability and security. Developed by the Applied Cyber-Security and User-Centered Security research groups at Darmstadt University of Applied Sciences.
