---
title: "Standards & Drafts"
linktitle: "Standards & Drafts"
date: 2021-05-06T00:11:20+02:00
draft: false
type: docs
weight: 3
---
- [A Transport Layer Security (TLS) Extension For Establishing An Additional Shared Secret](https://datatracker.ietf.org/doc/html/draft-schanck-tls-additional-keyshare-00): TLS extension to establish an additional shared secret using a second key exchange algorithm [[SS17]](../../refs#ss17)
- [Alternative Approach for Mixing Preshared Keys in IKEv2 for Post-quantum Security](https://datatracker.ietf.org/doc/html/draft-smyslov-ipsecme-ikev2-qr-alt-04): Extending IKEv2 so that the shared secret exchanged between peers has resistance against quantum computer attacks including the initial IKEv2 SA [[S21]](../../refs#s21)
- [Composite Keys and Signatures For Use In Internet PKI](https://datatracker.ietf.org/doc/html/draft-ounsworth-pq-composite-sigs-03): This document defines the structures CompositePublicKey, CompositeSignatureValue, and CompositeParams, which are sequences of the respective structure for each component algorithm. [[OP20]](../../refs#op20)
- [Framework to Integrate Post-quantum Key Exchanges into Internet Key Exchange Protocol Version 2 (IKEv2)](https://datatracker.ietf.org/doc/html/draft-tjhai-ipsecme-hybrid-qske-ikev2-04): Extending IKEv2 so that the shared secret exchanged between peers has resistance against quantum computer attacks. [[TTg19]](../../refs#ttg19)
- [Hybrid ECDHE-SIDH Key Exchange for TLS](https://datatracker.ietf.org/doc/html/draft-kiefer-tls-ecdhe-sidh-00): TLS key exchange combining Supersingular elliptic curve isogenie diffie-hellman (SIDH), with elliptic curve Diffie-Hellman (ECDHE) key exchange [[KK18]](../../refs#kk18)
- [Intermediate Exchange in the IKEv2 Protocol](https://datatracker.ietf.org/doc/html/draft-ietf-ipsecme-ikev2-intermediate-06): This documents defines a new exchange, called Intermediate Exchange, for the Internet Key Exchange protocol Version 2 (IKEv2) which helps to avoid IP fragmentation of large IKE messages. [[S21]](../../refs#s21)
- [Internet-Draft: Hybrid Post-Quantum Key Encapsulation Methods (PQ KEM) for Transport Layer Security 1.2 (TLS)](https://datatracker.ietf.org/doc/html/draft-campagna-tls-bike-sike-hybrid-03): New hybrid key exchange schemes for the Transport Layer Security 1.2 (TLS) protocol. [[CC20]](../../refs#cc20)
- [Internet-Draft: Hybrid key exchange in TLS 1.3](https://tools.ietf.org/id/draft-stebila-tls-hybrid-design-03.html): Providing construction for hybrid key exchange in the Transport Layer Security (TLS) protocol version 1.3. [[SFG20]](../../refs#sfg20)
- [Internet-Draft: Quantum-Safe Hybrid (QSH) Key Exchange for Transport Layer Security (TLS) version 1.3](https://tools.ietf.org/html/draft-whyte-qsh-tls13-06): Mechanism for providing modular design for quantum-safe cryptography in the handshake for TLS protocol version 1.3 [[WZF17]](../../refs#wzf17)
- [Mixing Preshared Keys in the Internet Key Exchange Protocol Version 2 (IKEv2) for Post-quantum Security](https://rfc-editor.org/rfc/rfc8784.txt): Extension of IKEv2 to allow it to be resistant to a quantum computer by using pre-shared keys [[FKM20]](../../refs#fkm20)
- [Post-quantum public key algorithms for the Secure Shell (SSH) protocol](https://datatracker.ietf.org/doc/html/draft-kampanakis-curdle-pq-ssh-00): Hybrid key exchange in the SSH Transport Layer Protocol using ECDH and PQC signature schemes [[KSF20]](../../refs#ksf20)
- [Postquantum Preshared Keys for IKEv2](https://datatracker.ietf.org/doc/html/draft-fluhrer-qr-ikev2-03): This document describes an extension of IKEv2 to allow it to be resistant to a Quantum Computer, by using preshared keys. [[FMK16]](../../refs#fmk16)
- [Quantum-Safe Hybrid (QSH) Ciphersuite for Transport Layer Security (TLS) version 1.2](https://datatracker.ietf.org/doc/html/draft-whyte-qsh-tls12-02): New cipher suite providing modular design for quantum-safe cryptography in the handshake for TLS protocol version 1.2 [[SWZ16]](../../refs#swz16)
