---
title: "PQC算法"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 1
layout: no_index
---
[NIST PQC标准化进程](https://www.nist.gov/pqcrypto)展示出了PQC的研究现状
- [Submission Requirements and Evaluation Criteria  for the Post-Quantum Cryptography Standardization Process](https://csrc.nist.rip/groups/ST/post-quantum-crypto/documents/call-for-proposals-final-dec-2016.pdf).
- [第一轮报告](https://nvlpubs.nist.gov/nistpubs/ir/2019/NIST.IR.8240.pdf) [[AASA+19]](../../refs#aasa19).
- [第二轮报告](https://nvlpubs.nist.gov/nistpubs/ir/2020/NIST.IR.8309.pdf) [[MAA+20]](../../refs#maa20).

#### **NIST PQC候选算法**

| 算法                           | 描述 | 类型 | 进入NIST第几轮 |
|-------------------------------------|-------------|------|------------|
| [BIKE]({{< ref "/bike" >}} "BIKE") | 基于准循环低密度奇偶校验法的比特翻转密钥封装算法 | 公钥加密和密钥封装 | 第三轮替补算法 |
| [CRYSTALS-Dilithium]({{< ref "/dilithium" >}} "CRYSTALS-Dilithium") | 基于模格上最短向量问题困难性的数字签名算法 | 数字签名 | 第三轮最终候选算法 |
| [Falcon]({{< ref "/falcon" >}} "Falcon") | 基于NTRU格上小整数解问题（SIS）的签名机制 | 数字签名 | 第三轮最终候选算法 |
| [FrodoKEM]({{< ref "/frodo" >}} "FrodoKEM")| 基于一般格的密钥封装机制 | 公钥加密和密钥封装 | 第三轮替补算法 |
| [GeMSS]({{< ref "/gemss" >}} "GeMSS") | 基于多变量签名，生成的签名长度小 | 数字签名 | 第三轮替补算法 |
| [HQC]({{< ref "/hqc" >}} "HQC") | 基于汉明准循环编码的公钥加密机制 | 公钥加密和密钥封装 | 第三轮替补算法 |
| [CRYSTALS-Kyber]({{< ref "/kyber" >}} "KYBER")| 基于模格上容错学习（LWE）问题的IND-CCA2安全的密钥封装机制 | 公钥加密和密钥封装 | 第三轮最终候选算法 |
| [Classic McEliece]({{< ref "/mceliece" >}} "Classic McEliece") | 基于编码的公钥加密机制，以随机二元Goppa编码的线性解码问题为基础 | 公钥加密和密钥封装 | 第三轮最终候选算法 |
| [NTRU]({{< ref "/ntru" >}} "NTRU") | 基于格最短向量问题的公钥加密机制 | 公钥加密和密钥封装 | 第三轮最终候选算法 |
| [NTRU-Prime]({{< ref "/prime" >}} "NTRU-Prime")  | 基于格的小体量KEM算法 | 公钥加密和密钥封装 | 第三轮替补算法 |
| [Picnic]({{< ref "/picnic" >}} "Picnic") | 基于零知识证明和对称加密原语的数字签名算法 | 数字签名 | 第三轮替补算法 |
| [Rainbow]({{< ref "/rainbow" >}} "Rainbow")| 一种公钥加密机制，基于解一系列随机多变量二次方程的困难性 | 数字签名 | 第三轮最终候选算法 |
| [SABER]({{< ref "/saber" >}} "SABER") | 基于MLWR问题的IND-CCA2安全的密钥封装机制 | 公钥加密和密钥封装 | 第三轮最终候选算法 |
| [SIKE]({{< ref "/sike" >}} "SIKE")| 基于同源的密钥封装机制，以超奇异同源图上的伪随机游走为基础 | 公钥加密和密钥封装 | 第三轮替补算法 |
| [SPHINCS+]({{< ref "/sphincs" >}} "SPHINCS+") | 基于哈希的无状态签名机制 | 数字签名 | 第三轮替补算法 |
