---
title: "SPHINCS+"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 15
---
[SPHINCS+](https://sphincs.org/) 是一种基于哈希的无状态签名机制。[第三轮提交](https://sphincs.org/data/sphincs+-round3-specification.pdf)
