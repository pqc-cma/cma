---
title: "SABER"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 13
---
[SABER](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/) 是一种基于MLWR问题的密钥封装机制。[Specs.](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/files/saberspecround3.pdf)

- 为确定性所必需的参数：
  - $l$：正整数

- 非必需参数：
  - $n$：正整数 (一般是 $256$)
  - $q$：正整数 (一般是 $2^{13}$)
  - $p$：正整数 (一般是 $2^{10}$)
  - $T$：正整数
  - $\mu$：正整数
