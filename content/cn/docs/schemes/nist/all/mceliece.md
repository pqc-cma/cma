---
title: "Classic McEliece"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 8
---
[Classic McEliece](https://classic.mceliece.org/) 是基于编码的公钥加密机制，以随机二元Goppa编码的线性解码问题为基础。[Specs.](https://classic.mceliece.org/nist/mceliece-20201010.pdf)

- 为确定性所必需的参数：
  - $m$：正整数
  - $n$：正整数
  - $t$：正整数
  - $\mu$：整数（默认为0）
  - $\nu$：整数（默认为0）

- 非必需参数：
  - $f(z)$：多项式
  - $F(z)$：多项式
