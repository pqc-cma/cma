---
title: "SIKE"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 14
---
[SIKE](https://sike.org/) 是一种基于同源的密钥封装机制，以超奇异同源图上的伪随机游走为基础。[Specs.](https://sike.org/files/SIDH-spec.pdf)
