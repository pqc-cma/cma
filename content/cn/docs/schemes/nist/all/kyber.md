---
title: "CRYSTALS-Kyber"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 7
---
[CRYSTALS-Kyber](https://pq-crystals.org/kyber/) 是基于模格上容错学习（LWE）问题的密钥封装机制。[Specs.](https://pq-crystals.org/kyber/data/kyber-specification-round3-20210131.pdf)

- 为确定性所必需的参数：
  - $k$：正整数

- 非必需参数：
  - $n$：正整数
  - $q$：正整数
  - $\eta_1$：正整数
  - $\eta_2$：正整数
  - $(d_1, d_2)$：正整数
