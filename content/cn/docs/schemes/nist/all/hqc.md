---
title: "HQC"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 6
---
[HQC](http://pqc-hqc.org/) 是基于汉明准循环编码的公钥加密机制 [Specs.](https://www.pqc-hqc.org/download.php?file=hqc-specification_2021-06-06.pdf)
