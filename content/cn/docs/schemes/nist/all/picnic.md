---
title: "Picnic"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 10
---
[Picnic](https://microsoft.github.io/Picnic/) 是基于零知识证明和对称加密原语的数字签名算法 [Specs.](https://github.com/microsoft/Picnic/blob/master/spec/spec-v3.0.pdf)
