---
title: "Rainbow"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 12
---
[Rainbow](https://www.pqcrainbow.org/) 是一种公钥加密机制，基于解一系列随机多变量二次方程的困难性。[Specs.](https://csrc.nist.gov/projects/post-quantum-cryptography/round-2-submissions)

- 为确定性所必需的参数：
  - $\nu_1$：正整数

- 为确定性所必需的参数：
  - $\mathbb{F}$: 伽罗瓦域 ($GF(16)$ 或 $GF(256)$)
  - $o_1$：正整数
  - $o_2$：正整数
