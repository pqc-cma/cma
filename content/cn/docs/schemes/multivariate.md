---
title: "Multivariate-Based Schemes"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 5
---
- [A key exchange protocol relying on polynomial maps](https://www.worldscientific.com/doi/full/10.1142/S2661335219500035):  [[ANI19]](../../refs#ani19)
- [An Improvement of a Key Exchange Protocol Relying on Polynomial Maps](http://arxiv.org/abs/2107.05924):  [[SN21]](../../refs#sn21)
- [Current State of Multivariate Cryptography](https://ieeexplore.ieee.org/document/8012305):  [[DP17]](../../refs#dp17)
- [Efficient key generation for Rainbow](https://www.researchgate.net/publication/340568378_Efficient_Key_Generation_for_Rainbow):  [[P20]](../../refs#p20)
- [Multivariate Encryption Schemes Based on Polynomial Equations over Real Numbers](https://link.springer.com/chapter/10.1007/978-3-030-44223-1_22):  [[YWT20]](../../refs#ywt20)
- [New Practical Multivariate Signatures from a Nonlinear Modifier](https://eprint.iacr.org/2021/429):  [[S21]](../../refs#s21)
- [On Desynchronised Multivariate El Gamal Algorithm](https://eprint.iacr.org/2017/712):  [[U17]](../../refs#u17)
- [Rainbow, a New Multivariable Polynomial Signature Scheme](https://link.springer.com/chapter/10.1007/11496137_12):  [[DS05]](../../refs#ds05)
