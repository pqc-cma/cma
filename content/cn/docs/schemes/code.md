---
title: "Code-Based Schemes"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 2
---
- [A Public-Key Cryptosystem Based On Algebraic Coding Theory](https://tmo.jpl.nasa.gov/progress_report2/42-44/44N.PDF):  [[M78]](../../refs#m78)
- [A Survey on Code-Based Cryptography](http://arxiv.org/abs/2201.07119):  [[WGR22]](../../refs#wgr22)
- [Code-based key encapsulation mechanisms for post-quantum standardization](https://ieeexplore.ieee.org/document/8409144):  [[KLK18]](../../refs#klk18)
- [Implementing QC-MDPC McEliece Encryption](https://doi.org/10.1145/2700102):  [[MOG15]](../../refs#mog15)
- [LESS-FM: Fine-tuning Signatures from a Code-based Cryptographic Group Action.](https://eprint.iacr.org/2021/396):  [[BBP21]](../../refs#bbp21)
- [Lightweight code-based cryptography: QC-MDPC McEliece encryption on reconfigurable devices](https://ieeexplore.ieee.org/document/6800252?arnumber=6800252):  [[vG14]](../../refs#vg14)
- [McBits Revisited](https://eprint.iacr.org/2017/793):  [[C17]](../../refs#c17)
- [McBits: Fast Constant-Time Code-Based Cryptography](https://eprint.iacr.org/2015/610):  [[BCS13]](../../refs#bcs13)
- [McEliece Cryptosystem Implementation: Theory and Practice](https://link.springer.com/chapter/10.1007/978-3-540-88403-3_4):  [[BS08]](../../refs#bs08)
- [McTiny: Fast High-Confidence Post-Quantum Key Erasure for Tiny Network Servers](https://www.usenix.org/conference/usenixsecurity20/presentation/bernstein):  [[BL20]](../../refs#bl20)
- [NIST PQC: CODE-BASED CRYPTOSYSTEMS](https://www.dl.begellhouse.com/journals/0632a9d54950b268,42c7cdd7728557a2,6b97dcff290ba987.html):  [[KGP19]](../../refs#kgp19)
- [QC-MDPC Decoders with Several Shades of Gray.](https://eprint.iacr.org/2019/1423):  [[DGK20]](../../refs#dgk20)
- [Quasi-Cyclic Low-Density Parity-Check Codes in the McEliece Cryptosystem](https://ieeexplore.ieee.org/document/4288832):  [[BCG07]](../../refs#bcg07)
- [Reducing Key Length of the McEliece Cryptosystem](https://www.researchgate.net/publication/221462135_Reducing_Key_Length_of_the_McEliece_Cryptosystem):  [[BCG09]](../../refs#bcg09)
- [Smaller Keys for Code-Based Cryptography: QC-MDPC McEliece Implementations on Embedded Devices](https://eprint.iacr.org/2015/425):  [[HvG13]](../../refs#hvg13)
- [Towards Side-Channel Resistant Implementations of QC-MDPC McEliece Encryption on Constrained Devices](https://www.researchgate.net/publication/285235340_Towards_Side-Channel_Resistant_Implementations_of_QC-MDPC_McEliece_Encryption_on_Constrained_Devices):  [[vG14]](../../refs#vg14)
