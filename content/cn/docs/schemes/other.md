---
title: "Other Development Efforts"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 7
---
- [Algebraic Extension Ring Framework for Non-Commutative Asymmetric Cryptography](https://arxiv.org/abs/2002.08343):  [[H20]](../../refs#h20)
- [BUFFing signature schemes beyond unforgeability and the case of post-quantum signatures](https://eprint.iacr.org/2020/1525):  [[CDF20]](../../refs#cdf20)
- [Challenges in Proving Post-Quantum Key Exchanges Based on Key Encapsulation Mechanisms](https://eprint.iacr.org/2019/1356/20191127:081325):  [[BFG19]](../../refs#bfg19)
- [Diffie-Hellman Instantiations in Pre- and Post- Quantum World: A Review Paper](https://ieeexplore.ieee.org/document/9296172):  [[RM20]](../../refs#rm20)
- [Encryptor Combiners: A Unified Approach to Multiparty NIKE, (H)IBE, and Broadcast Encryption](https://eprint.iacr.org/2017/152):  [[MZ17]](../../refs#mz17)
- [Network Coding-Based Post-Quantum Cryptography](http://arxiv.org/abs/2009.01931):  [[CDS20]](../../refs#cds20)
- [PQC: Extended Triple Decomposition Problem (XTDP) Applied To GL(d, Fp)-An Evolved Framework For Canonical Non-Commutative Cryptography](http://arxiv.org/abs/1812.05454):  [[H18]](../../refs#h18)
- [PQC: R-Propping a Chaotic Cellular Automata](http://rgdoi.net/10.13140/RG.2.2.11309.61924):  [[H21]](../../refs#h21)
- [PQC: R-Propping of Burmester-Desmedt Conference Key Distribution System](http://rgdoi.net/10.13140/RG.2.2.22638.43846):  [[H21]](../../refs#h21)
- [PQC: R-Propping of Public-Key Cryptosystems Using Polynomials over Non-commutative Algebraic Extension Rings](http://rgdoi.net/10.13140/RG.2.2.25826.56002):  [[H20]](../../refs#h20)
- [PQC: R-Propping of a New Group-Based Digital Signature](http://rgdoi.net/10.13140/RG.2.2.35795.91683):  [[H21]](../../refs#h21)
- [PQC: R-Propping of a Simple Oblivious Transfer](http://rgdoi.net/10.13140/RG.2.2.13925.32489):  [[H21]](../../refs#h21)
- [PQC: Triple Decomposition Problem Applied To GL(d, Fp) - A Secure Framework For Canonical Non-Commutative Cryptography](http://arxiv.org/abs/1810.08983):  [[H18]](../../refs#h18)
- [Post-Quantum Cryptography(PQC): Generalized ElGamal Cipher over GF(251\textasciicircum8)](http://arxiv.org/abs/1702.03587):  [[H17]](../../refs#h17)
- [Post-Quantum Cryptography: A Zero-Knowledge Authentication Protocol](http://arxiv.org/abs/1703.08630):  [[H17]](../../refs#h17)
- [Post-Quantum Cryptography: S381 Cyclic Subgroup of High Order](http://arxiv.org/abs/1704.07238):  [[H17]](../../refs#h17)
- [Post-Quantum Diffie-Hellman and Symmetric Key Exchange Protocols](https://ieeexplore.ieee.org/document/1652122):  [[LLK06]](../../refs#llk06)
- [Post-Quantum Zero-Knowledge and Signatures from Symmetric-Key Primitives](https://eprint.iacr.org/2017/279):  [[CDG17]](../../refs#cdg17)
- [Post-quantum key exchange mechanism for safety critical systems](https://hss-opus.ub.ruhr-uni-bochum.de/opus4/6653):  [[FVS19]](../../refs#fvs19)
- [Practical and post-quantum authenticated key exchange from one-way secure key encapsulation mechanism](https://dl.acm.org/doi/10.1145/2484313.2484323):  [[FSX13]](../../refs#fsx13)
- [R-Propping of HK17: Upgrade for a Detached Proposal of NIST PQC First Round Survey](https://eprint.iacr.org/2020/1217):  [[H20]](../../refs#h20)
- [SimS: a Simplification of SiGamal](https://eprint.iacr.org/2021/218):  [[FP21]](../../refs#fp21)
- [Strongly Secure Authenticated Key Exchange from Factoring, Codes, and Lattices](https://www.iacr.org/archive/pkc2012/72930468/72930468.pdf):  [[FSX12]](../../refs#fsx12)
