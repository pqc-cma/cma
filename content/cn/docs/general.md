---
title: "相关工作"
linktitle: "相关工作"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 4
---
针对后量子密码迁移和密码敏捷，有一些一般性的挑战，已有一系列综述工作在应对这些挑战，并给出了推荐做法。

*完整的引用列表见[引用](../refs)一节，该节以字母表顺序列出引用。*
- [Biggest Failures in Security](https://drops.dagstuhl.de/opus/volltexte/2020/11981/pdf/dagrep_v009_i011_p001_19451.pdf): 信息安全面临的大量问题，以及解决这些问题可能的策略 [[AVV19]](../refs#avv19)
- [Cybersecurity in an era with quantum computers: will we be ready?](https://ieeexplore.ieee.org/document/8490169): Assessment of the risks of quantum computers and their impact on the security of IT systems [[M15]](../refs#m15)
- [Eberbacher Gespräch: Next Generation Crypto](https://www.sit.fraunhofer.de/de/eberbach-crypto/): Key challenges forthree groups of industry differently involved in cryptography [[KNW18]](../refs#knw18)
- [From Pre-Quantum to Post-Quantum IoT Security: A Survey on Quantum-Resistant Cryptosystems for the Internet of Things](https://ieeexplore.ieee.org/abstract/document/8932459): 比较了各个PQC算法的性能，并给出物联网环境下PQC面临的挑战 [[F20]](../refs#f20)
- [Getting Ready for Post-Quantum Cryptography:: Explore Challenges Associated with Adoption and Use of Post-Quantum Cryptographic Algorithms](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.05262020-draft.pdf): 与使用后量子密码算法相关的挑战 [[BPS20]](../refs#bps20)
- [Identifying Research Challenges in Post Quantum Cryptography Migration and Cryptographic Agility](http://arxiv.org/abs/1909.07353): A wide range of topics and challenges at a high abstraction level grouped into categories of PQC migration and crypto-agility [[OPp19]](../refs#opp19)
- [Introduction to post-quantum cryptography](http://link.springer.com/10.1007/978-3-540-88702-7_1): Introduction to post-quantum cryptography [[B09]](../refs#b09)
- [Kryptografie quantensicher gestalten](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Broschueren/Kryptografie-quantensicher-gestalten.pdf?__blob=publicationFile&v=4): Overview of the recent development in quantum technology and recommendations for migration to quantum resistant cryptography [[d21]](../refs#d21)
- [Migration zu Post-Quanten-Kryptografie - Handlungsempfehlungen des BSI](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Post-Quanten-Kryptografie.html): 德国信息安全联邦办公室针对PQC迁移行动的建议 [[B20]](../refs#b20)
- [On PQC Migration and Crypto-Agility](https://arxiv.org/abs/2106.09599): 对所发现的挑战和不同领域的解决方案进行了现状综述，让读者入门该学科，了解现状变得更加容易 [[WAG21]](../refs#wag21)
- [On the Role of Hash-based Signatures in Quantum-Safe Internet of Things: Current Solutions and Future Directions](https://ieeexplore.ieee.org/document/9152977): Applications of one of the post-quantum signatures called Hash-Based Signature (HBS) schemes for the security of IoT devices in the quantum era [[SHK20]](../refs#shk20)
- [On the State of Post-Quantum Cryptography Migration](https://dl.gi.de/bitstream/handle/20.500.12116/37746/J1-2.pdf?sequence=1&isAllowed=y): Survey on the current state of the migration process towards PQ-Secure algorithms [[AWG21]](../refs#awg21)
- [Post Quantum Cryptography: Readiness Challenges and the Approaching Storm](http://arxiv.org/abs/2101.01269): Literature-study article about the upcomming threat of quantum computers against the currently widely used cryptography. [[CLO21]](../refs#clo21)
- [Post-Quantum Crypto for dummies](https://www.utimaco.com/de/post-quantum-crypto-dummies): Introduction to post-quantum cryptography [[WNG18]](../refs#wng18)
- [Post-Quantum Cryptography and 5G Security: Tutorial](https://doi.org/10.1145/3317549.3324882): 以建立“准备好应对量子时代”的5G核心网络为目标，给出了同时升级3GPP 5G标准和NIST后量子密码标准的指引 [[CMC19]](../refs#cmc19)
- [Post-quantum cryptography](http://www.nature.com/articles/nature23461): Literature-study of the ongoing development of PQC-schemes and algorithms as well as the ongoing security-scruitiniering of them. [[BL17]](../refs#bl17)
- [Practical Post-Quantum Cryptography](https://www.sit.fraunhofer.de/fileadmin/dokumente/studien_und_technical_reports/Practical.PostQuantum.Cryptography_WP_FraunhoferSIT.pdf?_=1503992279): 该文是Fraunhofer安全信息技术研究所的白皮书，比较了各个PQC算法，并给出对PQC迁移的挑战应对方法 [[NW17]](../refs#nw17)
- [Quantencomputerresistente Kryptografie: Aktuelle Aktivitäten und Fragestellungen](https://www.secumedia-shop.net/Deutschland-Digital-Sicher-30-Jahre-BSI): 简要评估了量子密码和后量子密码现状 [[HLL21]](../refs#hll21)
- [Quantum Safe Cryptography and Security: An introduction, benefits, enablers and challenges](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf): 给出了密码学的一些重要用例，以及后量子密码迁移可能实施的策略 [[CCD15]](../refs#ccd15)
- [Quantum computers will compromise the security of identity documents](https://www.eurosmart.com/quantum-computers-will-compromise-the-security-of-identity-documents/): The effects of quantum computers on the securiy of electronic identity documents [[E19]](../refs#e19)
- [Report on Post-Quantum Cryptography](https://nvlpubs.nist.gov/nistpubs/ir/2016/NIST.IR.8105.pdf): NIST Internal Report of the current state of quantum computers and post-quantum cryptography. [[CJL16]](../refs#cjl16)
- [Sicherheitsrisiko Quantencomputer](https://hub.kpmg.de/whitepaper-security-risks-of-quantum-computing?utm_campaign=Whitepaper%20-%20Security%20Risks%20of%20Quantum%20Computing%3A%20Recommended%20Actions%20for%20Post-quantum%20Cryptography&utm_source=AEM): Recommendations for post-quanum cryptography [[A19]](../refs#a19)
- [SoK: Challenges of Post-Quantum Digital Signing in Real-world Applications](https://eprint.iacr.org/2019/1374): Applications’ signing requirements against all 6 NIST’s post-quantum cryptography contest round 3 candidate algorithms [[TSZ19]](../refs#tsz19)
- [Standardisierung von Post-Quanten-Kryptografie und Empfehlungen des BSI](https://www.secumedia-shop.net/p/deutschland-digital-sicher-30-jahre-bsi-tg-band-2021): 综述了PQC标准化的现状，并以BSI名义提出建议 [[HKW21]](../refs#hkw21)
