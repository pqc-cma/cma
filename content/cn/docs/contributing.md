---
title: "贡献方法"
linktitle: "贡献方法"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 13
---
欢迎对本站进行贡献！

若要进行贡献，您可fork我们的[Gitlab仓库](https://gitlab.com/pqc-cma/cma)，更新后提交合并请求（merge request，即GitHub的pull request）。

#### 如何贡献
本站使用Hugo的Docsy Documentation主题，页面使用Markdown格式编写，一般的文本编辑器即可编辑。在访问Gitlab仓库并fork项目之后，您可自由编辑更新文件，然后提交并发送合并请求。若现有页面不足以满足您的需求，您还可以创建新的页面。请注意语法和格式，若您不确定写法和操作方法，尽量尝试即可，我们很乐意改正您合并请求中的错误。

#### 分叉（Fork）与合并（Merge）
可通过位于项目首页右上角的[Fork](https://gitlab.com/pqc-cma/cma/-/forks/new)按钮创建仓库分叉。在开发分支（dev分支）编辑您想要修改的文件（如添加或更新内容）。然后，您可以提交合并请求到原项目仓库的主分支（master分支）。

#### 仓库结构
项目树包含以下层次：
- 第1层包括Hugo的内容和主题目录，以及配置文件（如gitlab-ci.yml、config.toml、.gitmodules、.gitignore）。
- 第2层包括Hugo的Docsy主题和布局、mindmap_gen工具（使用Python脚本生成思维导图）、语言依赖项目录。
- 第3层包括文档目录，内含实际的HTML和Markdown文件，用于生成网站的页面。

若您要增添或修改内容，只关心文档目录中的文件即可。

#### 构建依赖项
建议尽可能使用Gitlab CICD流水线。然而事情并非那么简单……

若要在本地构建，环境中需要配置以下依赖项：
- 系统包：npm、nodejs、python3、git
- npm包：autoprefixer、postcss-cli、markmap-lib、markmap-cli
- 思维导图的HTML文件：./layouts/shortcodes/mindmap.html

可通过依次执行gitlab-ci.yml中的命令生成思维导图的HTML文件。

#### 内容依赖项
在分好类的页面中添加的新内容并不会自动添加到参考文献页面，这一机制还在开发中，目前可使用以下方法进行引用。

**例**：Quantum Safe Cryptography and Security: An introduction, benefits, enablers and challenges: 给出了密码学的重要用例和PQC迁移的潜在策略 [[CCD+15]](../refs#ccd15).

相关联的bib项需要在refs.md中这样写出：
###### [CCD+15]
[M.  Campagna,  L.  Chen,  O.  Dagdelen,  J.  Ding,  J  Fernick,  N.  Gisin,  D.  Hayford,  T.  Jennewein,  N.  Lütkenhaus,  and  M.  Mosca.  2015.Quantum  SafeCryptography  and  Security:  An  introduction,  benefits,  enablers  and  chal-lenges.European Telecommunications Standards InstituteETSI White Paper,8  (June  2015),  1–64.](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf)

需要注意，[CCD+15]是使用锚点的写法，和引用写法[[CCD+15]]是一样的，而钩子写法需要字母全小写且不含特殊符号（如../refs#ccd15）。钩子写法中的相对路径是相对于refs.md来说的（如上面钩子的路径意为返回上一级目录，再在文件refs.md中寻找#ccd15这个锚点）。

#### 步骤指引

* 使用开发分支。
* 每个PR都要加一个链接。
    * PR的标题必须以`Add project-name`格式编写。
    * 需要写一段话介绍您的贡献。
* 加上这个链接：`* [project-name](http://example.com/) - A short description ends with a period.`
    * 介绍需**保持简洁**。
* 按需添加节。
    * 添加节后，还需要添加该节的描述。
    * 在目录中加入节标题。
* 在提交新PR或issue之前请先搜索，防止重复提交。
* 检查好拼写和语法。
* 删除结尾多余的空格。

注：不限于我校成员，任何人都能进行贡献！
