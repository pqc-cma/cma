---
title: "其它"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 20
---
- [Incorporating post-quantum cryptography in a microservice environment](https://rp.os3.nl/2019-2020/p13/report.pdf): Performance of PQC in a microservice architecture similar to classical cryptography. [[W20]](../../../../refs#w20)
- [Quantum-resistant authentication algorithms for satellite-based augmentation systems](https://onlinelibrary.wiley.com/doi/abs/10.1002/navi.287): 给出了理解现代密码学面临的量子威胁所必需的密码原语，研究了TESLA和EC-Schnorr算法在广播系统中的使用 [[NWE19]](../../../../refs#nwe19)
