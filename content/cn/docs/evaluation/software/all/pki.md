---
title: "证书与密码标准"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 15
---
- [Composite Keys and Signatures For Use In Internet PKI](https://datatracker.ietf.org/doc/html/draft-ounsworth-pq-composite-sigs-03): 定义了组合公钥（CompositePublicKey）、组合签名（CompositeSignatureValue）、组合参数（CompositeParams）三种结构体，将每种算法“构件”分别定义的结构体组成序列 [[OP20]](../../../../refs#op20)
- [Next-Generation Web Public-Key Infrastructure Technologies](https://eprints.qut.edu.au/128643): 基于一般区块链的新式去中心化证书管理法，能与现有PKI兼容 [[H19]](../../../../refs#h19)
- [QChain: Quantum-resistant and Decentralized PKI using Blockchain](https://caislab.kaist.ac.kr/publication/paper_files/2018/SCIS'18_HC_BC.pdf): 使用区块链实现的量子安全去中心化PKI系统 [[AK18]](../../../../refs#ak18)
- [The Viability of Post-quantum X.509 Certificates](http://eprint.iacr.org/2018/063): 为签名算法的软件用例提出了合适的参数；为TLS 1.3的认证提出了合适的签名候选算法 [[KPD18]](../../../../refs#kpd18)
- [X.509-Compliant Hybrid Certificates for the Post-Quantum Transition](https://joss.theoj.org/papers/10.21105/joss.01606): 在公钥基础设施中同时使用两种密码算法，给出步步为营迁移到量子安全算法和混合模式 [[BBG19]](../../../../refs#bbg19)
