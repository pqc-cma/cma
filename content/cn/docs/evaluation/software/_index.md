---
title: "后量子安全协议"
date: 2021-05-05T22:41:49+02:00
draft: false
weight: 2
layout: no_index
---
本页综述了使用密码系统和密码包的标准安全协议及应用，以及它们PQC集成工作的现状。

|协议/应用|描述|安全面|机制|使用|OSI层次|
|--------------------|-----------|---------------|---------|-----|---------|
|**链路层**|
|[MACsec]({{< ref "/macsec" >}} "MACsec")|媒体访问控制安全|机密性|预共享秘密|以太网|2|
|[CHAP]({{< ref "/pap" >}} "CHAP")|询问握手认证协议|用户认证|哈希、询问、预共享秘密|点对点|2|
|[PAP]({{< ref "/pap" >}} "PAP")|口令认证协议|用户认证|用户名、口令|点对点|2|
|**网络层**|
|[AH]({{< ref "/ikev2" >}} "AH")|认证头|数据认证与完整性|哈希、预共享秘密|IPsec|3-4|
|[ESP]({{< ref "/ikev2" >}} "ESP")|封装安全负载|数据认证、完整性、机密性|哈希、预共享秘密、加密|IPsec|3-4|
|[IKEv2]({{< ref "/ikev2" >}} "IKEv2")|互联网密钥交换|用户认证和机密性|Diffie-Hellman密钥协商 & X.509证书|IPsec|3-4|
|**传输层**|
|[TLS/SSL]({{< ref "/tls" >}} "TLS/SSL")|传输层安全/安全套接字层|用户认证和机密性|Diffie-Hellman密钥协商 & X.509证书|TCP|4-7|
|[DTLS]({{< ref "/dtls" >}} "DTLS")|数据报传输层安全|用户认证和机密性|Diffie-Hellman密钥协商 & X.509证书|UDP|4|
|[SRTP/ZRTP]({{< ref "/rtp" >}} "SRTP/ZRTP")|（安全）实时传输协议|用户认证、完整性、机密性|Diffie-Hellman与哈希密钥协商、加密、MAC|RTP|4-7|
|**应用层**|
|[SSH]({{< ref "/ssh" >}} "SSH")|安全外壳协议|用户认证、完整性、机密性|Diffie-Hellman密钥协商、X.509证书、MAC、口令或公钥认证、加密|-|7|
|[OpenVPN]({{< ref "/vpn" >}} "OpenVPN")|开放虚拟专用网|数据认证、完整性、机密性|预共享密钥、Diffie-Hellman密钥协商（TLS）、X.509 证书、用户名与口令、MAC、加密|VPN|2-7|
|[WireGuard]({{< ref "/vpn" >}} "WireGuard")|WireGuard虚拟专用网|数据认证、完整性、机密性|Diffie-Hellman 密钥协商、MAC、加密|VPN|2-7|
|[X.509]({{< ref "/pki" >}} "X.509")|International Telecommunications Union (ITU) Standard|User Authentication, Integrity, Confidentiality|Digital Signatures & Certificates|Public Key Infrastructure (PKI)|7|
|[DNSSEC]({{< ref "/dnssec" >}} "DNSSEC")|域名系统安全扩展|数据认证和完整性|数字签名与证书|IP|7|
|[S-MIME/PGP-MIME]({{< ref "/smime" >}} "S-MIME/PGP-MIME")|安全多用途互联网邮件扩展|数据认证、完整性、机密性|混合加密与数字签名|电子邮件|7|
|[PGP/GPG]({{< ref "/pgp" >}} "PGP/GPG")|Pretty Good Privacy/GNU Privacy Guard|数据认证、完整性、机密性|混合加密与数字签名|电子邮件|7|
|[Kerberos/PKINIT]({{< ref "/pkinit" >}} "Kerberos/PKINIT")|认证服务|用户认证|用户名与口令、加密|网络通信|7|
|[SSO (OAuth/LDAP/SAML/RADIUS)]({{< ref "/sso" >}} "SSO (OAuth/LDAP/SAML/RADIUS)")|单点登录|用户认证|取决于使用的协议|网络通信|7|
|[SFTP]({{< ref "/sftp" >}} "SFTP")|SSH文件传输协议|用户认证与机密性|SSH和加密|文件传输|7|
|[eIDs]({{< ref "/eid" >}} "eIDs")|Electronic Identification Documents|Data / User Authentication, Integrity & Confidentiality|Diffie-Hellman Key Agreement, Certificates & Encryption|Secure eCard / Terminal Communication|7|
|[Signal]({{< ref "/signal" >}} "Signal")|Signal 协议|安全即时通信协议|用户认证、数据完整性与机密性|即时通信|7|
|[Blockchain]({{< ref "/blockchain" >}} "区块链")|DLTs (Distributed Ledgers)|Data / User Authentication, Integrity & Confidentiality|Hash, Digital Signatures, & Encryption|Peer-to-Peer Distributed Networks|2-7|
|[TOR]({{< ref "/tor" >}} "TOR")|Tor Network|Data / User Authentication, Integrity & Confidentiality|Diffie-Hellman Key Agreement & Encryption|Onion Routing|2-7|
-----------------------------------------------------------------------------------

##### [无线网络]({{< ref "/wlan" >}} "无线网络")
- WPA、IEEE 802.x1、EAP
- UMTS、LTE、5G
- 蓝牙、IR、蜂窝网络
- WSN

##### [其它]({{< ref "/others" >}} "其它")
- PKCS (公钥密码标准)
- CMS (密码消息语法)
- OPC UA (开放平台通信统一架构)
- L2TP (第二层隧道协议)
- SSTP (安全套接字隧道协议)
- GRE (一般路由封装)
- QUIC (Quick)
- S-RPC (安全远程过程调用)
- FinTS/HBCI
- PCT
- SET
- Telnet
