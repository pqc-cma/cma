---
title: "Embedded Evaluations"
linktitle: "Embedded Evaluations"
date: 2021-05-05T22:35:41+02:00
draft: false
type: docs
weight: 3
---
- [A Lightweight Implementation of Saber Resistant Against Side-Channel Attacks](https://eprint.iacr.org/2021/1452): Development of a masked hardware implementation of Saber key encapsulation mechanism. [[AMD21]](../../../refs#amd21)
- [Classic McEliece Implementation with Low Memory Footprint](https://link.springer.com/chapter/10.1007%2F978-3-030-68487-7_3): Implementation of McEliece on ARM Cortex-M4. [[RKK21]](../../../refs#rkk21)
- [Classic McEliece on the ARM Cortex-M4](https://eprint.iacr.org/2021/492): Classic McEliece算法在ARM Cortex-M4上的常数时间实现 [[CC21]](../../../refs#cc21)
- [Eﬃcient Implementations of A Quantum-Resistant Key-Exchange Protocol on Embedded systems](https://cacr.uwaterloo.ca/techreports/2014/cacr2014-20.pdf): Investigating the efficiency of implementing a newly proposed post-quantum key-exchange protocol on PC and ARM-powered embedded platforms. [[AFJ14]](../../../refs#afj14)
- [Fast Quantum-Safe Cryptography on IBM Z](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/hess-fast-quantum-safe-pqc2021.pdf): Presenting software optimizations of two candidates in the third round of the NIST PQC standardization process: SIKE and Dilithium. [[J21]](../../../refs#j21)
- [First-Order Masked Kyber on ARM Cortex-M4](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/heinz-first-order-pqc2021.pdf): Presenting a fast and first-order secure Kyber implementation optimized for ARM Cortex-M4. [[HKL21]](../../../refs#hkl21)
- [Implementing and Evaluating the Quantum Resistant Cryptographic Scheme Kyber on a Smart Card](http://www.diva-portal.org/smash/get/diva2:1464485/FULLTEXT01.pdf):  [[E20]](../../../refs#e20)
- [Memory-Efficient High-Speed Implementation of Kyber on Cortex-M4](http://link.springer.com/10.1007/978-3-030-23696-0_11): Kyber算法在ARM Cortex-M4上的优化软件实现 [[BKS19]](../../../refs#bks19)
- [MicroEliece: McEliece for Embedded Devices](https://link.springer.com/chapter/10.1007/978-3-642-04138-9_4): Implementation of the McEliece encryption scheme on a low-cost 8-bit AVR microprocessor and a Xilinx Spartan-3AN FPGA. [[EGH09]](../../../refs#egh09)
- [NEON-SIDH: Efficient Implementation of Supersingular Isogeny Diffie-Hellman Key Exchange Protocol on ARM](https://link.springer.com/chapter/10.1007/978-3-319-48965-0_6): New primes to speed up constant-time finite field arithmetic and perform isogenies quickly. [[KJA16]](../../../refs#kja16)
- [NTT Multiplication for NTT-unfriendly Rings:](https://tches.iacr.org/index.php/TCHES/article/view/8791): Efficient implementation of multiplication for polynomial rings used in Saber and NTRU using the Number-theoretic transform (NTT). [[CHK21]](../../../refs#chk21)
- [Post-Quantum Cryptography in Embedded Systems](https://dl.acm.org/doi/10.1145/3339252.3341475): Evaluation of NIST competition submissions with regard to their applicability to secure boot and protection of intermediate keys. [[MK19]](../../../refs#mk19)
- [Precomputation Methods for Faster and Greener Post-Quantum Cryptography on Emerging Embedded Platforms](https://eprint.iacr.org/2015/288): Domain-specific vector processor, VPQC, leveraging the extensible RISC-V architecture. [[AS15]](../../../refs#as15)
- [Rainbow on Cortex-M4](https://eprint.iacr.org/2021/532): NIST PQC最终候选签名算法Rainbow在Cortex-M4上的实现 [[CKY21]](../../../refs#cky21)
- [Ring-LWE Ciphertext Compression and Error Correction: Tools for Lightweight Post-Quantum Cryptography](https://dl.acm.org/doi/10.1145/3055245.3055254): Lattice-based public key cryptography can be implemented on ultra-lightweight hardware. [[S17]](../../../refs#s17)
- [Standard Lattice-Based Key Encapsulation on Embedded Devices](https://tches.iacr.org/index.php/TCHES/article/view/7279): Implementing FrodoKEM on a low-cost FPGA and microcontroller devices making conservative post-quantum cryptography practical on small devices. [[HOK18]](../../../refs#hok18)
- [Supersingular Isogeny Diffie-Hellman Key Exchange on 64-Bit ARM](https://ieeexplore.ieee.org/abstract/document/7970184): Efficient implementation of the supersingular isogeny Diffie-Hellman (SIDH) key exchange protocol on 64-bit ARMv8 processors. [[JAK19]](../../../refs#jak19)
- [pqm4: Testing and Benchmarking NIST PQC on ARM Cortex-M4](https://eprint.iacr.org/2019/844): pqm4, testing and benchmarking framework for the ARM Cortex-M4. [[KRS19]](../../../refs#krs19)
