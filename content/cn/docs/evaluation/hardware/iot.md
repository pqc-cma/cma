---
title: "IoT Evaluations"
linktitle: "IoT Evaluations"
date: 2021-05-05T22:35:41+02:00
draft: false
type: docs
weight: 4
---
- [A Flexible and Generic Gaussian Sampler With Power Side-Channel Countermeasures for Quantum-Secure Internet of Things](https://ieeexplore.ieee.org/document/9037365): CDT sampler providing high hardware flexibility, side-channel security, and suitability for resource-constrained IoT nodes. [[ZLC20]](../../../refs#zlc20)
- [A Lightweight Implementation of NTRU Prime for the Post-quantum Internet of Things](http://link.springer.com/10.1007/978-3-030-41702-4_7): First assembler-optimized implementation of Streamlined NTRU Prime for an 8-bit AVR microcontroller. [[CDG20]](../../../refs#cdg20)
- [A Study on Transmission Overhead of Post Quantum Cryptography Algorithms in Internet of Things Networks](https://ieeexplore.ieee.org/document/9021842/): Analysis of the overhead associated with the wireless transmission of post quantum cryptography algorithms through the Internet of Things networks. [[ATK19]](../../../refs#atk19)
- [A comprehensive survey of prominent cryptographic aspects for securing communication in post-quantum IoT networks](https://linkinghub.elsevier.com/retrieve/pii/S2542660520300159): Analysis of post-quantum cryptographic techniques for securing IoT networks. [[LLJ20]](../../../refs#llj20)
- [Evaluating Kyber post-quantum KEM in a mobile application](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ribeiro-evaluating-kyber-pqc2021.pdf): Evaluation of Kyber KEM running on an Android mobile application. [[dRd21]](../../../refs#drd21)
- [From Pre-Quantum to Post-Quantum IoT Security: A Survey on Quantum-Resistant Cryptosystems for the Internet of Things](https://ieeexplore.ieee.org/abstract/document/8932459): 比较了各个PQC算法的性能，并给出物联网环境下PQC面临的挑战 [[F20]](../../../refs#f20)
- [Lattice-based cryptosystems for the security of resource-constrained IoT devices in post-quantum world: a survey](https://link.springer.com/10.1007/s10586-021-03380-7): Examination of fundamental characteristics of IoT environments with focus on the post-quantum security. [[SNA21]](../../../refs#sna21)
- [Lightweight Post-Quantum-Secure Digital Signature Approach for IoT Motes](https://eprint.iacr.org/2019/122): Investigation of the use of the XMSS scheme targeting constrained devices (IoT). [[GMS19]](../../../refs#gms19)
- [Mobile Energy Requirements of the Upcoming NIST Post-Quantum Cryptography Standards](https://ieeexplore.ieee.org/document/9126751/): Energy requirement analysis based on extensive measurements of PQC candidate algorithms on a Cortex M4-based reference platform. [[O20]](../../../refs#o20)
- [Performance Evaluation and Optimisation for Kyber on the MULTOS IoT Trust-Anchor](https://ieeexplore.ieee.org/document/9191980/): Analysis from a practical implementation of the Kyber768 CPAPKE public key encryption component on a MULTOS IoT Trust-Anchor chip. [[M20]](../../../refs#m20)
- [Requirements for Post-Quantum Cryptography on Embedded Devices in the IoT](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/atkins-requirements-pqc-iot-pqc2021.pdf): Discussion on PQC on resources restricted devices and hints on achieving a fitting solution. [[D21]](../../../refs#d21)
- [Saber Post-Quantum Key Encapsulation Mechanism (KEM): Evaluating Performance in Mobile Devices and Suggesting Some Improvements](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ribeiro-saber-pq-key-pqc2021.pdf): Analysis of Saber performance in x64 and ARM architectures. [[LJd21]](../../../refs#ljd21)
- [Securing Edge Devices in the Post-Quantum Internet of Things Using Lattice-Based Cryptography](http://ieeexplore.ieee.org/document/8291132/): Discussion of the suitability of lattice-based cryptosystems for resource-constrained devices, outlining efficient implementations for 8 and 32-bit microcontrollers. [[LCG18]](../../../refs#lcg18)
- [Towards lightweight Identity-Based Encryption for the post-quantum-secure Internet of Things](http://ieeexplore.ieee.org/document/7918335/): Adopting an RLWE problem based IBE scheme for two security levels on ARM Cortex-M. [[GO17]](../../../refs#go17)
- [Towards post-quantum security for IoT endpoints with NTRU](http://ieeexplore.ieee.org/document/7927079/): Comparison of four different NTRUEncrypt implementations on an ARM Cortex M0-based microcontroller. [[GPB17]](../../../refs#gpb17)
