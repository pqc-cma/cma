---
title: "项目和计划"
linktitle: "项目和计划"
date: 2021-05-06T00:12:55+02:00
draft: false
type: docs
weight: 10
menu:
  main:
    weight: 2
---
- [NCCoE](https://www.nccoe.nist.gov/projects/building-blocks/post-quantum-cryptography)：
NCCoE“后量子密码迁移”项目

- [Open Quantum Safe](https://openquantumsafe.org/)：
开源项目，旨在支持后量子密码的原型开发

- [Quantum RISC](https://www.quantumrisc.de/)：
为嵌入式系统实现的下一代密码学

- [Eclipse CogniCrypt]( https://www.eclipse.org/cognicrypt/)：
密码软件的安全集成

- [BSI-Project: Secure Implementation of a Universal Crypto Library](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Kryptografie/Kryptobibliothek-Botan/kryptobibliothek-botan_node.html)：密码库的安全实现，详细信息可在[“Universal”夏季项目总结](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Projektzusammenfassung_Botan.pdf)中找到（仅德语）。

- [Aquorypt](https://www.forschung-it-sicherheit-kommunikationssysteme.de/projekte/aquorypt): Quantum-Resistant Cryptography integration in software applications.

- [PQC4MED](https://www.forschung-it-sicherheit-kommunikationssysteme.de/projekte/pqc4med): Protecting medical data in the post-quantum era.

- [CrypoEng pqdb](https://github.com/cryptoeng/pqdb): A comprehensive list of post-quantum crypto schemes and their properties.

- [PQC Integration](https://cspub.h-da.io/pqc/)：由Darmstadt应用科技大学的应用网络安全与用户中心安全研究组发起的PQC集成项目

- [DemoQuanDT](https://www.forschung-it-sicherheit-kommunikationssysteme.de/projekte/demoquandt):
Quantum-Key-Exchange in German telecommunication networks in the context of IT-Security.

- [PoQuID](https://www.mi.fu-berlin.de/inf/groups/ag-idm/projects/poquid/index.html):
Migration of cryptographic standards in official electronic documents towards quantum-computer-secure algorithms.
