---
title: "密码库和密码接口"
linktitle: "密码库和密码接口"
date: 2021-05-06T00:13:06+02:00
draft: false
type: docs
weight: 11
menu:
  main:
    weight: 4
---
- [liboqs](https://github.com/open-quantum-safe/liboqs)：
C语言编写的开源PQC算法库

- [PQClean](https://github.com/PQClean/PQClean)：
便携的C语言PQC实现

- [The Legion of the Bouncy Castle](https://www.bouncycastle.org/specifications.html)：
密码算法的Java实现，包括适用于任何环境的轻量级API

- [PQCCRYPTO](https://pqcrypto.eu.org/) A portfolio of high-security post-quantum public-key systems, providing efficient implementations of high-security post-quantum cryptography for a broad spectrum of real-world applications.

- [NaCL (Salt)](https://nacl.cr.yp.to/)：
实现网络通信、加解密、签名等的软件库

- [Libsodium](https://libsodium.gitbook.io/doc/)：
具有便携性，可跨平台编译，可安装和打包。包含一个可兼容的API软件库，实现加解密、签名，以及口令哈希

- [LibHydrogen](https://github.com/jedisct1/libhydrogen)：
资源受限环境下的轻量级密码库

- [WASI Cryptography APIs](https://github.com/WebAssembly/wasi-crypto)：[WebAssembly社区群组](https://www.w3.org/community/webassembly/)WASI子群组的密码API提案开发

- [Botan: Crypto and TLS for Modern C++](https://botan.randombit.net/)：该库使用C++编写，实现一系列实际系统，如TLS协议、X.509证书、现代AEAD密码、PKCS#11和TPM硬件支持、口令哈希、后量子密码。该库还提供一些其它语言（如Python）的绑定（binding）。BSI批准的Botan版本可在[GitHub仓库](https://github.com/Rohde-Schwarz/botan)中找到

- [eUCRITE API](https://cspub.h-da.io/eucrite/)：
密码API，非常强调可用性和安全性。由Darmstadt应用科技大学的应用网络安全与用户中心安全研究组开发。
