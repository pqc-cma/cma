---
title: "Side-Channel Attacks"
linktitle: "Side-Channel Attacks"
date: 2021-05-05T22:35:41+02:00
draft: false
type: docs
weight: 2
---
- [A Reaction Attack Against Cryptosystems Based on LRPC Codes](https://link.springer.com/chapter/10.1007/978-3-030-30530-7_10): 分析了基于低秩对偶检验（LRPC）编码的密码系统 [[SSP19]](../../refs#ssp19)
- [A Side-Channel Assisted Attack on NTRU](https://eprint.iacr.org/2021/790): Secret key recovery through power analysis and a mittigation approach [[AR21]](../../refs#ar21)
- [A Side-channel Resistant Implementation of SABER](https://eprint.iacr.org/2020/733.pdf): 综述了格密码算法面临的侧信道攻击，以及相应反制法 [[D20]](../../refs#d20)
- [Chosen Ciphertext k-Trace Attacks on Masked CCA2 Secure Kyber](https://eprint.iacr.org/2021/956.pdf): Side-Channel-Analysis-Attack on the lattice-based algorithm Kyber, using leakage from the Number-Theoretic-Transform (NTT). [[HHP21]](../../refs#hhp21)
- [Chosen ciphertext Simple Power Analysis on software 8-bit implementation of ring-LWE encryption](https://ieeexplore.ieee.org/document/7835555): A Simple Power Analysis attack on ring-LWE based PQC schemes [[PH16]](../../refs#ph16)
- [Curse of Re-encryption: A Generic Power/EM Analysis on Post-Quantum KEMs](https://tches.iacr.org/index.php/TCHES/article/view/9298): EM/Power-Analysis Attack on the re-encryption part of the FO-Tranform [[UXT22]](../../refs#uxt22)
- [Defeating NewHope with a Single Trace.](https://rd.springer.com/content/pdf/10.1007%2F978-3-030-44223-1_11.pdf): Power Attack recreating NewHope private key from measuring the power-consumption on a cortex M4 with the reference C implementation. [[ACL20]](../../refs#acl20)
- [Differential Power Analysis of XMSS and SPHINCS](http://link.springer.com/10.1007/978-3-319-89641-0_10): Differential Power Analysis Attack on XMSS and SPHINCS through analysing different building blocks of these schemes. [[KGB18]](../../refs#kgb18)
- [Differential Power Analysis of the Picnic Signature Scheme.](https://eprint.iacr.org/2020/267): Multi-Trace Differential Power Attack of Picnic Signature scheme on an ARM cortex-m4. [[GSE20]](../../refs#gse20)
- [EM Side-channel Analysis on Smartphone Early Boot Encryption](https://www.seceng.ruhr-uni-bochum.de/media/attachments/files/2021/04/Master_thesis_Oleksiy_Lisovets.pdf): Recovery of the bootcode encryption Key of a Smartphone by EM-Probing [[L20]](../../refs#l20)
- [Fault Attacks on UOV and Rainbow](http://link.springer.com/10.1007/978-3-030-16350-1_11): Multivariate signature schemes offer a good protection against physical fault attacks. [[KL19]](../../refs#kl19)
- [Flush, Gauss, and Reload – A Cache Attack on the BLISS Lattice-Based Signature Scheme](https://rd.springer.com/content/pdf/10.1007%2F978-3-662-53140-2_16.pdf): Recovering the private Key of the BLISS signature scheme through a cache timing sidechannel attack. [[GHL16]](../../refs#ghl16)
- [Full key recovery side-channel attack against ephemeral SIKE on the Cortex-M4](https://eprint.iacr.org/2021/858.pdf): Power Analysis on the Three-Point-Ladder in the PQC Algorithm SIKE [[GdK21]](../../refs#gdk21)
- [Generic Side-channel attacks on CCA-secure lattice-based PKE and KEMs](https://tches.iacr.org/index.php/TCHES/article/view/8592): EM-Sidechannel in the Error-Correction-Code of lattice-based LWE/LWR Algorithms [[RSC20]](../../refs#rsc20)
- [Horizontal side-channel vulnerabilities of post-quantum key exchange protocols](https://ieeexplore.ieee.org/document/8383894): Differential Power Attacks on Frodo and NewHope FPGA based implementations [[ATT18]](../../refs#att18)
- [Improving smart card security using self-timed circuits](https://www.cl.cam.ac.uk/~swm11/research/papers/async2002.pdf): Encoding mechanism lowering the correlation between data and power consumption [[MAC02]](../../refs#mac02)
- [Lattice-Based Signature Schemes and their Sensitivity to Fault Attacks](https://eprint.iacr.org/2016/415): Study about fault attacks on lattice-based signature schemes [[BBK16]](../../refs#bbk16)
- [Location, Location, Location: Revisiting Modeling and Exploitation for Location-Based Side Channel Leakages](http://link.springer.com/10.1007/978-3-030-34618-8_10): Neural Network aided EM-probing based sidechannel attack [[ABC19]](../../refs#abc19)
- [On Generic Side-Channel Assisted Chosen Ciphertext Attacks on Lattice-based PKE/KEMs Towards key recovery attacks on NTRU-based PKE/KEMs](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ravi-generic-side-channel-pqc2021.pdf): EM-Sdechannel on Lattice-based LWE/LWR PQC schemes [[REB21]](../../refs#reb21)
- [On Generic Side-Channel Assisted Chosen Ciphertext Attacks on Lattice-based PKE/KEMs](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/ravi-generic-side-channel-pqc2021.pdf): Study on different sidechannel attack methodes [[PMS21]](../../refs#pms21)
- [Physical Protection of Lattice-Based Cryptography: Challenges and Solutions](https://dl.acm.org/doi/10.1145/3194554.3194616): 分析了格密码算法使用的高斯取样器面临的攻击，以及相应的反制法 [[KOV18]](../../refs#kov18)
- [Physical security in the post-quantum era: A survey on side-channel analysis, random number generators, and physically unclonable functions](http://link.springer.com/10.1007/s13389-021-00255-w): 综述了几种与PQC相关的侧信道攻击方法 [[CCA21]](../../refs#cca21)
- [Power-based Side Channel Attack Analysis on PQC Algorithms](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/kamucheka-power-based-pqc2021.pdf): Summary of different Hardwarebased sidechannel attacks [[TMT21]](../../refs#tmt21)
- [Resistance of Isogeny-Based Cryptographic Implementations to a Fault Attack](https://hal-cea.archives-ouvertes.fr/cea-03266892): Private key leakage by EM-Fault injection on an ARM Cortex A53 with the SIKE PCQ algorithm. [[TDE21]](../../refs#tde21)
- [Safe-Error Attacks on SIKE and CSIDH](https://eprint.iacr.org/2021/1132): Four safe-error attacks, two against SIKE and two against aconstant-time implementation of CSIDH that uses dummy isogenies [[CKM21]](../../refs#ckm21)
- [Security of Hedged Fiat–Shamir Signatures Under Fault Attacks](https://eprint.iacr.org/2019/956.pdf): exploration of the effect of bit-tampering fault attacks in fiat-shamir signing operations [[AOT20]](../../refs#aot20)
- [Side-Channel Analysis and Countermeasure Design on ARM-based Quantum-Resistant SIKE](https://ieeexplore.ieee.org/document/9181442): 使用掩码技术实现了Saber算法，能够抵抗侧信道攻击 [[ZYD20]](../../refs#zyd20)
- [Side-Channel Attacks on Post-Quantum Signature Schemes based on Multivariate Quadratic Equations](https://tches.iacr.org/index.php/TCHES/article/view/7284): Key-recovery with correlation power attacks [[PSK18]](../../refs#psk18)
- [Side-Channel Protections for Picnic Signatures](https://eprint.iacr.org/2021/735.pdf): Study of side-channel security of MPCitH proof protocols [[ABE21]](../../refs#abe21)
- [Single-Trace Attacks on Keccak](https://tches.iacr.org/index.php/TCHES/article/view/8590): Singe Trace Power analysis attack of keccak [[KPP20]](../../refs#kpp20)
- [Single-Trace Side-Channel Attacks on ω-Small Polynomial Sampling: With Applications to NTRU, NTRU Prime, and CRYSTALS-DILITHIUM](https://ieeexplore.ieee.org/document/9702284/): Power-Sidechannel Attack on the Random-Sampling and Sorting used in NTRU-HPS and some other lattice-based Schemes. [[KAA21]](../../refs#kaa21)
- [Systematic Classification of Side-Channel Attacks: A Case Study for Mobile Devices](https://repository.ubn.ru.nl/bitstream/2066/187230/1/187230.pdf): Survey on Side-Channel Attacks on modern commonly used device classes [[SMK18]](../../refs#smk18)
- [Torsion point attacks on SIDH-like cryptosystems](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/kirshanova-lower-bounds-pqc2021.pdf): optimization of the nearest neighbor technique used in lattice based cryptography [[KL21]](../../refs#kl21)
- [Zero-Value Side-Channel Attacks on SIKE](https://eprint.iacr.org/2022/054.pdf): Power-Analysis based Sidechannel-Attack on the SIKE-PQC-Scheme [[dPT22]](../../refs#dpt22)
