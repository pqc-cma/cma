---
title: "Testing & Automation"
linktitle: "Testing & Automation"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 2
---
- [A Survey on Tools and Techniques for the Programming and Verification of Secure Cryptographic Software](https://www.researchgate.net/publication/283730120_A_Survey_on_Tools_and_Techniques_for_the_Programming_and_Verification_of_Secure_Cryptographic_Software): This paper contributes to broaden the discussion on tools and techniques in cryptographic programming and verification [[BD15]](../../refs#bd15)
- [A property-based testing framework for encryption programs](https://doi.org/10.1007/s11704-014-3040-y): In this paper, the author propose a property-based approach to testing encryption programs in the absence of oracles. [[SWW14]](../../refs#sww14)
- [Automation of the Cryptographic Module Validation Program (CMVP)](https://csrc.nist.gov/publications/detail/white-paper/2021/04/12/automation-of-the-cryptographic-module-validation-program-cmvp/draft): This paper documents the project background and proposed scenarios for the the CMVP (Cryptographic Module Validation Program). [[V21]](../../refs#v21)
- [Evaluation Framework for Security and Resource Consumption of Cryptographic Algorithms](https://www.semanticscholar.org/paper/Evaluation-Framework-for-Security-and-Resource-of-Duta-Mocanu/c9e56cd2c10ccd3f01f911739eb369a7770abf61): In this paper, the authors present a framework for testing and evaluating cryptographic algorithms. [[DMV15]](../../refs#dmv15)
- [Extending NIST's CAVP Testing of Cryptographic Hash Function Implementations](https://www.springerprofessional.de/en/extending-nist-s-cavp-testing-of-cryptographic-hash-function-imp/17699818): This paper describes a vulnerability in Apple's CoreCrypto library, which affects 11 out of the 12 implemented hash functions. [[MC20]](../../refs#mc20)
- [Many a Mickle Makes a Muckle: A Framework for Provably Quantum-Secure Hybrid Key Exchange](https://eprint.iacr.org/2020/099.pdf): 本文提出混合认证密钥交换协议的安全分析框架，引入Muckle协议 [[DHP20]](../../refs#dhp20)
- [Systematic Testing of Post-Quantum Cryptographic Implementations Using Metamorphic Testing](https://ieeexplore.ieee.org/document/8785645/): 密码算法的实现非常复杂，本文探究了针对密码算法实现的系统性测试方法，用于发现其中的bug [[PRK19]](../../refs#prk19)
- [THE POWER OF NIST CRYPTOGRAPHIC STATISTICAL TESTS SUITE](https://www.researchgate.net/publication/333968632_THE_POWER_OF_NIST_CRYPTOGRAPHIC_STATISTICAL_TESTS_SUITE): This paper focuses on the open question regarding the correlation and the power of the NIST statistical test suite. [[SB19]](../../refs#sb19)
- [Topics in Cryptology – CT-RSA 2020: The Cryptographers’ Track at the RSA Conference 2020, San Francisco, CA, USA, February 24–28, 2020, Proceedings](http://link.springer.com/10.1007/978-3-030-40186-3): In this paper the authors formalize and study a generic framework for building tweakable block cipher based on regular block ciphers. [[J20]](../../refs#j20)
- [Towards a Methodology for the Development of Secure Cryptographic Software](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7861647): This paper proposes a methodology for development of secure cryptographic software, providing a structured way to approach cryptography into secure development methods. [[BD16]](../../refs#bd16)
