---
title: "贡献者"
linktitle: "贡献者"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 14
---
本站由[Darmstadt应用科技大学](https://h-da.de/)的[应用网络安全](https://fbi.h-da.de/forschung/arbeitsgruppen/applied-cyber-security-darmstadt)和[用户中心安全](https://fbi.h-da.de/index.php?id=764)研究组发起，[德国国家应用网络安全研究中心ATHENE](https://www.athene-center.de)提供资助。

贡献者如下：

| 姓名                           | 所属机构 | 贡献 |
|--------------------------------|-------------|--------------|
|Alexander Wiesmaier|Darmstadt应用科技大学|原论文、社区网站|
|Alexander Zeier|MTG AG Darmstadt|原论文|
|Andreas Heinemann|Darmstadt应用科技大学|原论文|
|Dominik Heinz|Darmstadt应用科技大学|社区网站、开发 |
|Julian Geißler|Darmstadt应用科技大学|原论文|
|Nouri Alnahawi|Darmstadt应用科技大学|原论文、社区网站、开发、更新 |
|Peter Hecht|Buenos Aires University|社区网站|
|Pia Bauspieß|Darmstadt应用科技大学|原论文|
|Robin Meunier|Darmstadt应用科技大学|PQC算法标准、协议集成|
|Tobias Grasmeyer|Darmstadt应用科技大学|原论文、社区网站、开发 |
|胡希|重庆大学|社区网站内容与中文翻译|
