baseURL = "/"
relativeURLs = true
languageCode = "en-us"
title = "Cryptographic Migration & Agility"

enableRobotsTXT = true

theme = ["docsy"]

disableKinds = ["taxonomy", "term"]

# Language settings
contentDir = "content/en"
defaultContentLanguage = "en"
defaultContentLanguageInSubdir = false


enableGitInfo = true
time_format_default = "02.01.2006"

[languages]
[languages.en]
title = "Cryptographic Migration & Agility"
description = "An open community site for sharing any relevant research, findings, and solutions on PQC migration and cryptographic agility"
languageName ="English"
# Weight used for sorting.
weight = 1

[languages.cn]
title = "Cryptographic Migration & Agility"
description = "Simplified Chinese Test"
languageName ="Chinese"
contentDir = "content/cn"
weight = 2


[Author]
  name = "Nouri Alnahawi"
  email = "nouri.alnahawi@h-da.de"

# Highlighting config
pygmentsCodeFences = true
pygmentsUseClasses = false
# Use the new Chroma Go highlighter in Hugo.
pygmentsUseClassic = false
#pygmentsOptions = "linenos=table"
# See https://help.farbox.com/pygments.html
pygmentsStyle = "tango"

[[menu.main]]
    name = "Home"
    weight = 5
    url = "/"
    pre = "<i class='fas fa-home'></i>"

# Configure how URLs look like per section.
[permalinks]
blog = "/:section/:year/:month/:day/:slug/"

# Image processing configuration.
[imaging]
resampleFilter = "CatmullRom"
quality = 75
anchor = "smart"

[markup]
  [markup.goldmark]
    [markup.goldmark.renderer]
      unsafe = true
  [markup.highlight]
      # See a complete list of available styles at https://xyproto.github.io/splash/docs/all.html
      style = "tango"
      # Uncomment if you want your chosen highlight style used for code blocks without a specified language
      # guessSyntax = "true"

# Google Analytics Settings
# Requires adding env to build: HUGO_ENV="production" hugo
[services.googleAnalytics]
id = "UA-202391736-1"

[params]
copyright = "Darmstadt University of Applied Sciences"

# Repository configuration (URLs for in-page links to opening issues and suggesting changes)
# github_repo = "https://gitlab.com/pqc-cma/cma"
# github_branch = "dev"

# User interface configuration
[params.ui]
# Enable to show the side bar menu in its compact state.
sidebar_menu_compact = true
#  Set to true to disable breadcrumb navigation.
breadcrumb_disable = false
#  Set to true to hide the sidebar search box (the top nav search box will still be displayed if search is enabled)
sidebar_search_disable = true
#  Set to false if you don't want to display a logo (/assets/icons/logo.svg) in the top nav bar
navbar_logo = false
# Set to true to disable the About link in the site footer
footer_about_disable = false

[params.katex]
enable = true

[[params.katex.options.delimiters]]
  left = "$$"
  right = "$$"
  display = true

[[params.katex.options.delimiters]]
  left = "$"
  right = "$"
  display = false

[params.links]
# End user relevant links. These will show up on left side of footer and in the community page if you have one.
[[params.links.developer]]
	name = "E-Mail"
	url = "mailto:cma@h-da.de"
	icon = "fa fa-envelope"
    desc = "E-Mail Contact"

# Developer relevant links. These will show up on right side of footer and in the community page if you have one.
[[params.links.developer]]
	name = "GitLab"
	url = "https://gitlab.com/pqc-cma/cma"
	icon = "fab fa-gitlab"
  desc = "GitLab Repository of This Site"

[[params.links.developer]]
	name = "Institute"
	url = "https://fbi.h-da.de"
	icon = "fa fa-university"
    desc = "Visit our institute homepage"

[params.links.impress]
  url = "https://fbi.h-da.de/en/index.php?id=15"
  text = "Impress"

[outputFormats]
[outputFormats.PRINT]
baseName = "index"
isHTML = true
mediaType = "text/html"
path = "_print"
permalinkable = false
